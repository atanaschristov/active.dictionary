var globalLanguages = [{
	'family': 'Northwest Caucasian',
	'name': 'Abkhaz',
	'native name': 'аҧсуа бызшәа',
	'639-1': ' аҧсшәа',
	'639-2/t': 'ab',
	'639-2/b': 'abk',
	'639-3': 'abk',
	'639-6': 'abk',
	'notes': 'abks'
}, {
	'family': 'Afro-Asiatic',
	'name': 'Afar',
	'native name': 'Afaraf',
	'639-1': 'aa',
	'639-2/t': 'aar',
	'639-2/b': 'aar',
	'639-3': 'aar',
	'639-6': 'aars',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Afrikaans',
	'native name': 'Afrikaans',
	'639-1': 'af',
	'639-2/t': 'afr',
	'639-2/b': 'afr',
	'639-3': 'afr',
	'639-6': 'afrs',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Akan',
	'native name': 'Akan',
	'639-1': 'ak',
	'639-2/t': 'aka',
	'639-2/b': 'aka',
	'639-3': 'aka + 2',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Indo-European',
	'name': 'Albanian',
	'native name': 'gjuha shqipe',
	'639-1': 'sq',
	'639-2/t': 'sqi',
	'639-2/b': 'alb',
	'639-3': 'sqi + 4',
	'639-6': '–',
	'notes': 'macrolanguage'
}, {
	'family': 'Afro-Asiatic',
	'name': 'Amharic',
	'native name': 'አማርኛ',
	'639-1': 'am',
	'639-2/t': 'amh',
	'639-2/b': 'amh',
	'639-3': 'amh',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Afro-Asiatic',
	'name': 'Arabic',
	'native name': 'العربية',
	'639-1': 'ar',
	'639-2/t': 'ara',
	'639-2/b': 'ara',
	'639-3': 'ara + 30',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Indo-European',
	'name': 'Aragonese',
	'native name': 'aragonés',
	'639-1': 'an',
	'639-2/t': 'arg',
	'639-2/b': 'arg',
	'639-3': 'arg',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Armenian',
	'native name': 'Հայերեն',
	'639-1': 'hy',
	'639-2/t': 'hye',
	'639-2/b': 'arm',
	'639-3': 'hye',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Assamese',
	'native name': 'অসমীয়া',
	'639-1': 'as',
	'639-2/t': 'asm',
	'639-2/b': 'asm',
	'639-3': 'asm',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Northeast Caucasian',
	'name': 'Avaric',
	'native name': 'авар мацӀ',
	'639-1': ' магӀарул мацӀ',
	'639-2/t': 'av',
	'639-2/b': 'ava',
	'639-3': 'ava',
	'639-6': 'ava',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Avestan',
	'native name': 'avesta',
	'639-1': 'ae',
	'639-2/t': 'ave',
	'639-2/b': 'ave',
	'639-3': 'ave',
	'639-6': '',
	'notes': 'ancient'
}, {
	'family': 'Aymaran',
	'name': 'Aymara',
	'native name': 'aymar aru',
	'639-1': 'ay',
	'639-2/t': 'aym',
	'639-2/b': 'aym',
	'639-3': 'aym + 2',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Turkic',
	'name': 'Azerbaijani',
	'native name': 'azərbaycan dili',
	'639-1': 'az',
	'639-2/t': 'aze',
	'639-2/b': 'aze',
	'639-3': 'aze + 2',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Niger–Congo',
	'name': 'Bambara',
	'native name': 'bamanankan',
	'639-1': 'bm',
	'639-2/t': 'bam',
	'639-2/b': 'bam',
	'639-3': 'bam',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Turkic',
	'name': 'Bashkir',
	'native name': 'башҡорт теле',
	'639-1': 'ba',
	'639-2/t': 'bak',
	'639-2/b': 'bak',
	'639-3': 'bak',
	'639-6': '',
	'notes': ''
}, {
	'family': 'isolate',
	'name': 'Basque',
	'native name': 'euskara',
	'639-1': ' euskera',
	'639-2/t': 'eu',
	'639-2/b': 'eus',
	'639-3': 'baq',
	'639-6': 'eus',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Belarusian',
	'native name': 'беларуская мова',
	'639-1': 'be',
	'639-2/t': 'bel',
	'639-2/b': 'bel',
	'639-3': 'bel',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Bengali; Bangla',
	'native name': 'বাংলা',
	'639-1': 'bn',
	'639-2/t': 'ben',
	'639-2/b': 'ben',
	'639-3': 'ben',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Bihari',
	'native name': 'भोजपुरी',
	'639-1': 'bh',
	'639-2/t': 'bih',
	'639-2/b': 'bih',
	'639-3': '–',
	'639-6': '',
	'notes': 'Collective code for Bhojpuri'
}, {
	'family': 'Creole',
	'name': 'Bislama',
	'native name': 'Bislama',
	'639-1': 'bi',
	'639-2/t': 'bis',
	'639-2/b': 'bis',
	'639-3': 'bis',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Bosnian',
	'native name': 'bosanski jezik',
	'639-1': 'bs',
	'639-2/t': 'bos',
	'639-2/b': 'bos',
	'639-3': 'bos',
	'639-6': 'boss',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Breton',
	'native name': 'brezhoneg',
	'639-1': 'br',
	'639-2/t': 'bre',
	'639-2/b': 'bre',
	'639-3': 'bre',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Bulgarian',
	'native name': 'български език',
	'639-1': 'bg',
	'639-2/t': 'bul',
	'639-2/b': 'bul',
	'639-3': 'bul',
	'639-6': 'buls',
	'notes': ''
}, {
	'family': 'Sino-Tibetan',
	'name': 'Burmese',
	'native name': 'ဗမာစာ',
	'639-1': 'my',
	'639-2/t': 'mya',
	'639-2/b': 'bur',
	'639-3': 'mya',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Catalan; Valencian',
	'native name': 'català',
	'639-1': 'valencià',
	'639-2/t': 'ca',
	'639-2/b': 'cat',
	'639-3': 'cat',
	'639-6': 'cat',
	'notes': ''
}, {
	'family': 'Austronesian',
	'name': 'Chamorro',
	'native name': 'Chamoru',
	'639-1': 'ch',
	'639-2/t': 'cha',
	'639-2/b': 'cha',
	'639-3': 'cha',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Northeast Caucasian',
	'name': 'Chechen',
	'native name': 'нохчийн мотт',
	'639-1': 'ce',
	'639-2/t': 'che',
	'639-2/b': 'che',
	'639-3': 'che',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Chichewa; Chewa; Nyanja',
	'native name': 'chiCheŵa',
	'639-1': ' chinyanja',
	'639-2/t': 'ny',
	'639-2/b': 'nya',
	'639-3': 'nya',
	'639-6': 'nya',
	'notes': ''
}, {
	'family': 'Sino-Tibetan',
	'name': 'Chinese',
	'native name': '中文 (Zhōngwén)',
	'639-1': ' 汉语',
	'639-2/t': ' 漢語',
	'639-2/b': 'zh',
	'639-3': 'zho',
	'639-6': 'chi',
	'notes': 'zho + 13'
}, {
	'family': 'Turkic',
	'name': 'Chuvash',
	'native name': 'чӑваш чӗлхи',
	'639-1': 'cv',
	'639-2/t': 'chv',
	'639-2/b': 'chv',
	'639-3': 'chv',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Cornish',
	'native name': 'Kernewek',
	'639-1': 'kw',
	'639-2/t': 'cor',
	'639-2/b': 'cor',
	'639-3': 'cor',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Corsican',
	'native name': 'corsu',
	'639-1': ' lingua corsa',
	'639-2/t': 'co',
	'639-2/b': 'cos',
	'639-3': 'cos',
	'639-6': 'cos',
	'notes': ''
}, {
	'family': 'Algonquian',
	'name': 'Cree',
	'native name': 'ᓀᐦᐃᔭᐍᐏᐣ',
	'639-1': 'cr',
	'639-2/t': 'cre',
	'639-2/b': 'cre',
	'639-3': 'cre + 6',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Indo-European',
	'name': 'Croatian',
	'native name': 'hrvatski jezik',
	'639-1': 'hr',
	'639-2/t': 'hrv',
	'639-2/b': 'hrv',
	'639-3': 'hrv',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Czech',
	'native name': 'čeština',
	'639-1': ' český jazyk',
	'639-2/t': 'cs',
	'639-2/b': 'ces',
	'639-3': 'cze',
	'639-6': 'ces',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Danish',
	'native name': 'dansk',
	'639-1': 'da',
	'639-2/t': 'dan',
	'639-2/b': 'dan',
	'639-3': 'dan',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Divehi; Dhivehi; Maldivian;',
	'native name': 'ދިވެހި',
	'639-1': 'dv',
	'639-2/t': 'div',
	'639-2/b': 'div',
	'639-3': 'div',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Dutch',
	'native name': 'Nederlands',
	'639-1': ' Vlaams',
	'639-2/t': 'nl',
	'639-2/b': 'nld',
	'639-3': 'dut',
	'639-6': 'nld',
	'notes': ''
}, {
	'family': 'Sino-Tibetan',
	'name': 'Dzongkha',
	'native name': 'རྫོང་ཁ',
	'639-1': 'dz',
	'639-2/t': 'dzo',
	'639-2/b': 'dzo',
	'639-3': 'dzo',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'English',
	'native name': 'English',
	'639-1': 'en',
	'639-2/t': 'eng',
	'639-2/b': 'eng',
	'639-3': 'eng',
	'639-6': 'engs',
	'notes': ''
}, {
	'family': 'Constructed',
	'name': 'Esperanto',
	'native name': 'Esperanto',
	'639-1': 'eo',
	'639-2/t': 'epo',
	'639-2/b': 'epo',
	'639-3': 'epo',
	'639-6': '',
	'notes': 'constructed'
}, {
	'family': 'Uralic',
	'name': 'Estonian',
	'native name': 'eesti',
	'639-1': ' eesti keel',
	'639-2/t': 'et',
	'639-2/b': 'est',
	'639-3': 'est',
	'639-6': 'est + 2',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Ewe',
	'native name': 'Eʋegbe',
	'639-1': 'ee',
	'639-2/t': 'ewe',
	'639-2/b': 'ewe',
	'639-3': 'ewe',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Faroese',
	'native name': 'føroyskt',
	'639-1': 'fo',
	'639-2/t': 'fao',
	'639-2/b': 'fao',
	'639-3': 'fao',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Austronesian',
	'name': 'Fijian',
	'native name': 'vosa Vakaviti',
	'639-1': 'fj',
	'639-2/t': 'fij',
	'639-2/b': 'fij',
	'639-3': 'fij',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Uralic',
	'name': 'Finnish',
	'native name': 'suomi',
	'639-1': ' suomen kieli',
	'639-2/t': 'fi',
	'639-2/b': 'fin',
	'639-3': 'fin',
	'639-6': 'fin',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'French',
	'native name': 'français',
	'639-1': ' langue française',
	'639-2/t': 'fr',
	'639-2/b': 'fra',
	'639-3': 'fre',
	'639-6': 'fra',
	'notes': 'fras'
}, {
	'family': 'Niger–Congo',
	'name': 'Fula; Fulah; Pulaar; Pular',
	'native name': 'Fulfulde',
	'639-1': ' Pulaar',
	'639-2/t': ' Pular',
	'639-2/b': 'ff',
	'639-3': 'ful',
	'639-6': 'ful',
	'notes': 'ful + 9'
}, {
	'family': 'Indo-European',
	'name': 'Galician',
	'native name': 'galego',
	'639-1': 'gl',
	'639-2/t': 'glg',
	'639-2/b': 'glg',
	'639-3': 'glg',
	'639-6': '',
	'notes': ''
}, {
	'family': 'South Caucasian',
	'name': 'Georgian',
	'native name': 'ქართული',
	'639-1': 'ka',
	'639-2/t': 'kat',
	'639-2/b': 'geo',
	'639-3': 'kat',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'German',
	'native name': 'Deutsch',
	'639-1': 'de',
	'639-2/t': 'deu',
	'639-2/b': 'ger',
	'639-3': 'deu',
	'639-6': 'deus',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Greek',
	'native name': ' Modern',
	'639-1': 'ελληνικά',
	'639-2/t': 'el',
	'639-2/b': 'ell',
	'639-3': 'gre',
	'639-6': 'ell',
	'notes': 'ells'
}, {
	'family': 'Tupian',
	'name': 'Guaraní',
	'native name': 'Avañe\'\'ẽ',
	'639-1': 'gn',
	'639-2/t': 'grn',
	'639-2/b': 'grn',
	'639-3': 'grn + 5',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Indo-European',
	'name': 'Gujarati',
	'native name': 'ગુજરાતી',
	'639-1': 'gu',
	'639-2/t': 'guj',
	'639-2/b': 'guj',
	'639-3': 'guj',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Creole',
	'name': 'Haitian; Haitian Creole',
	'native name': 'Kreyòl ayisyen',
	'639-1': 'ht',
	'639-2/t': 'hat',
	'639-2/b': 'hat',
	'639-3': 'hat',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Afro-Asiatic',
	'name': 'Hausa',
	'native name': 'Hausa',
	'639-1': ' هَوُسَ',
	'639-2/t': 'ha',
	'639-2/b': 'hau',
	'639-3': 'hau',
	'639-6': 'hau',
	'notes': ''
}, {
	'family': 'Afro-Asiatic',
	'name': 'Hebrew (modern)',
	'native name': 'עברית',
	'639-1': 'he',
	'639-2/t': 'heb',
	'639-2/b': 'heb',
	'639-3': 'heb',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Herero',
	'native name': 'Otjiherero',
	'639-1': 'hz',
	'639-2/t': 'her',
	'639-2/b': 'her',
	'639-3': 'her',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Hindi',
	'native name': 'हिन्दी',
	'639-1': ' हिंदी',
	'639-2/t': 'hi',
	'639-2/b': 'hin',
	'639-3': 'hin',
	'639-6': 'hin',
	'notes': 'hins'
}, {
	'family': 'Austronesian',
	'name': 'Hiri Motu',
	'native name': 'Hiri Motu',
	'639-1': 'ho',
	'639-2/t': 'hmo',
	'639-2/b': 'hmo',
	'639-3': 'hmo',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Uralic',
	'name': 'Hungarian',
	'native name': 'magyar',
	'639-1': 'hu',
	'639-2/t': 'hun',
	'639-2/b': 'hun',
	'639-3': 'hun',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Constructed',
	'name': 'Interlingua',
	'native name': 'Interlingua',
	'639-1': 'ia',
	'639-2/t': 'ina',
	'639-2/b': 'ina',
	'639-3': 'ina',
	'639-6': '',
	'notes': 'constructed by International Auxiliary Association'
}, {
	'family': 'Austronesian',
	'name': 'Indonesian',
	'native name': 'Bahasa Indonesia',
	'639-1': 'id',
	'639-2/t': 'ind',
	'639-2/b': 'ind',
	'639-3': 'ind',
	'639-6': '',
	'notes': 'Covered by macro[ms/msa]'
}, {
	'family': 'Constructed',
	'name': 'Interlingue',
	'native name': 'Originally called Occidental; then Interlingue after WWII',
	'639-1': 'ie',
	'639-2/t': 'ile',
	'639-2/b': 'ile',
	'639-3': 'ile',
	'639-6': '',
	'notes': 'constructed by Edgar de Wahl'
}, {
	'family': 'Indo-European',
	'name': 'Irish',
	'native name': 'Gaeilge',
	'639-1': 'ga',
	'639-2/t': 'gle',
	'639-2/b': 'gle',
	'639-3': 'gle',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Igbo',
	'native name': 'Asụsụ Igbo',
	'639-1': 'ig',
	'639-2/t': 'ibo',
	'639-2/b': 'ibo',
	'639-3': 'ibo',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Eskimo–Aleut',
	'name': 'Inupiaq',
	'native name': 'Iñupiaq',
	'639-1': ' Iñupiatun',
	'639-2/t': 'ik',
	'639-2/b': 'ipk',
	'639-3': 'ipk',
	'639-6': 'ipk + 2',
	'notes': ''
}, {
	'family': 'Constructed',
	'name': 'Ido',
	'native name': 'Ido',
	'639-1': 'io',
	'639-2/t': 'ido',
	'639-2/b': 'ido',
	'639-3': 'ido',
	'639-6': 'idos',
	'notes': 'constructed by De Beaufront'
}, {
	'family': 'Indo-European',
	'name': 'Icelandic',
	'native name': 'Íslenska',
	'639-1': 'is',
	'639-2/t': 'isl',
	'639-2/b': 'ice',
	'639-3': 'isl',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Italian',
	'native name': 'italiano',
	'639-1': 'it',
	'639-2/t': 'ita',
	'639-2/b': 'ita',
	'639-3': 'ita',
	'639-6': 'itas',
	'notes': ''
}, {
	'family': 'Eskimo–Aleut',
	'name': 'Inuktitut',
	'native name': 'ᐃᓄᒃᑎᑐᑦ',
	'639-1': 'iu',
	'639-2/t': 'iku',
	'639-2/b': 'iku',
	'639-3': 'iku + 2',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Japonic',
	'name': 'Japanese',
	'native name': '日本語 (にほんご)',
	'639-1': 'ja',
	'639-2/t': 'jpn',
	'639-2/b': 'jpn',
	'639-3': 'jpn',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Austronesian',
	'name': 'Javanese',
	'native name': 'basa Jawa',
	'639-1': 'jv',
	'639-2/t': 'jav',
	'639-2/b': 'jav',
	'639-3': 'jav',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Eskimo–Aleut',
	'name': 'Kalaallisut',
	'native name': ' Greenlandic',
	'639-1': 'kalaallisut',
	'639-2/t': ' kalaallit oqaasii',
	'639-2/b': 'kl',
	'639-3': 'kal',
	'639-6': 'kal',
	'notes': 'kal'
}, {
	'family': 'Dravidian',
	'name': 'Kannada',
	'native name': 'ಕನ್ನಡ',
	'639-1': 'kn',
	'639-2/t': 'kan',
	'639-2/b': 'kan',
	'639-3': 'kan',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Nilo-Saharan',
	'name': 'Kanuri',
	'native name': 'Kanuri',
	'639-1': 'kr',
	'639-2/t': 'kau',
	'639-2/b': 'kau',
	'639-3': 'kau + 3',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Indo-European',
	'name': 'Kashmiri',
	'native name': 'कश्मीरी',
	'639-1': ' كشميري‎',
	'639-2/t': 'ks',
	'639-2/b': 'kas',
	'639-3': 'kas',
	'639-6': 'kas',
	'notes': ''
}, {
	'family': 'Turkic',
	'name': 'Kazakh',
	'native name': 'қазақ тілі',
	'639-1': 'kk',
	'639-2/t': 'kaz',
	'639-2/b': 'kaz',
	'639-3': 'kaz',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Austroasiatic',
	'name': 'Khmer',
	'native name': 'ខ្មែរ',
	'639-1': ' ខេមរភាសា',
	'639-2/t': ' ភាសាខ្មែរ',
	'639-2/b': 'km',
	'639-3': 'khm',
	'639-6': 'khm',
	'notes': 'khm'
}, {
	'family': 'Niger–Congo',
	'name': 'Kikuyu',
	'native name': ' Gikuyu',
	'639-1': 'Gĩkũyũ',
	'639-2/t': 'ki',
	'639-2/b': 'kik',
	'639-3': 'kik',
	'639-6': 'kik',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Kinyarwanda',
	'native name': 'Ikinyarwanda',
	'639-1': 'rw',
	'639-2/t': 'kin',
	'639-2/b': 'kin',
	'639-3': 'kin',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Turkic',
	'name': 'Kyrgyz',
	'native name': 'Кыргызча',
	'639-1': ' Кыргыз тили',
	'639-2/t': 'ky',
	'639-2/b': 'kir',
	'639-3': 'kir',
	'639-6': 'kir',
	'notes': ''
}, {
	'family': 'Uralic',
	'name': 'Komi',
	'native name': 'коми кыв',
	'639-1': 'kv',
	'639-2/t': 'kom',
	'639-2/b': 'kom',
	'639-3': 'kom + 2',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Niger–Congo',
	'name': 'Kongo',
	'native name': 'KiKongo',
	'639-1': 'kg',
	'639-2/t': 'kon',
	'639-2/b': 'kon',
	'639-3': 'kon + 3',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'isolate',
	'name': 'Korean',
	'native name': '한국어 (韓國語)',
	'639-1': ' 조선어 (朝鮮語)',
	'639-2/t': 'ko',
	'639-2/b': 'kor',
	'639-3': 'kor',
	'639-6': 'kor',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Kurdish',
	'native name': 'Kurdî',
	'639-1': ' كوردی‎',
	'639-2/t': 'ku',
	'639-2/b': 'kur',
	'639-3': 'kur',
	'639-6': 'kur + 3',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Kwanyama',
	'native name': ' Kuanyama',
	'639-1': 'Kuanyama',
	'639-2/t': 'kj',
	'639-2/b': 'kua',
	'639-3': 'kua',
	'639-6': 'kua',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Latin',
	'native name': 'latine',
	'639-1': ' lingua latina',
	'639-2/t': 'la',
	'639-2/b': 'lat',
	'639-3': 'lat',
	'639-6': 'lat',
	'notes': 'lats'
}, {
	'family': 'Indo-European',
	'name': 'Luxembourgish',
	'native name': ' Letzeburgesch',
	'639-1': 'Lëtzebuergesch',
	'639-2/t': 'lb',
	'639-2/b': 'ltz',
	'639-3': 'ltz',
	'639-6': 'ltz',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Ganda',
	'native name': 'Luganda',
	'639-1': 'lg',
	'639-2/t': 'lug',
	'639-2/b': 'lug',
	'639-3': 'lug',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Limburgish',
	'native name': ' Limburgan',
	'639-1': ' Limburger',
	'639-2/t': 'Limburgs',
	'639-2/b': 'li',
	'639-3': 'lim',
	'639-6': 'lim',
	'notes': 'lim'
}, {
	'family': 'Niger–Congo',
	'name': 'Lingala',
	'native name': 'Lingála',
	'639-1': 'ln',
	'639-2/t': 'lin',
	'639-2/b': 'lin',
	'639-3': 'lin',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Tai–Kadai',
	'name': 'Lao',
	'native name': 'ພາສາລາວ',
	'639-1': 'lo',
	'639-2/t': 'lao',
	'639-2/b': 'lao',
	'639-3': 'lao',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Lithuanian',
	'native name': 'lietuvių kalba',
	'639-1': 'lt',
	'639-2/t': 'lit',
	'639-2/b': 'lit',
	'639-3': 'lit',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Luba-Katanga',
	'native name': 'Tshiluba',
	'639-1': 'lu',
	'639-2/t': 'lub',
	'639-2/b': 'lub',
	'639-3': 'lub',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Latvian',
	'native name': 'latviešu valoda',
	'639-1': 'lv',
	'639-2/t': 'lav',
	'639-2/b': 'lav',
	'639-3': 'lav + 2',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Indo-European',
	'name': 'Manx',
	'native name': 'Gaelg',
	'639-1': ' Gailck',
	'639-2/t': 'gv',
	'639-2/b': 'glv',
	'639-3': 'glv',
	'639-6': 'glv',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Macedonian',
	'native name': 'македонски јазик',
	'639-1': 'mk',
	'639-2/t': 'mkd',
	'639-2/b': 'mac',
	'639-3': 'mkd',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Austronesian',
	'name': 'Malagasy',
	'native name': 'fiteny malagasy',
	'639-1': 'mg',
	'639-2/t': 'mlg',
	'639-2/b': 'mlg',
	'639-3': 'mlg + 10',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Austronesian',
	'name': 'Malay',
	'native name': 'bahasa Melayu',
	'639-1': ' بهاس ملايو‎',
	'639-2/t': 'ms',
	'639-2/b': 'msa',
	'639-3': 'may',
	'639-6': 'msa + 13',
	'notes': ''
}, {
	'family': 'Dravidian',
	'name': 'Malayalam',
	'native name': 'മലയാളം',
	'639-1': 'ml',
	'639-2/t': 'mal',
	'639-2/b': 'mal',
	'639-3': 'mal',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Afro-Asiatic',
	'name': 'Maltese',
	'native name': 'Malti',
	'639-1': 'mt',
	'639-2/t': 'mlt',
	'639-2/b': 'mlt',
	'639-3': 'mlt',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Austronesian',
	'name': 'Māori',
	'native name': 'te reo Māori',
	'639-1': 'mi',
	'639-2/t': 'mri',
	'639-2/b': 'mao',
	'639-3': 'mri',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Marathi (Marāṭhī)',
	'native name': 'मराठी',
	'639-1': 'mr',
	'639-2/t': 'mar',
	'639-2/b': 'mar',
	'639-3': 'mar',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Austronesian',
	'name': 'Marshallese',
	'native name': 'Kajin M̧ajeļ',
	'639-1': 'mh',
	'639-2/t': 'mah',
	'639-2/b': 'mah',
	'639-3': 'mah',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Mongolic',
	'name': 'Mongolian',
	'native name': 'монгол',
	'639-1': 'mn',
	'639-2/t': 'mon',
	'639-2/b': 'mon',
	'639-3': 'mon + 2',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Austronesian',
	'name': 'Nauru',
	'native name': 'Ekakairũ Naoero',
	'639-1': 'na',
	'639-2/t': 'nau',
	'639-2/b': 'nau',
	'639-3': 'nau',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Dené–Yeniseian',
	'name': 'Navajo',
	'native name': ' Navaho',
	'639-1': 'Diné bizaad',
	'639-2/t': ' Dinékʼehǰí',
	'639-2/b': 'nv',
	'639-3': 'nav',
	'639-6': 'nav',
	'notes': 'nav'
}, {
	'family': 'Indo-European',
	'name': 'Norwegian Bokmål',
	'native name': 'Norsk bokmål',
	'639-1': 'nb',
	'639-2/t': 'nob',
	'639-2/b': 'nob',
	'639-3': 'nob',
	'639-6': '',
	'notes': 'Covered by macro[no/nor]'
}, {
	'family': 'Niger–Congo',
	'name': 'North Ndebele',
	'native name': 'isiNdebele',
	'639-1': 'nd',
	'639-2/t': 'nde',
	'639-2/b': 'nde',
	'639-3': 'nde',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Nepali',
	'native name': 'नेपाली',
	'639-1': 'ne',
	'639-2/t': 'nep',
	'639-2/b': 'nep',
	'639-3': 'nep',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Ndonga',
	'native name': 'Owambo',
	'639-1': 'ng',
	'639-2/t': 'ndo',
	'639-2/b': 'ndo',
	'639-3': 'ndo',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Norwegian Nynorsk',
	'native name': 'Norsk nynorsk',
	'639-1': 'nn',
	'639-2/t': 'nno',
	'639-2/b': 'nno',
	'639-3': 'nno',
	'639-6': '',
	'notes': 'Covered by macro[no/nor]'
}, {
	'family': 'Indo-European',
	'name': 'Norwegian',
	'native name': 'Norsk',
	'639-1': 'no',
	'639-2/t': 'nor',
	'639-2/b': 'nor',
	'639-3': 'nor + 2',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Sino-Tibetan',
	'name': 'Nuosu',
	'native name': 'ꆈꌠ꒿ Nuosuhxop',
	'639-1': 'ii',
	'639-2/t': 'iii',
	'639-2/b': 'iii',
	'639-3': 'iii',
	'639-6': '',
	'notes': 'Standard form of Yi languages'
}, {
	'family': 'Niger–Congo',
	'name': 'South Ndebele',
	'native name': 'isiNdebele',
	'639-1': 'nr',
	'639-2/t': 'nbl',
	'639-2/b': 'nbl',
	'639-3': 'nbl',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Occitan',
	'native name': 'occitan',
	'639-1': ' lenga d\'\'òc',
	'639-2/t': 'oc',
	'639-2/b': 'oci',
	'639-3': 'oci',
	'639-6': 'oci',
	'notes': ''
}, {
	'family': 'Algonquian',
	'name': 'Ojibwe',
	'native name': ' Ojibwa',
	'639-1': 'ᐊᓂᔑᓈᐯᒧᐎᓐ',
	'639-2/t': 'oj',
	'639-2/b': 'oji',
	'639-3': 'oji',
	'639-6': 'oji + 7',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Old Church Slavonic',
	'native name': ' Church Slavonic',
	'639-1': ' Old Bulgarian',
	'639-2/t': 'ѩзыкъ словѣньскъ',
	'639-2/b': 'cu',
	'639-3': 'chu',
	'639-6': 'chu',
	'notes': 'chu'
}, {
	'family': 'Afro-Asiatic',
	'name': 'Oromo',
	'native name': 'Afaan Oromoo',
	'639-1': 'om',
	'639-2/t': 'orm',
	'639-2/b': 'orm',
	'639-3': 'orm + 4',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Indo-European',
	'name': 'Oriya',
	'native name': 'ଓଡ଼ିଆ',
	'639-1': 'or',
	'639-2/t': 'ori',
	'639-2/b': 'ori',
	'639-3': 'ori',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Ossetian',
	'native name': ' Ossetic',
	'639-1': 'ирон æвзаг',
	'639-2/t': 'os',
	'639-2/b': 'oss',
	'639-3': 'oss',
	'639-6': 'oss',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Panjabi',
	'native name': ' Punjabi',
	'639-1': 'ਪੰਜਾਬੀ',
	'639-2/t': ' پنجابی‎',
	'639-2/b': 'pa',
	'639-3': 'pan',
	'639-6': 'pan',
	'notes': 'pan'
}, {
	'family': 'Indo-European',
	'name': 'Pāli',
	'native name': 'पाऴि',
	'639-1': 'pi',
	'639-2/t': 'pli',
	'639-2/b': 'pli',
	'639-3': 'pli',
	'639-6': '',
	'notes': 'ancient'
}, {
	'family': 'Indo-European',
	'name': 'Persian (Farsi)',
	'native name': 'فارسی',
	'639-1': 'fa',
	'639-2/t': 'fas',
	'639-2/b': 'per',
	'639-3': 'fas + 2',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Indo-European',
	'name': 'Polish',
	'native name': 'język polski',
	'639-1': ' polszczyzna',
	'639-2/t': 'pl',
	'639-2/b': 'pol',
	'639-3': 'pol',
	'639-6': 'pol',
	'notes': 'pols'
}, {
	'family': 'Indo-European',
	'name': 'Pashto',
	'native name': ' Pushto',
	'639-1': 'پښتو',
	'639-2/t': 'ps',
	'639-2/b': 'pus',
	'639-3': 'pus',
	'639-6': 'pus + 3',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Portuguese',
	'native name': 'português',
	'639-1': 'pt',
	'639-2/t': 'por',
	'639-2/b': 'por',
	'639-3': 'por',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Quechuan',
	'name': 'Quechua',
	'native name': 'Runa Simi',
	'639-1': ' Kichwa',
	'639-2/t': 'qu',
	'639-2/b': 'que',
	'639-3': 'que',
	'639-6': 'que + 44',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Romansh',
	'native name': 'rumantsch grischun',
	'639-1': 'rm',
	'639-2/t': 'roh',
	'639-2/b': 'roh',
	'639-3': 'roh',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Kirundi',
	'native name': 'Ikirundi',
	'639-1': 'rn',
	'639-2/t': 'run',
	'639-2/b': 'run',
	'639-3': 'run',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Romanian',
	'native name': 'limba română',
	'639-1': 'ro',
	'639-2/t': 'ron',
	'639-2/b': 'rum',
	'639-3': 'ron',
	'639-6': '',
	'notes': '[mo] for Moldavian has been withdrawn'
}, {
	'family': 'Indo-European',
	'name': 'Russian',
	'native name': 'русский язык',
	'639-1': 'ru',
	'639-2/t': 'rus',
	'639-2/b': 'rus',
	'639-3': 'rus',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Sanskrit (Saṁskṛta)',
	'native name': 'संस्कृतम्',
	'639-1': 'sa',
	'639-2/t': 'san',
	'639-2/b': 'san',
	'639-3': 'san',
	'639-6': '',
	'notes': 'ancient'
}, {
	'family': 'Indo-European',
	'name': 'Sardinian',
	'native name': 'sardu',
	'639-1': 'sc',
	'639-2/t': 'srd',
	'639-2/b': 'srd',
	'639-3': 'srd + 4',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Indo-European',
	'name': 'Sindhi',
	'native name': 'सिन्धी',
	'639-1': ' سنڌي، سندھی‎',
	'639-2/t': 'sd',
	'639-2/b': 'snd',
	'639-3': 'snd',
	'639-6': 'snd',
	'notes': ''
}, {
	'family': 'Uralic',
	'name': 'Northern Sami',
	'native name': 'Davvisámegiella',
	'639-1': 'se',
	'639-2/t': 'sme',
	'639-2/b': 'sme',
	'639-3': 'sme',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Austronesian',
	'name': 'Samoan',
	'native name': 'gagana fa\'\'a Samoa',
	'639-1': 'sm',
	'639-2/t': 'smo',
	'639-2/b': 'smo',
	'639-3': 'smo',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Creole',
	'name': 'Sango',
	'native name': 'yângâ tî sängö',
	'639-1': 'sg',
	'639-2/t': 'sag',
	'639-2/b': 'sag',
	'639-3': 'sag',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Serbian',
	'native name': 'српски језик',
	'639-1': 'sr',
	'639-2/t': 'srp',
	'639-2/b': 'srp',
	'639-3': 'srp',
	'639-6': '',
	'notes': 'The ISO 639-2/T code srp deprecated the ISO 639-2/B code scc'
}, {
	'family': 'Indo-European',
	'name': 'Scottish Gaelic; Gaelic',
	'native name': 'Gàidhlig',
	'639-1': 'gd',
	'639-2/t': 'gla',
	'639-2/b': 'gla',
	'639-3': 'gla',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Shona',
	'native name': 'chiShona',
	'639-1': 'sn',
	'639-2/t': 'sna',
	'639-2/b': 'sna',
	'639-3': 'sna',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Sinhala',
	'native name': ' Sinhalese',
	'639-1': 'සිංහල',
	'639-2/t': 'si',
	'639-2/b': 'sin',
	'639-3': 'sin',
	'639-6': 'sin',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Slovak',
	'native name': 'slovenčina',
	'639-1': ' slovenský jazyk',
	'639-2/t': 'sk',
	'639-2/b': 'slk',
	'639-3': 'slo',
	'639-6': 'slk',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Slovene',
	'native name': 'slovenski jezik',
	'639-1': ' slovenščina',
	'639-2/t': 'sl',
	'639-2/b': 'slv',
	'639-3': 'slv',
	'639-6': 'slv',
	'notes': ''
}, {
	'family': 'Afro-Asiatic',
	'name': 'Somali',
	'native name': 'Soomaaliga',
	'639-1': ' af Soomaali',
	'639-2/t': 'so',
	'639-2/b': 'som',
	'639-3': 'som',
	'639-6': 'som',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Southern Sotho',
	'native name': 'Sesotho',
	'639-1': 'st',
	'639-2/t': 'sot',
	'639-2/b': 'sot',
	'639-3': 'sot',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Turkic',
	'name': 'South Azerbaijani',
	'native name': 'تورکجه‎',
	'639-1': 'az',
	'639-2/t': 'azb',
	'639-2/b': 'azb',
	'639-3': 'azb',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Spanish; Castilian',
	'native name': 'español',
	'639-1': ' castellano',
	'639-2/t': 'es',
	'639-2/b': 'spa',
	'639-3': 'spa',
	'639-6': 'spa',
	'notes': ''
}, {
	'family': 'Austronesian',
	'name': 'Sundanese',
	'native name': 'Basa Sunda',
	'639-1': 'su',
	'639-2/t': 'sun',
	'639-2/b': 'sun',
	'639-3': 'sun',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Swahili',
	'native name': 'Kiswahili',
	'639-1': 'sw',
	'639-2/t': 'swa',
	'639-2/b': 'swa',
	'639-3': 'swa + 2',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Niger–Congo',
	'name': 'Swati',
	'native name': 'SiSwati',
	'639-1': 'ss',
	'639-2/t': 'ssw',
	'639-2/b': 'ssw',
	'639-3': 'ssw',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Swedish',
	'native name': 'Svenska',
	'639-1': 'sv',
	'639-2/t': 'swe',
	'639-2/b': 'swe',
	'639-3': 'swe',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Dravidian',
	'name': 'Tamil',
	'native name': 'தமிழ்',
	'639-1': 'ta',
	'639-2/t': 'tam',
	'639-2/b': 'tam',
	'639-3': 'tam',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Dravidian',
	'name': 'Telugu',
	'native name': 'తెలుగు',
	'639-1': 'te',
	'639-2/t': 'tel',
	'639-2/b': 'tel',
	'639-3': 'tel',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Tajik',
	'native name': 'тоҷикӣ',
	'639-1': ' toğikī',
	'639-2/t': ' تاجیکی‎',
	'639-2/b': 'tg',
	'639-3': 'tgk',
	'639-6': 'tgk',
	'notes': 'tgk'
}, {
	'family': 'Tai–Kadai',
	'name': 'Thai',
	'native name': 'ไทย',
	'639-1': 'th',
	'639-2/t': 'tha',
	'639-2/b': 'tha',
	'639-3': 'tha',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Afro-Asiatic',
	'name': 'Tigrinya',
	'native name': 'ትግርኛ',
	'639-1': 'ti',
	'639-2/t': 'tir',
	'639-2/b': 'tir',
	'639-3': 'tir',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Sino-Tibetan',
	'name': 'Tibetan Standard',
	'native name': ' Tibetan',
	'639-1': ' Central',
	'639-2/t': 'བོད་ཡིག',
	'639-2/b': 'bo',
	'639-3': 'bod',
	'639-6': 'tib',
	'notes': 'bod'
}, {
	'family': 'Turkic',
	'name': 'Turkmen',
	'native name': 'Türkmen',
	'639-1': ' Түркмен',
	'639-2/t': 'tk',
	'639-2/b': 'tuk',
	'639-3': 'tuk',
	'639-6': 'tuk',
	'notes': ''
}, {
	'family': 'Austronesian',
	'name': 'Tagalog',
	'native name': 'Wikang Tagalog',
	'639-1': ' ᜏᜒᜃᜅ᜔ ᜆᜄᜎᜓᜄ᜔',
	'639-2/t': 'tl',
	'639-2/b': 'tgl',
	'639-3': 'tgl',
	'639-6': 'tgl',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Tswana',
	'native name': 'Setswana',
	'639-1': 'tn',
	'639-2/t': 'tsn',
	'639-2/b': 'tsn',
	'639-3': 'tsn',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Austronesian',
	'name': 'Tonga (Tonga Islands)',
	'native name': 'faka Tonga',
	'639-1': 'to',
	'639-2/t': 'ton',
	'639-2/b': 'ton',
	'639-3': 'ton',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Turkic',
	'name': 'Turkish',
	'native name': 'Türkçe',
	'639-1': 'tr',
	'639-2/t': 'tur',
	'639-2/b': 'tur',
	'639-3': 'tur',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Tsonga',
	'native name': 'Xitsonga',
	'639-1': 'ts',
	'639-2/t': 'tso',
	'639-2/b': 'tso',
	'639-3': 'tso',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Turkic',
	'name': 'Tatar',
	'native name': 'татар теле',
	'639-1': ' tatar tele',
	'639-2/t': 'tt',
	'639-2/b': 'tat',
	'639-3': 'tat',
	'639-6': 'tat',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Twi',
	'native name': 'Twi',
	'639-1': 'tw',
	'639-2/t': 'twi',
	'639-2/b': 'twi',
	'639-3': 'twi',
	'639-6': '',
	'notes': 'Covered by macro[ak/aka]'
}, {
	'family': 'Austronesian',
	'name': 'Tahitian',
	'native name': 'Reo Tahiti',
	'639-1': 'ty',
	'639-2/t': 'tah',
	'639-2/b': 'tah',
	'639-3': 'tah',
	'639-6': '',
	'notes': 'One of the Reo Mā`ohi (languages of French Polynesia)'
}, {
	'family': 'Turkic',
	'name': 'Uyghur',
	'native name': ' Uighur',
	'639-1': 'Uyƣurqə',
	'639-2/t': ' ئۇيغۇرچە‎',
	'639-2/b': 'ug',
	'639-3': 'uig',
	'639-6': 'uig',
	'notes': 'uig'
}, {
	'family': 'Indo-European',
	'name': 'Ukrainian',
	'native name': 'українська мова',
	'639-1': 'uk',
	'639-2/t': 'ukr',
	'639-2/b': 'ukr',
	'639-3': 'ukr',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Urdu',
	'native name': 'اردو',
	'639-1': 'ur',
	'639-2/t': 'urd',
	'639-2/b': 'urd',
	'639-3': 'urd',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Turkic',
	'name': 'Uzbek',
	'native name': 'O‘zbek',
	'639-1': ' Ўзбек',
	'639-2/t': ' أۇزبېك‎',
	'639-2/b': 'uz',
	'639-3': 'uzb',
	'639-6': 'uzb',
	'notes': 'uzb + 2'
}, {
	'family': 'Niger–Congo',
	'name': 'Venda',
	'native name': 'Tshivenḓa',
	'639-1': 've',
	'639-2/t': 'ven',
	'639-2/b': 'ven',
	'639-3': 'ven',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Austroasiatic',
	'name': 'Vietnamese',
	'native name': 'Tiếng Việt',
	'639-1': 'vi',
	'639-2/t': 'vie',
	'639-2/b': 'vie',
	'639-3': 'vie',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Constructed',
	'name': 'Volapük',
	'native name': 'Volapük',
	'639-1': 'vo',
	'639-2/t': 'vol',
	'639-2/b': 'vol',
	'639-3': 'vol',
	'639-6': '',
	'notes': 'constructed'
}, {
	'family': 'Indo-European',
	'name': 'Walloon',
	'native name': 'walon',
	'639-1': 'wa',
	'639-2/t': 'wln',
	'639-2/b': 'wln',
	'639-3': 'wln',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Welsh',
	'native name': 'Cymraeg',
	'639-1': 'cy',
	'639-2/t': 'cym',
	'639-2/b': 'wel',
	'639-3': 'cym',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Wolof',
	'native name': 'Wollof',
	'639-1': 'wo',
	'639-2/t': 'wol',
	'639-2/b': 'wol',
	'639-3': 'wol',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Western Frisian',
	'native name': 'Frysk',
	'639-1': 'fy',
	'639-2/t': 'fry',
	'639-2/b': 'fry',
	'639-3': 'fry',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Niger–Congo',
	'name': 'Xhosa',
	'native name': 'isiXhosa',
	'639-1': 'xh',
	'639-2/t': 'xho',
	'639-2/b': 'xho',
	'639-3': 'xho',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Indo-European',
	'name': 'Yiddish',
	'native name': 'ייִדיש',
	'639-1': 'yi',
	'639-2/t': 'yid',
	'639-2/b': 'yid',
	'639-3': 'yid + 2',
	'639-6': '',
	'notes': 'macrolanguage'
}, {
	'family': 'Niger–Congo',
	'name': 'Yoruba',
	'native name': 'Yorùbá',
	'639-1': 'yo',
	'639-2/t': 'yor',
	'639-2/b': 'yor',
	'639-3': 'yor',
	'639-6': '',
	'notes': ''
}, {
	'family': 'Tai–Kadai',
	'name': 'Zhuang',
	'native name': ' Chuang',
	'639-1': 'Saɯ cueŋƅ',
	'639-2/t': ' Saw cuengh',
	'639-2/b': 'za',
	'639-3': 'zha',
	'639-6': 'zha',
	'notes': 'zha + 16'
}, {
	'family': 'Niger–Congo',
	'name': 'Zulu',
	'native name': 'isiZulu',
	'639-1': 'zu',
	'639-2/t': 'zul',
	'639-2/b': 'zul',
	'639-3': 'zul',
	'639-6': '',
	'notes': ''
}]
var fs = require("fs");
var countries = require("./countries-data.js");
var nonExisting = [];
for(var idx in countries) {
 	var countryParts = countries[idx].split('|');
 	var fileName = countryParts[1]+'.png';
	var newFileName = countryParts[0].toLowerCase()+'.png';
 	// console.log(fileName);
	var exists = fs.existsSync(fileName);
 	// console.log(exists,newFileName);
	if (exists) {
		fs.rename(fileName, newFileName, function(err) {
			if(err) { 
				console.log(err)
			}
			else {
				console.log('completed')
			}
		});
	} else {
		nonExisting.push(countryParts[1]);
	}
}
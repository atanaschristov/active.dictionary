/*global angular*/
(function () {
	'use strict';
	var StartFrom = function ($window) {
		return {
			restrict: 'A',
			scope: {
				someAttr: '=',
			},
			link: function ($scope, el, attr) {
				// console.log('StartFrom', $scope, el, attr);
				var attrs;
				if (typeof attr.startFrom !== 'string') {
					return;
				}

				// We pass json object and JSON.parse is quite picky 
				// if the object string do not use double quotes, but single quotes 
				attr.startFrom = attr.startFrom.split('\'').join('"');

				// console.log(attr.startFrom);
				try {
					attrs = JSON.parse(attr.startFrom);
				} catch (err) {
					console.log(err);
					return;
				}

				// console.log('StartFrom attr', attrs);
				if (!attrs.selector && !attrs.selector) {
					return;
				}

				var srcEl = $(attrs.selector),
					srcElOffset = srcEl.offset(),
					win = angular.element($window);

				// console.log('StartFrom',attr.startFrom,srcElOffset,);
				// console.log('StartFrom',attr.startFrom,srcEl[0].offsetHeight);
				$scope.$watch(function () {
					// console.log(attrs.selector, srcElOffset, srcEl.outerHeight());
					return srcEl.outerHeight();
				}, function (newValue) {
					// console.log('old new heights', attrs.selector, oldValue, newValue);
					var top = attrs.realtive ? 0 : srcElOffset.top;
					// console.log('top', attrs.realtive, top);
					top += newValue;
					el.css('top', top);
					// console.log('Result:', el.css('top'));
				});

				win.bind('resize', function () {
					$scope.$apply();
				});
			}
		};
	};
	angular.module('active.dict').directive('startFrom', ['$window', StartFrom]);
})();
/*global angular*/
(function() {
	'use strict';
	var SidebarController = function($scope) {
		console.log('SidebarController',$scope);
		var hidden = false;
		$scope.$on('toggleSidebar',function(e,data) {
			console.log('toggleSidebar evnet recieved',e,data);
		});
		$scope.toggle = function() {
			hidden = !hidden;
		};
	};
	angular.module('active.dict').controller('SidebarController',['$scope',SidebarController]);
})();
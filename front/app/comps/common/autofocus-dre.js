/*global angular*/
(function () {
	'use strict';
	var appName = 'active.dict';
	// var UiselectAutofocus = function($timeout) {
	// 	return {
	// 		restrict: 'A',
	// 		require: 'uiSelect',
	// 		link: function($scope,el,attr) {
	// 			// console.log('UiselectAutofocus directive', el);
	// 			// console.log(el.hasClass('ui-select-container'));
	// 		}
	// 	};
	// };
	// angular.module(appName).directive('uiselectAutofocus',['$timeout',UiselectAutofocus]);

	var FocusUiselect = function ($timeout) {
		return {
			restrict: 'A',
			require: 'uiSelect',
			link: function ($scope, el, attr) {
				// console.log('FocusUiselect directive', el,el.hasClass('ui-select-container'));
				var doFocusUiselect = function () {
					$timeout(function () {
						var input = el.find('input');
						// console.log('UiselectAutofocus directive', attr);

						if (attr.uiselectAutofocus === 'open') {
							input.click();
						}

						input.focus();
					}, 0);
				};
				doFocusUiselect();
				$scope.$on('focusOn', function (e, name) {
					// console.log(el.hasClass('ui-select-container'));
					if (name === attr.focusEvent) {
						doFocusUiselect();
					}
				});
			}
		};
	};
	angular.module(appName).directive('focusUiselect', ['$timeout', FocusUiselect]);

	// var AutoFocus = function($timeout) {
	// 	return {
	// 		restrict: 'A',
	// 		link: function($scope,el,attr) {
	// 			$timeout(function(){
	// 				el[0].focus();
	// 				el.select();
	// 			}, 0);
	// 		}
	// 	};
	// };
	// angular.module(appName).directive('autoFocus',['$timeout',AutoFocus]);
	var ForceFocus = function () {
		return {
			restrict: 'A',
			link: function ($scope, el, attr) {
				var doFocus = function () {
					// console.log('doFocus',el[0]);
					el[0].focus();
					el.select();
				};
				// console.log('ForceFocus directive', attr);
				$scope.$on('focusOn', function (e, name) {
					// console.log('focusOn event fired',name);
					if (name === attr.forceFocus) {
						doFocus();
					}
				});
			}
		};
	};
	angular.module(appName).directive('forceFocus', [ForceFocus]);

	var FocusService = function ($rootScope, $timeout) {
		// console.log('FocusService');

		var self = {
			fire: function (name) {
				$timeout(function () {
					$rootScope.$broadcast('focusOn', name);
				});
			}
		};
		return self;
	};
	angular.module(appName).service('FocusService', ['$rootScope', '$timeout', FocusService]);
})();
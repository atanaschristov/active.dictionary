/*global angular*/
(function(){
	'use strict';
	// 'angular-local-json-generator','ui.select','angular-google-gapi'
	var app = angular.module('active.dict', ['ui.router','LocalStorageModule','ui.select','ngSanitize','googleplus','facebook','angular-local-json-generator','config']);

	// Storage configuration
	app.config(['localStorageServiceProvider',function(localStorageServiceProvider) {
		localStorageServiceProvider.setPrefix('active.dict-');
	}]);

	// app.config(['$linkedInProvider', function($linkedInProvider) {
	// 	$linkedInProvider.set('appKey', '77tr65t6g8o2ma');
	// }]);

	app.config(['FacebookProvider', 'CONFIG', function(FacebookProvider,CONFIG) {
		// console.log('FacebookProvider',CONFIG);
		if(CONFIG.fbAppId) {
			FacebookProvider.init(CONFIG.fbAppId);
		} else {
			console.log('Config: Missing fb app id');
		}
	}]);

	app.config(['GooglePlusProvider', 'CONFIG', function(GooglePlusProvider,CONFIG) {
		if(CONFIG.googleClientId) {
			GooglePlusProvider.init({
				clientId: CONFIG.googleClientId,
				apiKey: CONFIG.googleClientId.googleAppKey
			});
		} else {
			console.log('Config: Missing Google\'s clientId app apiKey');
		}
	}]);

	app.config(['$stateProvider', '$urlRouterProvider',function($stateProvider, $urlRouterProvider) {
		$stateProvider
			.state('signin', {
				url: '/signin',
				templateUrl: 'comps/signin/signin.html',
				controller: 'SigninController'
			})
			.state('languages', {
				url: '/languages',
				templateUrl: 'comps/languages/languages.html',
				controller: 'LanguagesController'
			})
			.state('languages.themes', {
				url: '/:langSlug',
				params: {
					langId: null
				},
				views: {
					'@': {
						templateUrl: 'comps/themes/themes.html',
						controller: 'ThemeController'
					}
				}
			})
			.state('languages.themes.browseWords', {
				url: '/:themeId/browse',
				params: {
					langId: null
				},
				views: {
					'@': {
						templateUrl: 'comps/words/browse/browse-words.html',
						controller: 'BrowseWordsController'
					}
				}
			})
			.state('languages.themes.testWords', {
				url: '/:themeId/test',
				params: {
					langId: null
				},
				views: {
					'@': {
						templateUrl: 'comps/words/test/test-words.html',
						controller: 'TestWordsController'
					}
				}
			});

		$urlRouterProvider.otherwise('/languages');
	}]);
})();
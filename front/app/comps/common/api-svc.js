/*global angular*/
(function() {
	'use strict';
	var ApiService = function($http,CONFIG) {
		// console.log('ApiService',CONFIG);

		var baseUrl = 'http://localhost:8181',
			baseHttpsUrl = 'https://localhost:8443',
			version = '',
			requestOptions;

		// NOTE! the CONFIG constant is defined in comps/common/config.js, 
		// which is generated automatically by grunt serve/grunt build
		// check Gruntfile.js ngconstant taskl
		if(CONFIG) {
			baseUrl = CONFIG.apiEndpoint;
			baseHttpsUrl = CONFIG.secApiEndpoint;
		}

/*
requestOptions object may include:

params – {Object.<string|Object>} – Map of strings or objects which will be turned to ?key1=value1&key2=value2 after the url. If the value is not a string, it will be JSONified.
headers – {Object} – Map of strings or functions which return strings representing HTTP headers to send to the server. If the return value of a function is null, the header will not be sent. Functions accept a config object as an argument.
xsrfHeaderName – {string} – Name of HTTP header to populate with the XSRF token.
xsrfCookieName – {string} – Name of cookie containing the XSRF token.
transformRequest – {function(data, headersGetter)|Array.<function(data, headersGetter)>} – transform function or an array of such functions. The transform function takes the http request body and headers and returns its transformed (typically serialized) version. See Overriding the Default Transformations
transformResponse – {function(data, headersGetter, status)|Array.<function(data, headersGetter, status)>} – transform function or an array of such functions. The transform function takes the http response body, headers and status and returns its transformed (typically deserialized) version. See Overriding the Default Transformations
paramSerializer - {string|function(Object):string} - A function used to prepare string representation of request parameters (specified as an object). Is specified as string, it is interpreted as function registered in with the {$injector}.
cache – {boolean|Cache} – If true, a default $http cache will be used to cache the GET request, otherwise if a cache instance built with $cacheFactory, this cache will be used for caching.
timeout – {number|Promise} – timeout in milliseconds, or promise that should abort the request when resolved.
withCredentials - {boolean} - whether to set the withCredentials flag on the XHR object. See requests with credentials for more information.
responseType - {string} - see requestType.
*/

		var self = {
			setReqOptions: function(options) {
				requestOptions = options;
			},
			getReqOptions: function() {
				return requestOptions;
			},
			setHeaders: function(headers) {
				if(!requestOptions) {
					requestOptions = {};
				}
				requestOptions.headers = headers;
			},
			setUrl: function(url,ver,secure) {
				if(typeof url === 'string') {
					if(secure) {
						baseHttpsUrl = url;
					} else {
						baseUrl = url;
					}
				}
				if(typeof ver === 'string') {
					version = ver+'/';
				}
			},
			head: function(partial,secure) {
				var url = baseUrl;
				if(secure) {
					url = baseHttpsUrl;
				}
				return $http.head(url+version+partial,requestOptions);
			},
			get: function(partial,secure) {
				var url = baseUrl;
				if(secure) {
					url = baseHttpsUrl;
				}
				return $http.get(url+version+partial,requestOptions);
			},
			delete: function(partial,data,secure) {
				var url = baseUrl;
				if(secure) {
					url = baseHttpsUrl;
				}
				if(!requestOptions) {
					requestOptions = {};
				}
				if(data) {
					requestOptions.data = data;
				}
				return $http.delete(url+version+partial,requestOptions);
			},
			post: function(partial,data,secure) {
				var url = baseUrl;
				if(secure) {
					url = baseHttpsUrl;
				}
				return $http.post(url+version+partial,data,requestOptions);
			},
			put: function(partial,data,secure) {
				var url = baseUrl;
				if(secure) {
					url = baseHttpsUrl;
				}
				return $http.put(url+version+partial,data,requestOptions);
			},
			patch: function(partial,data,secure) {
				var url = baseUrl;
				if(secure) {
					url = baseHttpsUrl;
				}
				return $http.patch(url+version+partial,data,requestOptions);
			}
		};
		return self;
	};
	angular.module('active.dict').factory('ApiService',['$http','CONFIG',ApiService]);
})();
/*global angular*/
(function () {
	'use strict';
	var MaxWidthTo = function () {
		return {
			// templateUrl: 'relativePath',
			// controller : '',
			restrict: 'A',
			scope: {
				selector: '@maxWidthTo'
			},
			link: function ($scope, el) {
				// Get parent elmenets width and subtract fixed width
				// console.log('MaxWidthTo', $scope);
				// console.log('Width of ', $scope.selector, $(el).closest($scope.selector).width());
				el.css({
					'max-width': $(el).closest($scope.selector).width() + 'px'
				});
			}
		};
	};
	angular.module('active.dict').directive('maxWidthTo', [MaxWidthTo]);
})();
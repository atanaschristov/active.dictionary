/*global angular,_*/
(function() {
	'use strict';
	var RootController = function($scope,$state,$timeout,UsersService,LanguagesService,ThemesService) {
		// console.log('RootController',$scope,LanguagesService,ThemesService);
		$scope.hideSidebar = false;
		$scope.view = 'languages';
		var staticFlow = [
			'Select or add a language',
			'Select or add a theme'
		];
		$scope.selectedLanguage=null;
		$scope.selectedTheme=null;
		if(!UsersService.getToken()) {
			$state.go('signin');			
		}

		$scope.$watch('view',function() {
			// console.log('RootController $scope.$watch view',$scope.view);
			$scope.flow = _.clone(staticFlow);
			$scope.selectedLanguage = LanguagesService.getSelected();
			$scope.selectedTheme = ThemesService.getSelected();
			switch($scope.view) {
				case 'languages':
				break;
				case 'themes':
					if($scope.selectedLanguage) {
						$scope.flow[0] = $scope.selectedLanguage.name;
					}
				break;
				case 'test':
				case 'words':
					if($scope.view === 'words') {
						$scope.flow.push('Browse Words');
					}
					if($scope.view === 'test') {
						$scope.flow.push('Test');
					}
					if($scope.selectedLanguage) {
						$scope.flow[0] = $scope.selectedLanguage.name;
					}
					// console.log('RootController $scope.selectedTheme',$scope.selectedTheme);
					if($scope.selectedTheme) {
						// console.log('words',$scope.selectedTheme);
						if($scope.selectedTheme.body.length > 50) {
							$scope.flow[1] =  $scope.selectedTheme.body.substring(0, 50)+' ...';
						} else {
							$scope.flow[1] = $scope.selectedTheme.body;
						}
					}
				break;
				default:
				break;
			}
			// console.log('RootController $scope.$watch flow',$scope.flow);
		});

		$scope.toggleSidebar = function() {
			$scope.hideSidebar = !$scope.hideSidebar;
			// console.log('$scope.hideSidebar',$scope.hideSidebar);
		};
	};
	angular.module('active.dict').controller('RootController',['$scope','$state','$timeout','UsersService','LanguagesService','ThemesService',RootController]);
})();
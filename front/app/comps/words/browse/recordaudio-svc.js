/* global angular,AudioContext,Recorder */
(function () {
	'use strict';
	angular.module('active.dict').factory('RecordAudioSvc', ['$rootScope','Base64',function ($rootScope,Base64) {
		// console.log('RecordAudioSvc',$rootScope);
		var player;

		window.AudioContext = window.AudioContext || window.webkitAudioContext;
		navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

		var browserSupport = !!(navigator.getUserMedia),
			context = new AudioContext(),
			microphone = null,recording = false,record = null	,recorder = null;

		var streamHandler = function(stream,eventData) {
			// console.log(stream);
			// console.log('streamHandler',eventData);
			if(microphone === null) {
				microphone = context.createMediaStreamSource(stream);
			}
			recorder = new Recorder(microphone,
				{
					workerPath: '../bower_components/recordmp3/js/recorderWorker.js'
				}
			);
			// TODO change the events with promises
			$rootScope.$emit('audioInitialized',eventData);
		};

		// =============================
		var init = function (eventData) {
			if(!browserSupport) {
				console.log('Your browser does not support audio recording');
				return false;
			}
			navigator.getUserMedia(
				{
					audio: true
				},function(stream) {
					streamHandler(stream,eventData);
				},function (err) { // error
					// TODO change the events with promises
					$rootScope.$emit('audioInitialized',false);
				}
			);
		};

		// =============================
		var start = function () {
			if(microphone === null) {
				console.log('input stream is not initialized');
				return false;
			}

			if(recording === false) {
				// console.log('mic start');
				if(recorder) {
					recorder.record();
					recording = true;
					return recording;
				}
			} else {
				console.log('mic already started');
			}
			return false;
		};

		// =============================
		var stop = function (feedback) {
			if(microphone === null) {
				console.log('input stream is not initialized');
				return false;
			}

			if(recording === true) {
				// console.log('mic stop');
				if(recorder) {
					recorder.stop();
					recorder.exportWAV(function (blob) {
						record = blob;
						if(typeof feedback === 'function') {
							feedback(blob);
						}
					});
					recorder.clear();
					recording = false;
					return true;
				}
			} else {
				console.log('mic already stopped');
			}
		};

		// =============================
		var getRecord = function(options) {
			return record;
		};

		// =============================
		var playBlob = function(what,data) {
			if(what instanceof Blob) {
				// console.log("Play the record");
				var url = (window.URL || window.webkitURL).createObjectURL(what);
				if(player) {
					player.pause();
				} else {
					player = new window.Audio();
				}
				player.src = url;
				player.play();
				player.onended = function(e) {
					// TODO change the events with promises
					$rootScope.$emit('audioRecordFinished',data);
					// console.log('The audio has ended',e);
				};
			} else {
				console.log('Error: Wrong audio data type');
			}
		};

		// =============================
		var playBase64 = function(what,data) {
			playBlob(Base64.decodeBlob(what,data));
		};

		// =============================
		var stopPlay = function() {
			// console.log(player);
			if(player) {
				player.pause();
				player = null;
			}
		};


		// =============================
		// var download = function(what) {
		// 	if(what instanceof Blob) {
		// 		Recorder.forceDownload(what);
		// 	} else {
		// 		console.log('Error: Wrong audio data type');
		// 	}
		// };

		// =============================
		return {
			browserSupport: browserSupport,
			init: init,
			start: start,
			stop: stop,
			playBlob: playBlob,
			playBase64: playBase64,
			stopPlay: stopPlay
			// getRecord: getRecord,
			// uploadRecord: upload,
			// downloadRecord: download
		};
	}]);
})();
/*global angular,_*/
(function () {
	'use strict';
	var BrowseWordsController = function ($rootScope, $scope, $controller, $state, $stateParams, $timeout, FocusService, WordsService, RecordAudioSvc) {
		// console.log('BrowseWordsController',$scope,$stateParams);
		$controller('WordsBaseController', {
			$scope: $scope
		}); // inherit from WordsBase controller

		$scope.$parent.view = 'words';

		$scope.activeForm = false;
		$scope.selectedWordKey = null;
		$scope.expanded = false; //TODO if the footer expands on word click

		$scope.newWord;

		$scope.recordAudioSupport = RecordAudioSvc.browserSupport;
		$scope.recording = false, $scope.mediaInit = false;
		$scope.newTranslations = {};
		$scope.detailsFlag = false;
		// console.log('recordAudioSupport:',$scope.recordAudioSupport,',mediaInit:',$scope.mediaInit);

		$scope.setFocusOnTranslateTo = function () {
			FocusService.fire('focusTranslateTo');
		};

		$scope.setFocusOnForm = function () {
			FocusService.fire('focusForm');
		};

		var prepareForRequest = function (wordItem) {
			var result, idx;
			if (!wordItem) {
				return;
			}
			result = _.clone(wordItem); // make sure we copy , so we do not impact the data
			delete result.editMode;
			delete result.globalEditModeFlag;
			for (idx in result.translations) {
				delete result.translations[idx].word.editMode;
			}
			return result;
		};

		var prepareToShow = function (wordItem) {
			if (!$scope.words || !$scope.words instanceof Array) {
				return;
			}

			var prepareTranslations = function (translations, translationCounter) {
				var idx;
				if (!translations instanceof Array) {
					return;
				}
				if (translations.length > 0) {
					for (idx in translations) {
						translations[idx].word.editMode = {
							isUpdated: false,
							word: null
						}

						if (!translationCounter[translations[idx].language]) {
							translationCounter[translations[idx].language] = 1;
						} else {
							translationCounter[translations[idx].language] += 1;
						}
					}
				}
			};

			if (wordItem) {
				wordItem.editMode = {
					word: null,
					isUpdated: false
				};
				wordItem.globalEditModeFlag = false;
				delete wordItem.globalNewTranslationFlag;
				prepareTranslations(wordItem.translations);
			} else {
				for (var idx in $scope.words) {
					if (typeof $scope.words[idx].editMode === 'undefined') {
						$scope.words[idx].globalEditModeFlag = false;
						$scope.words[idx].editMode = {
							word: null,
							isUpdated: false
						};
						$scope.words[idx].editMode.translationCounter = {}
						prepareTranslations($scope.words[idx].translations, $scope.words[idx].editMode.translationCounter);
					}
				}
			}
		};

		// The function handles the audio record processing and 
		// passing it to the server, when the recording is done
		var stopRecording = function (key) {
			$scope.audioProcessing = true;
			RecordAudioSvc.stop(function (audioData) {
				var requestData;
				// console.log('audioData',audioData);
				if (!$scope.words[key].meta) {
					$scope.words[key].meta = {};
				}
				var reader = new window.FileReader();
				reader.readAsDataURL(audioData);
				reader.onloadend = function () {
					// console.log('base64data',reader.result);
					$scope.words[key].meta.audioRecord = reader.result;
					$scope.words[key].meta.hasAudio = true;
					requestData = prepareForRequest($scope.words[key]);
					if (!requestData.updated) {
						requestData.updated = {};
					}
					requestData.updated.meta = true;
					WordsService.update(requestData).then(function (updated) {
						// console.log('updated word',updated);
						// $scope.reset(key); // reset edit mode
						prepareToShow(updated);
						$scope.words[key] = updated;
						if (updated.meta && updated.meta.audioRecord) {
							// console.log('base64data result',updated.meta.audioRecord);
							// RecordAudioSvc.playBase64(updated.meta.audioRecord); // DEBUG play what you've recorded
						}
						// console.log('before DEleteing $scope.words[key].editMode.audioProcessing',key,$scope.audioProcessing);
						delete $scope.audioProcessing;
					}, function (err) {
						console.log('Error: BrowseWordsController WordsService.get', err);
					});
				}
			});
		};

		// the function handles the start,stop recording requests as well as the recording timeout
		// If a person starts recording, the recording period is limited, to 5 seconds.
		// TODO how much time we should record
		var doRecording = function (key) {
			if ($scope.recording) {
				$scope.words[key].editMode.timoutPromise = $timeout(function () {
					// console.log('Timeout trigered',key);
					// Recording time is up
					delete $scope.words[key].editMode.timoutPromise;
					$scope.recording = false;
					stopRecording(key);
				}, 5000);
				if (RecordAudioSvc.start() === false) {
					console.log('Either not ready or recording');
					$scope.recording = false;
				}
			} else {
				// console.log('trigered Stop',key);
				// Recording is manually stoped ,so cancel the timer
				if ($scope.words[key].editMode.timoutPromise) {
					$timeout.cancel($scope.words[key].editMode.timoutPromise);
					delete $scope.words[key].editMode.timoutPromise;
				}
				stopRecording(key);
			}
		};

		// If the microfone button is clicked, 
		// 	we should initialize the device, 
		// 	start the recording and than process the audio
		$scope.triggerRecording = function (key) {
			// console.log($scope.recording,key);
			var eventData = key;
			// Do not allow 
			if ($scope.audioProcessing) {
				console.log('Wait untill the audio is processed');
				return;
			} else {
				if ($scope.recording) {
					if ($scope.selectedWordKey !== key) {
						console.log('Switch off the started recoding first');
						return;
					}
				}
			}

			$scope.recording = !$scope.recording;
			$scope.selectedWordKey = key;

			if (!$scope.mediaInit) {
				// console.log('Initialize media');
				RecordAudioSvc.init(eventData);
				return;
			}
			doRecording(key);
		};

		// The event is fired by RecordAudioSvc on init request. 
		// If the data is passed, the init is successful, if data is false, the init failed
		$rootScope.$on('audioInitialized', function (e, data) {
			$scope.$apply(function () {
				var wordArrayKey = data; // just for clarity what does that mean in the context
				// console.log('audioInitialized',wordArrayKey,$scope.mediaInit);
				if (wordArrayKey === false) {
					// Error
					$scope.mediaInit = false;
					$scope.recoding = false;
				} else {
					// Initialized not start the recording
					$scope.mediaInit = true;
					doRecording(wordArrayKey);
				}
			});
		})

		$scope.add = function () {
			var wordData = {
				word: $scope.newWord,
				languageId: $scope.selectedLanguage._id,
				themes: [$scope.theme._id]
			};

			var found = _.filter($scope.words, {
				word: $scope.newWord
			});
			// console.log('Found existing word', found);

			if (found.length > 0) {
				// console.log('The word exists');
				$scope.activeForm = false;
				return;
			}

			// console.log('add wordData',$scope.theme,wordData);
			WordsService.add(wordData).then(function (word) {
				// console.log('Saved word',word);
				if (word && word._id) {
					$scope.words.push(word);
					$scope.wordsCache.push(word);
					prepareToShow();
					$scope.activeForm = false;
				}
			}, function (err) {
				console.log('Error: BrowseWordsController WordsService.get', err);
			});
		};

		$scope.remove = function (key) {
			var params;
			// console.log('$scope.edit',key);

			params = {
				themeId: $scope.theme._id,
				_id: $scope.words[key]._id,
			};
			// console.log('WordsService $scope.remove params',params);

			WordsService.remove(params).then(function (result) {
				// console.log('$scope.remove result',result);
				if ((result.ok && result.ok === 1 && result.n > 0) ||
					result._id) {
					$scope.words.splice(key, 1);
				}
			}, function (err) {
				console.log('Error removing a word', err);
			});
		};

		$scope.update = function (wordKey) {
			// console.log('$scope.update',wordKey);
			var wordItem = $scope.words[wordKey],
				requestData,
				idx, tobeUpdated = [];
			if (!wordItem) {
				return;
			}
			// console.log('$scope.update wordItem',wordItem);
			// if the word itself is edited
			if (wordItem.editMode.isUpdated && wordItem.editMode.word !== wordItem.word) {
				// update existing word
				var found = _.filter($scope.words, {
					word: wordItem.editMode.word
				});
				if (!found.length) {
					wordItem.word = wordItem.editMode.word;
					// console.log('update word',wordItem);
					if (!wordItem.updated) {
						wordItem.updated = {};
					}
					wordItem.updated.word = true;
				}
			}

			// if translations are changed
			for (idx in wordItem.translations) {
				var pickedTranslation = wordItem.translations[idx];
				if (pickedTranslation.word.editMode.word === '') {
					// the translation should be removed
					pickedTranslation.cmd = 'unlink';
				} else if (pickedTranslation.word.editMode.isUpdated && pickedTranslation.word.editMode.word !== pickedTranslation.word.word) {
					// the translation word has changed
					tobeUpdated.push({
						id: pickedTranslation.word._id,
						newWord: pickedTranslation.word.editMode.word,
						oldWord: pickedTranslation.word.word
					});
					pickedTranslation.cmd = 'update';
					pickedTranslation.word.oldWord = pickedTranslation.word.word;
					pickedTranslation.word.word = pickedTranslation.word.editMode.word;
				}
				if (!wordItem.updated) {
					wordItem.updated = {};
				}
				wordItem.updated.translations = true;
			}

			// if new translations are added
			// console.log('$scope.update newTranslations',$scope.newTranslations);
			for (var translationLangId in $scope.newTranslations[wordItem._id]) {
				for (idx in $scope.newTranslations[wordItem._id][translationLangId]) {
					var newTranslationValue = $scope.newTranslations[wordItem._id][translationLangId][idx].translation;
					// console.log(translationLangId,newTranslationValue);
					var exists = _.some(wordItem.translations, {
						word: {
							word: newTranslationValue
						}
					});
					// console.log(exists);
					if (!exists) {
						if (!wordItem.translations) {
							wordItem.translations = [];
						}
						wordItem.translations.push({
							cmd: 'add',
							language: translationLangId,
							word: {
								word: newTranslationValue,
								languageId: translationLangId,
								themes: [$scope.theme._id]
							}
						})
					}

					if (!wordItem.updated) {
						wordItem.updated = {};
					}
					wordItem.updated.translations = true;
				}
			}
			// console.log('Before update',wordItem);
			if (wordItem.updated) {
				requestData = prepareForRequest(wordItem); // copy the ord and remove the internal data like editMode
				$scope.reset(wordKey); // reset edit mode
				// console.log('update requestData',requestData);
				WordsService.update(requestData).then(function (updated) {
					// console.log('updated word',updated);
					prepareToShow(updated);
					$scope.words[wordKey] = updated;
					$scope.wordsCache[wordKey] = _.clone(updated);

					// Update all the otherwords which has the same modified translations
					// TODO not the best way, find a better way to update
					for (var key1 in $scope.words) {
						if (parseInt(key1) !== wordKey) {
							for (var key2 in tobeUpdated) {
								_.filter($scope.words[key1].translations, function (transItem, transItemKey) {
									// console.log('Filtered item', transItem, transItemKey, ' for key ', key1, wordKey);
									if (transItem.word._id === tobeUpdated[key2].id) {
										transItem.word.word = tobeUpdated[key2].newWord;
										$scope.wordsCache[key1].translations[transItemKey].word.word = tobeUpdated[key2].newWord;
									}
								});
							}
						}
					}
					// console.log($scope.wordsCache);
				}, function (err) {
					console.log('Error: BrowseWordsController WordsService.get', err);
				});
			} else {
				// The modifications are not going to trigger update, so reset
				$scope.reset(wordKey);
			}
		};

		$scope.reset = function (wordKey) {
			prepareToShow($scope.words[wordKey]);
			delete $scope.newTranslations[$scope.words[wordKey]._id];
		}

		$scope.edit = function (context, wordKey, translationKey) {
			// console.log('$scope.edit params',context,wordKey,translationKey);
			var wordData = $scope.words[wordKey],
				translationLangKey;
			if (!wordData.editMode) {
				console.log('NotReady');
				return;
			}
			// if($scope.translateTo) {
			// 	translationLangKey = _.findKey(wordData.translations,{language: $scope.translateTo._id});
			// }
			$scope.selectedWordKey = wordKey;
			// console.log('$scope.edit wordData,translationLangKey',wordData,translationLangKey);
			if (!wordData) {
				return;
			}
			switch (context) {
			case 'word':
				wordData.editMode.isUpdated = true;
				wordData.editMode.word = wordData.word;
				FocusService.fire('focusWord');
				wordData.globalEditModeFlag = true;
				break;
			case 'translation':
				wordData.translations[translationKey].word.editMode.isUpdated = true;
				wordData.translations[translationKey].word.editMode.word = wordData.translations[translationKey].word.word;
				FocusService.fire('focusTranslation' + wordKey + translationKey);
				wordData.globalEditModeFlag = true;
				break;
			default:
				return;
			}
			// console.log('$scope.edit wordData',wordData);
		};

		$scope.addTranslation = function (wordKey) {
			var wordId = $scope.words[wordKey]._id,
				transLangId = $scope.translateTo._id;

			if (!wordId || !transLangId) {
				console.log('Unknown word and translation language');
				return;
			}
			// console.log('$scope.addTranslation translateTo',$scope.translateTo);
			// console.log('$scope.addTranslation word to be translated',$scope.words[wordKey]);
			if (!$scope.newTranslations[wordId]) {
				$scope.newTranslations[wordId] = {};
			}
			if (!$scope.newTranslations[wordId][transLangId]) {
				$scope.newTranslations[wordId][transLangId] = [];
			}
			$scope.newTranslations[wordId][transLangId].push({
				// editMode:true,
				// wordId:,
				translation: null
			});
			$scope.words[wordKey].globalNewTranslationFlag = true;
			FocusService.fire('focusTranslation' + wordKey);
			// console.log('$scope.addTranslation newTranslations',$scope.newTranslations);
		};

		$scope.delNewTranslation = function (wordKey, newTranslationKey) {
			var wordId = $scope.words[wordKey]._id,
				transLangId = $scope.translateTo._id;

			if (!wordId || !transLangId) {
				console.log('Unknown word and translation language');
				return;
			}

			$scope.newTranslations[wordId][transLangId].splice(newTranslationKey, 1);
			if ($scope.newTranslations[wordId][transLangId].length <= 0) {
				delete $scope.newTranslations[wordId][transLangId];
			}
			if (_.isEmpty($scope.newTranslations[wordId])) {
				delete $scope.newTranslations[wordId];
			}
			if (!$scope.newTranslations[wordId]) {
				delete $scope.words[wordKey].globalNewTranslationFlag;
			}
			// console.log('$scope.delNewTranslation newTranslations',$scope.newTranslations);
		};

		$scope.triggerDetails = function (wordKey) {
			$scope.detailsFlag = !$scope.detailsFlag;
		}

		// Preprae to show the elements in $scope.words, which are already created in WordsBaseController
		$scope.$on('wordsLoaded', function () {
			prepareToShow();
			// console.log('wordsLoaded', $scope.words);
		});
	};
	angular.module('active.dict').controller('BrowseWordsController', ['$rootScope', '$scope', '$controller', '$state', '$stateParams', '$timeout', 'FocusService', 'WordsService', 'RecordAudioSvc', BrowseWordsController]);
})();
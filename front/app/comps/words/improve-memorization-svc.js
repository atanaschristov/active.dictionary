/*global angular,moment*/
(function () {
	'use strict';
	var ImproveMemorizationSvc = function () {
		// console.log('ImproveMemorizationSvc');

		// The brain stores the data in a network of connected neurons. So one can access that 
		// data through one or more (ideas, thoughts). The thought triggers data access. Once the data 
		// by regularly learn and test. When the data is chunked, it is easy to be 
		// stored and accessed from the memory. THe info should be first tested often 
		// and reagular and in time the tests should be made more and more rare, but still 
		// continuous

		var self = {
			/**
			 * Caculates how often a test should be made in days
			 * @param  {Date string} whenCreated 
			 * @return {Number} days
			 */
			howOften: function (whenCreated) {
				var whenCreatedObj = moment(whenCreated),
					now = moment(),
					daysPassedFromCreation = now.diff(whenCreatedObj, 'days'),
					howOften;

				// console.log('howOften daysPassedFromCreation', daysPassedFromCreation);

				if (daysPassedFromCreation < 7) {
					howOften = 1; // every day
				} else if (daysPassedFromCreation >= 7 && daysPassedFromCreation < 30) {
					howOften = 7; // once a week
				} else if (daysPassedFromCreation >= 30) {
					howOften = 30; // once a month
				}
				// console.log('howOften howOften', howOften);

				return howOften;
			},
			/**
			 * Calculates how many days are left untill deadline
			 * @param  {Date String} whenCreated
			 * @param  {Date String} whenLastChecked
			 * @return {Number}	days 
			 */
			daysTillDeadline: function (whenCreated, whenLastChecked) {
				// console.log('daysTillDeadline params', whenCreated, whenLastChecked);
				var now = moment(),
					daysPassedFromLastTest = whenLastChecked ? now.diff(moment(whenLastChecked), 'days') : now.diff(moment(whenCreated), 'days');

				// console.log('daysTillDeadline daysPassedFromLastTest', daysPassedFromLastTest);

				return self.howOften(whenCreated) - daysPassedFromLastTest;
			},
			/**
			 * Calculates the next examination date
			 * @param  {Date string} whenCreated     
			 * @param  {Date string} whenLastChecked 
			 * @return {Date string}   Calculated date according to the when the object is created, Whether the test should be made daily,weekly or monthly
			 */
			nextExamDate: function (whenCreated, whenLastChecked) {
				var days = self.daysTillDeadline(whenCreated, whenLastChecked),
					date = whenLastChecked ? moment(whenLastChecked) : moment(whenCreated),
					nextDate = date.add(days, 'd').format();
				// console.log('nextExamDate days, next date', days, nextDate);
				return nextDate;
			},
			/**
			 * Calculates the period in which a test should be made
			 * @param  {Date string} whenCreated When the object is created
			 * @return {String} every day,week or month 
			 */
			examPeriod: function (whenCreated) {
				var howOften = self.howOften(whenCreated);
				switch (howOften) {
				case 1:
					return 'day';
				case 7:
					return 'week';
				case 30:
					return 'month';
				}
			},
			/**
			 * Returns a message, which provides tips
			 * @param  {Date string} whenCreated 
			 * @return {string}  message
			 */
			getHintMessage: function (whenCreated, whenLastChecked) {
				// console.log(self.daysTillDeadline(whenCreated, whenLastChecked));
				// console.log(moment.duration(-1, 'days').humanize());
				var msg = 'Test yourself every  ' +
					self.examPeriod(whenCreated) +
					'. The deadline is ' +
					moment.duration(self.daysTillDeadline(whenCreated, whenLastChecked), 'days').humanize();
				return msg;
			},
			/**
			 * Check weather the object has passed the test recently
			 * @param  {Date string} whenCreated
			 * @param  {Date string} whenLastChecked
			 * @return {String}  three enum like string values, which are also style classes, so don't change that
			 */
			examState: function (testsCount, whenCreated, whenLastChecked) {
				// console.log('examState params', whenCreated, whenLastChecked);
				var now = moment(),
					nextDeadline = self.nextExamDate(whenCreated, whenLastChecked),
					daysLeft = moment(nextDeadline).diff(now, 'days'),
					howOften = self.howOften(whenCreated),
					allowedDifference,
					result = '';

				// console.log('examState howOften, daysLeft', howOften, daysLeft);
				// console.log('examState daysPassedFromLastTest', daysPassedFromLastTest);

				switch (howOften) {
				case 1:
					allowedDifference = 1;
					break;
				case 7:
					allowedDifference = 3;
					break;
				case 30:
					allowedDifference = 7;
					break;
				}
				// console.log('examState allowedDifference', allowedDifference);

				if (daysLeft >= allowedDifference) {
					result = 'ok';
				} else if (daysLeft < allowedDifference && daysLeft >= 0) {
					result = 'todo';
				} else {
					result = 'missed';
				}
				return result;
			}

		};
		return self;
	};
	angular.module('active.dict').service('ImproveMemorizationSvc', [ImproveMemorizationSvc]);
})();
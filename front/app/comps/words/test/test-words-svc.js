/*global angular,_*/
(function() {
	'use strict';
	var ThemesService = function($q,ApiService) {
		// console.log('ThemesService');
		var url='/themes',
			themesCache;

		var apiRequest = function(cmd,data) {
			var deferred = $q.defer();
			console.log('ThemesService apiRequest arguments',url,cmd,data);
			switch(cmd) {
				case 'get':
					return ApiService.get(url);
				case 'add':
					if(!data) {
						deferred.reject('Error: Missing payload');
					} else {
						return ApiService.post(url,data);
					}
				break;
				case 'update':
					if(!data) {
						deferred.reject('Error: Missing payload');
					} else {
						var id = data._id;
						if(!id) {
							deferred.reject('Error: Missing index');
						} else {
							return ApiService.post(url+ '/'+id,data);
						}
					}
				break;
				case 'delete':
					if(!data) {
						deferred.reject('Error: Missing payload');
					} else {
						var urlPayload = '';
						for(var idx in data) {
							urlPayload += '/'+data[idx];
						}
						return ApiService.delete(url+urlPayload,data);
					}
				break;
				default:
					deferred.reject('Error: unrecognized request command ');
				break;
			}
			return deferred.promise;
		};

		var filterById = function(id) {
			var result;
			if(!themesCache) {
				return;
			}
			result = _.find(themesCache,{_id: id});
			console.log(result);
			return result;
		};

		var self = {
			get: function(id) {
				var deferred = $q.defer();
				if(themesCache && themesCache instanceof Array) {
					if(id) {
						deferred.resolve(filterById(id));
					} else {
						deferred.resolve(_.clone(themesCache));
					}
				} else {
					apiRequest('get').then(function (result) {
						console.log(result);
						if(result && result.data && result.data.themes) {
							themesCache = result.data.themes;
						}
						deferred.resolve(_.clone(themesCache));
					}, function(err) {
						deferred.reject('Error:', err);
						console.log('Error:', err);
					});
				}
				return deferred.promise;
			},
			add: function(params) {
				var deferred = $q.defer();
				// console.log('add params',params);
				apiRequest('add',params).then(function(result) {
					// console.log('add result',result);
					if(result && result.data) {
						themesCache.unshift(result.data);
						deferred.resolve(_.clone(result.data));
					} else {
						deferred.reject('Error: Unexpected result');
					}
				},function(err) {
					deferred.reject('Error:', err);
				});
				return deferred.promise;
			},
			update: function(params) {
				var deferred = $q.defer();
				var key = _.findKey(themesCache,{_id: params._id});
				apiRequest('update',params).then(function(result) {
					// console.log('update result',result);
					if(result && result.data) {
						if(key) {
							themesCache[key] = result.data;
						}
						deferred.resolve(_.clone(result.data));
					} else {
						deferred.reject('Error: Unexpected result');
					}
				},function(err) {
					deferred.reject('Error:', err);
				});
				return deferred.promise;
			},
			remove: function(params) {
				var deferred = $q.defer();
				apiRequest('delete',params).then(function(result) {
					// console.log('delete result',result);
					if(result && result.data) {
						deferred.resolve(_.clone(result.data));
					} else {
						deferred.reject('Error: no result');
					}
				},function(err) {
					deferred.reject('Error:', err);
				});
				return deferred.promise;
			}
		};
		return self;
	};
	angular.module('active.dict').factory('ThemesService',['$q','ApiService',ThemesService]);
})();
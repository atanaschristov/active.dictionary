/*global angular,_,moment*/
(function () {
	'use strict';
	/**
	 * TestWordsController description
	 * @param {Object} $scope
	 * @param {Object} $controller
	 * @param {Object} $state
	 * @param {Object} $stateParams
	 * @param {Object} $timeout
	 * @param {Object} FocusService
	 * @param {Object} WordsService
	 */
	var TestWordsController = function ($scope, $controller, $state, $stateParams, $timeout, FocusService, ThemesService, WordsService) {
		// console.log('TestWordsController',$scope,$stateParams);
		$controller('WordsBaseController', {
			$scope: $scope
		}); // inherit from WordsBase controller

		$scope.$parent.view = 'test';

		$scope.orderBy = ['Least known', 'Date(desc)', 'Date(asc)', 'Random'];
		$scope.selectedOrder = 'LeawordModelst known'; // TODO Store orderby

		// NOTE: this ensures that only the translated words are shown
		$scope.showAll = false;

		$scope.wordTranslationModel = {};
		$scope.test = '';

		$scope.selectOrder = function (key) {
			$scope.selectedOrder = $scope.orderBy[key];
		}

		var testFinished = function () {
			// console.log($scope.theme);
			$scope.theme.meta.tests++;
			$scope.theme.meta.beforeLastTestAt = $scope.theme.meta.lastTestAt;
			$scope.theme.meta.lastTestAt = moment().format();
			ThemesService.update($scope.theme).then(function (theme) {
				// console.log('Updated theme',theme);
				$scope.theme = theme;
				ThemesService.setSelected(theme);

				// TODO the behavior will change
				// Go to the themes immediately.  
				// $state.go('languages.themes', {
				// 	// $scope.selectedLanguage is defined in words-base-ctrl.js
				// 	langSlug: $scope.selectedLanguage.name,
				// 	langId: $scope.selectedLanguage._id
				// });
			}, function (err) {
				console.log(err);
			});
		};

		$scope.checkWord = function (wordKey) {
			var wordItem = $scope.words[wordKey],
				idx;
			// console.log('checkWord -> wordKey,translationKey',wordKey,translationKey);
			// console.log('checkWord -> translation',$scope.words[wordKey].translations[translationKey]);

			// console.log($scope.wordTranslationModel[wordKey]);
			wordItem.testMode.allCorrect = true;

			wordItem.meta.testStatistics.beforeLastTestAt = wordItem.meta.testStatistics.lastTestAt;
			wordItem.meta.testStatistics.lastTestAt = moment().format();
			for (idx in wordItem.translations) {
				var testMode = wordItem.translations[idx].testMode;
				var wordText = wordItem.translations[idx].word.word;
				var translationId = wordItem.translations[idx].word._id;

				testMode.correct = _.some($scope.wordTranslationModel[wordKey], function (item) {
					return item && item.toLowerCase() === wordText.toLowerCase();
				});
				testMode.passed = true;
				// console.log('testMode.meta',testMode);
				wordItem.testMode.allCorrect = wordItem.testMode.allCorrect && testMode.correct;
				// console.log('allCorrect',$scope.words[wordKey].testMode.allCorrect);

				// Prepare meta
				wordItem.meta.testStatistics.translations[translationId].howManyTimesPassed += 1;
				if (testMode.correct) {
					wordItem.meta.testStatistics.translations[translationId].howManyTimesCorrect += 1;
					wordItem.meta.testStatistics.translations[translationId].balance += 1;
					wordItem.meta.testStatistics.translations[translationId].correctInARow += 1;
					wordItem.meta.testStatistics.translations[translationId].incorrectInARow = 0;
				} else {
					wordItem.meta.testStatistics.translations[translationId].balance -= 1;
					wordItem.meta.testStatistics.translations[translationId].incorrectInARow += 1;
					wordItem.meta.testStatistics.translations[translationId].correctInARow = 0;
				}
			}
			wordItem.testMode.wordPassed = true;

			// console.log($scope.words[wordKey]);

			var requestData = prepareForRequest(wordItem);
			if (!requestData.updated) {
				requestData.updated = {};
			}
			requestData.updated.meta = true;
			// console.log('TestWordsController word requestData', requestData, wordItem);
			WordsService.update(requestData).then(function (updated) {
				// console.log('updated word', updated, wordItem);
				// $scope.reset(key); // reset edit mode
				// prepareToShow(updated);
				wordItem.meta = updated.meta; // update only the meta
				// $scope.words[wordKey] = updated;
				if (wordKey < ($scope.words.length - 1)) {
					$scope.words[wordKey + 1].testMode.showWordFlag = true;
				} else {
					testFinished();
				}
			}, function (err) {
				console.log('Error: TestWordsController WordsService.update', err);
			});
		}

		var prepareForRequest = function (word) {
			if (!word) {
				return;
			}

			var requestData = JSON.parse(JSON.stringify(word));

			delete requestData.testMode;
			var arrSize = word.translations.length;
			while (arrSize--) {
				delete requestData.translations[arrSize].testMode;
			};
			return requestData;
		}

		var prepareToShow = function (word) {
			var idx, zdx;
			// console.log($scope.words,$scope.wordsCache);
			var modifyWordStructure = function (wordItem) {
				wordItem.testMode = {
					showWordFlag: false,
					wordPassed: false,
					allCorrect: false
				};
				if (!wordItem.meta) {
					wordItem.meta = {};
				}
				if (!wordItem.meta.testStatistics) {
					wordItem.meta.testStatistics = {};
				}
				if (!wordItem.meta.testStatistics.translations) {
					wordItem.meta.testStatistics.translations = {};
				}
				if (!wordItem.meta.testStatistics.startAt) {
					wordItem.meta.testStatistics.startAt = moment().format();
				}
				if (typeof wordItem.meta.testStatistics.lastTestAt === 'undefined') { // it may be null, which is a valid, so I check for undefined
					wordItem.meta.testStatistics.lastTestAt = null;
				}
				if (typeof wordItem.meta.testStatistics.beforeLastTestAt === 'undefined') { // it may be null, which is a valid, so I check for undefined
					wordItem.meta.testStatistics.beforeLastTestAt = null;
				}

				var arrSize = wordItem.translations.length;
				$scope.wordTranslationModel[idx] = new Array(arrSize);
				while (arrSize--) {
					if (!wordItem.meta.testStatistics.translations[wordItem.translations[arrSize].word._id]) {
						wordItem.meta.testStatistics.translations[wordItem.translations[arrSize].word._id] = {
							howManyTimesPassed: 0, // generic counter for all time
							howManyTimesCorrect: 0, // generic counter for all time
							balance: 0, // if the tested is correct, the field will increment, otherwise decrement;
							correctInARow: 0, // how many times the translation was INCORRECT in a row, If the succession breaks, the field becomes 0
							incorrectInARow: 0 // how many times the translation was CORRECT in a row, If the succession breaks the field becomes 0
						}
					}
					wordItem.translations[arrSize].testMode = {
						passed: false,
						correct: false
					}
				};
			}

			// If we have a word passed as a parameter, just update that word, 
			// otherwise update everything
			if (word) {
				modifyWordStructure(word);
			} else {
				for (idx in $scope.words) {
					modifyWordStructure($scope.words[idx]);
				}
			}

			if (!$scope.theme.meta || !$scope.theme.meta.tests) {
				$scope.theme.meta = {
					tests: 0,
					beginTestAt: moment().format(),
					beforeLastTestAt: null,
					lastTestAt: null
				};
			}
			// console.log('prepareToShow wordTranslationModel',$scope.wordTranslationModel);
		};

		// Preprae to show the elements in $scope.words, which are already created in WordsBaseController
		$scope.$on('wordsLoaded', function () {
			// console.log('wordsLoaded', $scope.words);
			$scope.filterByTranslations($scope.translateTo._id); // remove the untranslated words
			prepareToShow(); // prepare
			$scope.words[0].testMode.showWordFlag = true; // show only the first word
			FocusService.fire('input00');
		});

	};
	angular.module('active.dict').controller('TestWordsController', ['$scope', '$controller', '$state', '$stateParams', '$timeout', 'FocusService', 'ThemesService', 'WordsService', TestWordsController]);
})();
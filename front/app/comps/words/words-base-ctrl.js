/*global angular,_,calculateSize*/
(function () {
	'use strict';
	var WordsBaseController = function ($rootScope, $scope, $state, $stateParams, UsersService, LanguagesService, ThemesService, WordsService, RecordAudioSvc) {
		// console.log('WordsBaseController',$scope,$stateParams);

		$scope._ = _;
		$scope.selectedLanguage = null;
		$scope.languages = null;
		$scope.theme = null;
		$scope.words = [];
		$scope.wordsCache = [];

		$scope.translateTo = null;

		$scope.showAll = true; // checkbox flag

		// The event is fired by RecordAudioSvc play methods, when the play has finished.
		// There are currently two play methods. Check recordaudio-svc.js
		$rootScope.$on('audioRecordFinished', function () {
			$scope.$apply(function () {
				delete $scope.audioPlaying;
				// console.log('audioRecordFinished',e,$scope.audioPlaying);
			});
		});

		// Used to update the input type text width, according the the string value
		$scope.changeInputWidth = function (context, input, initialSize) {
			var wordInputWidth = 150,
				realWidth = 0;

			if (parseInt(initialSize) === initialSize) { // check do we have integer
				wordInputWidth = initialSize;
			}

			if (typeof input === 'string') {
				// calculateSize comes from a bower plugin
				realWidth = calculateSize(input, {
					font: 'Arial',
					fontSize: '18px'
				}).width;
				// console.log(realWidth);
				if (realWidth && realWidth > 0) {
					wordInputWidth = realWidth;
				}
			}
			return wordInputWidth + 5; // 5 is empirical found shifting value
		};

		// The function handles the audio play, stop requests
		$scope.play = function (key) {
			// console.log($scope.audioPlaying,$scope.selectedWordKey,key);
			if ($scope.audioPlaying && $scope.selectedWordKey === key) {
				// console.log('$scope.play stop');
				RecordAudioSvc.stopPlay();
				delete $scope.audioPlaying;
			} else {
				$scope.selectedWordKey = key;
				var params = {
					_id: $scope.words[key]._id,
					'meta.audioRecord': 1
				};
				$scope.audioPlaying = true;
				// console.log('getWords params',params);
				WordsService.getMeta(params).then(function (result) {
					// console.log('WordsService.get() the result',result);
					RecordAudioSvc.playBase64(result.meta.audioRecord);
				}, function (err) {
					console.log(err);
				});
			}
		};

		$scope.filterByTranslations = function (langId) {
			if ($scope.showAll) {
				$scope.words = _.clone($scope.wordsCache);
			} else {
				$scope.words = _.clone(_.filter($scope.wordsCache, {
					translations: [{
						language: langId
					}]
				}));
			}
		};

		$scope.changeTranslation = function (key) {
			$scope.translateTo = $scope.languages[key];
			var filterBy = $scope.translateTo._id;
			$scope.filterByTranslations(filterBy);
			for (var idx in $scope.words) {
				if ($scope.words[idx].editMode.updatedTranslation) {
					$scope.words[idx].editMode.translations.key = _.findKey($scope.words[idx].translations, {
						language: $scope.translateTo._id
					});
					if ($scope.words[idx].editMode.translations.key) {
						$scope.words[idx].editMode.translations.language = $scope.translateTo._id;
					}
				}
			}
			// update all the 
		};

		var startWatchShowAll = function () {
			// we start the watcher after $scope.translateTo is loaded
			$scope.$watch('showAll', function () {
				// console.log($scope.translateTo);
				if ($scope.translateTo && $scope.translateTo._id) {
					var filterBy = $scope.translateTo._id;
					$scope.filterByTranslations(filterBy);
				}
			});
		};

		var getLanguages = function () {
			var langParams = {
				userId: $scope.user._id
			};
			return LanguagesService.get(langParams).then(function (languages) {
				// remove the selected Language
				var key = _.findKey(languages, {
					_id: $scope.selectedLanguage._id
				});
				$scope.selectedLanguage = languages.splice(key, 1);
				if ($scope.selectedLanguage) {
					$scope.selectedLanguage = $scope.selectedLanguage[0];
				}
				$scope.languages = languages;
				// $scope.langMenu = ['ALL'];
				// for(var idx in languages) {
				// 	$scope.langMenu.push(languages[idx].name);
				// }
				$scope.active =
					$scope.translateTo = $scope.languages[0];
				if ($scope.translateTo) {
					// If we dont have languages to translate to, don't start the watcher
					startWatchShowAll();
				}
				// console.log('WordsBaseController $scope.languages', $scope.translateTo, $scope.languages);
			}, function (err) {
				console.log('Error: WordsBaseController $scope.language', err);
			});
		};

		var getWords = function () {
			var params = {
				languageId: $scope.selectedLanguage._id,
				themes: [$scope.theme._id]
			};
			// console.log('getWords params',params);
			WordsService.get(params).then(function (result) {
				// console.log('WordsService.get() the result',result);
				$scope.wordsCache = _.clone(result);
				$scope.words = result;
				$scope.$emit('wordsLoaded');
			}, function (err) {
				console.log(err);
			});
		};

		// UsersService.resetRegistered(); // DEBUG
		UsersService.get().then(function (user) {
			// console.log('UsersService.get()',data);
			if (!user || !user._id) {
				console.log('Unknown user');
				$state.go('signin');
			}
			$scope.user = user;

			$scope.selectedLanguage = LanguagesService.getSelected();
			if (!$scope.selectedLanguage) {
				console.log('No language is selected');
				$state.go('languages');
				return;
			}

			$scope.theme = ThemesService.getSelected();
			if (!$scope.theme) {
				console.log('No selected theme');
				$state.go('languages.themes', {
					langSlug: $scope.selectedLanguage.name,
					langId: $scope.selectedLanguage._id
				});
				return;
			}
			// console.log('langParams',langParams);
			getLanguages().then(getWords).then(function () {
				// console.log('Everything loaded');
			});
		}, function (err) {
			console.log('Error ', err);
			$state.go('signin');
		});
	};
	angular.module('active.dict').controller('WordsBaseController', ['$rootScope', '$scope', '$state', '$stateParams', 'UsersService', 'LanguagesService', 'ThemesService', 'WordsService', 'RecordAudioSvc', WordsBaseController]);
})();
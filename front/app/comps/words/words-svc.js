/*global angular,_*/
(function() {
	'use strict';
	/**
	 * WordsService description
	 * @constructor
	 * @param {Object} $q - Allow using Promises
	 * @param {Object} $state - ui-route Allow redirecting 
	 * @param {Object} ApiService - Customer Service
	 * @param {Object} UsersService - Custom Service to handle user registration, and authentication
	 */
	var WordsService = function($q,$state,ApiService,UsersService) {
		// console.log('WordsService');
		var url='/words',
			wordsCache;

		var redirectUnauthorize = function(res) {
			if(res.status >400 && res.status <404) {
				$state.go('signin');
			}
		};

		var apiRequest = function(cmd,data,forcedUrl) {
			var deferred = $q.defer(),
				token = UsersService.getToken(),
				reqUrl = url,
				reqOptions,idx;

			// jshint camelcase: false
			if(!token && !token.access_token) {
				deferred.reject('Access token not provided');
				return deferred.promise;
			}
			reqOptions= {
					headers : {
						'Authorization': 'Bearer '+token.access_token
					}
			};
			// NOTE! seting or reseting reqOptions, because they are per request
			// if reqOptions is undefined, the reqOptions is reset
			ApiService.setReqOptions(reqOptions);

			if(forcedUrl) {
				reqUrl  = forcedUrl;
			}
			// console.log('WordsService apiRequest arguments',url,cmd,data);
			switch(cmd) {
				case 'get':
					if(data) {
						reqUrl += '?';
						for(var key in data) {
							if(data[key] instanceof Array) {
								reqUrl += key+'[]=';
								for(idx in data[key]) {
									reqUrl += data[key][idx]+',';
								}
								reqUrl = reqUrl.slice(0, - 1); // remove the last ','
							} else {
								reqUrl += key+'='+data[key];
							}
							reqUrl += '&';
						}
						reqUrl = reqUrl.slice(0, - 1); // remove the last '&'
					}
					// console.log('apiRequest get',reqUrl);
					return ApiService.get(reqUrl);
				case 'add':
					if(!data) {
						deferred.reject('Error: Missing payload');
					} else {
						return ApiService.post(reqUrl,data);
					}
				break;
				case 'update':
					if(!data) {
						deferred.reject('Error: Missing payload');
					} else {
						var id = data._id;
						if(!id) {
							deferred.reject('Error: Missing index');
						} else {
							return ApiService.post(reqUrl+ '/'+id,data);
						}
					}
				break;
				case 'delete':
					if(!data) {
						deferred.reject('Error: Missing payload');
					} else {
						var urlPayload = '';
						for(idx in data) {
							urlPayload += '/'+data[idx];
						}
						return ApiService.delete(reqUrl+urlPayload,data);
					}
				break;
				default:
					deferred.reject('Error: unrecognized request command ');
				break;
			}
			return deferred.promise;
		};

		// var filterById = function(id) {
		// 	var result;
		// 	if(!wordsCache) {
		// 		return;
		// 	}
		// 	result = _.find(wordsCache,{_id: id});
		// 	console.log(result);
		// 	return result;
		// };

		var self = {
			get: function(params) {
				var deferred = $q.defer();
				// if(wordsCache && wordsCache instanceof Array) {
				// 	if(id) {
				// 		deferred.resolve(filterById(id));
				// 	} else {
				// 		deferred.resolve(_.clone(wordsCache));
				// 	}
				// } else {
				// }
				apiRequest('get',params).then(function (result) {
					// console.log('WordsService get request, the result',result);
					if(result && result.data && result.data.words) {
						wordsCache = result.data.words;
					}
					deferred.resolve(_.clone(wordsCache));
				}, function(err) {
					redirectUnauthorize(err);
					deferred.reject('Error:', err);
					// console.log('Error:', err);
				});
				return deferred.promise;
			},
			add: function(params) {
				var deferred = $q.defer();
				// console.log('add params',params);
				apiRequest('add',params).then(function(result) {
					// console.log('add result',result);
					if(result && result.data) {
						wordsCache.unshift(result.data);
						deferred.resolve(_.clone(result.data));
					} else {
						deferred.reject('Error: Unexpected result');
					}
				},function(err) {
					redirectUnauthorize(err);
					deferred.reject('Error:', err);
				});
				return deferred.promise;
			},
			update: function(params) {
				var deferred = $q.defer();
				// var key = _.findKey(wordsCache,{_id: params._id});
				apiRequest('update',params).then(function(result) {
					// console.log('update result',result);
					if(result && result.data) {
						// if(key) {
						// 	wordsCache[key] = result.data;
						// }
						deferred.resolve(_.clone(result.data));
					} else {
						deferred.reject('Error: Unexpected result');
					}
				},function(err) {
					redirectUnauthorize(err);
					deferred.reject('Error:', err);
				});
				return deferred.promise;
			},
			remove: function(params) {
				var deferred = $q.defer();
				apiRequest('delete',params).then(function(result) {
					// console.log('delete result',result);
					if(result && result.data) {
						deferred.resolve(_.clone(result.data));
					} else {
						deferred.reject('Error: no result');
					}
				},function(err) {
					redirectUnauthorize(err);
					deferred.reject('Error:', err);
				});
				return deferred.promise;
			},
			getMeta: function(params) {
				var deferred = $q.defer(),
					forcedUrl = url+'/'+ params._id + '/meta';

				// console.log(forcedUrl);
				delete params._id;

				apiRequest('get',params,forcedUrl).then(function(result) {
					// console.log('delete result',result);
					if(result && result.data) {
						deferred.resolve(_.clone(result.data));
					} else {
						deferred.reject('Error: no result');
					}
				},function(err) {
					redirectUnauthorize(err);
					deferred.reject('Error:', err);
				});
				return deferred.promise;
			}
		};
		return self;
	};
	angular.module('active.dict').factory('WordsService',['$q','$state','ApiService','UsersService',WordsService]);
})();
/*global angular,_*/
(function () {
	'use strict';
	var ThemeController = function ($scope, $state, $stateParams, UsersService, FocusService, LanguagesService, ThemesService, ImproveMemorizationSvc) {
		// console.log('ThemeController',$scope,$stateParams);

		$scope.$parent.view = 'themes';
		$scope.language = null;
		$scope.themes = [];
		$scope.theme = null;
		$scope.activeForm = false;
		$scope.maxThemeLength = 130; // allowed symbols
		$scope.newTheme = {
			body: null
		};

		$scope.reset = function (key) {
			var theme = $scope.themes[key];
			$scope.newTheme.body = null;
			theme.editMode = false;
		};

		$scope.setFocusOnForm = function () {
			FocusService.fire('focusForm');
		};

		var prepareForRequest = function (theme) {
			var result;
			if (!theme) {
				return;
			}
			result = _.clone(theme); // make sure we copy , so we do not impact the data
			delete result.editMode;
			return result;
		};

		$scope.gotoTheme = function (key, theme, where) {
			var uiSrefParams = {
				langSlug: $scope.language.name,
				langId: $scope.language._id,
				themeId: theme._id
			};

			ThemesService.setSelected(theme);
			if (!$scope.themes[key].editMode) {
				$state.go('languages.themes.' + where, uiSrefParams);
			}
		};

		$scope.add = function () {
			// find if there is a theme with the same body
			// var theme = _.find($scope.themes, {body: $scope.newTheme.body});
			if ($scope.newTheme.body.length > $scope.maxThemeLength) {
				$scope.newTheme.body = $scope.newTheme.body.substring(0, $scope.maxThemeLength);
			}
			var key = _.findKey($scope.themes, {
				body: $scope.newTheme.body
			});
			var theme = $scope.themes[key];
			// console.log('Found existing theme',theme);
			if (!theme) {
				$scope.newTheme.userId = $scope.user._id;
				$scope.newTheme.languageIds = [];
				$scope.newTheme.languageIds.push($stateParams.langId);
				$scope.newTheme.meta = {
					beforeLastTestAt: null,
					lastTestAt: null,
					tests: 0
				};
				// console.log('$scope.newTheme', $scope.newTheme);
				ThemesService.add($scope.newTheme).then(function (theme) {
					if (theme) {
						$scope.themes.unshift(theme);
						prepareToShow([0]); // Update the first theme, since it is the newest added theme
						$scope.activeForm = false;
					} else {
						console.log('Error: ThemeController ThemesService.add -> no theme returned', theme);
					}
				}, function (err) {
					console.log('Error: ThemeController ThemesService.add', err);
				});
			} else {
				// the theme exists, but is the language different
				if (theme.languageIds.indexOf($stateParams.langId) === -1) {
					// if the theme exists but for different language, add the language
					theme.languageIds.push($stateParams.langId);
					var requestData = prepareForRequest(theme);
					ThemesService.update(requestData).then(function (updated) {
						$scope.themes[key] = updated;
						prepareToShow([key]);
						$scope.activeForm = false;
					}, function (err) {
						console.log('Error: ThemeController ThemesService.get', err);
					});
				}
			}
		};

		$scope.remove = function (key) {
			var params;
			// console.log('$scope.edit',key);

			// TODO pass request to the server
			params = {
				languageId: $scope.language._id,
				_id: $scope.themes[key]._id
			};
			// console.log('ThemesService $scope.remove params',params);

			ThemesService.remove(params).then(function (result) {
				// console.log('$scope.remove result', result);
				if (result.ok && result.ok === 1 && result.n > 0) {
					$scope.themes.splice(key, 1);
				} else {
					console.log(result);
				}
			}, function (err) {
				console.log('Error removing theme', err);
			});
		};

		$scope.update = function (key) {
			// console.log('$scope.edit',key);
			var theme = $scope.themes[key],
				requestData;
			if (!theme) {
				return;
			}
			theme.editMode = false;
			theme.body = $scope.newTheme.body;
			$scope.newTheme.body = null;
			requestData = prepareForRequest(theme);
			ThemesService.update(requestData).then(function (updated) {
				$scope.themes[key] = updated;
				prepareToShow();
			}, function (err) {
				console.log('Error: ThemeController ThemesService.get', err);
			});
			// console.log('updated theme,requestData',theme,requestData);
		};

		$scope.edit = function (key) {
			// console.log('$scope.edit',key);
			var theme = $scope.themes[key];
			if (!theme) {
				return;
			}
			$scope.newTheme.body = theme.body;
			FocusService.fire('justFocusHere');
			theme.editMode = true;
		};

		var handleThemeState = function (theme) {
			if (theme.meta) {
				theme.examState = ImproveMemorizationSvc.examState(theme.meta.tests, theme.createdAt, theme.meta.lastTestAt);
				theme.examMsg = ImproveMemorizationSvc.getHintMessage(theme.meta.tests, theme.createdAt, theme.meta.lastTestAt);
			}
		};

		var prepareToShow = function (keys) {
			var idx, theme;
			if (!$scope.themes || !$scope.themes instanceof Array) {
				return;
			}
			if (keys && keys instanceof Array && keys.length > 0) {
				for (idx in keys) {
					theme = $scope.themes[keys[idx]];
					if (typeof theme.editMode === 'undefined') {
						theme.editMode = false;
						handleThemeState(theme);
					}
				}
			} else {
				for (idx in $scope.themes) {
					theme = $scope.themes[idx];
					if (typeof theme.editMode === 'undefined') {
						theme.editMode = false;
						handleThemeState(theme);
					}
				}
			}
		};

		var getThemes = function () {
			var params = {
				languageIds: [$scope.language._id]
			};
			// console.log(params);
			return ThemesService.get(params).then(function (data) {
				$scope.themes = data;
				prepareToShow();
				// console.log('ThemeController $scope.themes', $scope.themes);
			}, function (err) {
				console.log('Error: ThemeController ThemesService.get', err);
			});
		};

		// UsersService.resetRegistered(); // DEBUG
		// UsersService.get().then(function(data){
		UsersService.get().then(function (user) {
			// console.log('UsersService.get()',user);
			$scope.language = LanguagesService.getSelected();
			// console.log('ThemesController Extract selected language', $scope.language);
			if (!$scope.language) {
				// console.log('No language is selected');
				$state.go('languages');
				return;
			}
			if (!$stateParams.langId) {
				$stateParams.langId = $scope.language._id;
			}
			// console.log('$stateParams',$stateParams);
			$scope.user = user;
			getThemes().then(function () {
				// console.log('Everything loaded');
			});
		}, function (err) {
			console.log('Error ', err);
			$state.go('signin');
		});

		// getLanguage.then(getThemes).then(function() {
		// 	console.log('Everything loaded');
		// });
	};
	angular.module('active.dict').controller('ThemeController', ['$scope', '$state', '$stateParams', 'UsersService', 'FocusService', 'LanguagesService', 'ThemesService', 'ImproveMemorizationSvc', ThemeController]);
})();
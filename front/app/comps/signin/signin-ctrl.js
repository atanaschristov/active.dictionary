/*global angular*/
(function() {
	'use strict';
	var SigninController = function($scope,$state,GooglePlus,Facebook,UsersService) {
		// console.log('SigninController',$scope);
		$scope.$parent.view = 'signin';
		var secondStage = function(user,where) {
			// console.log('GooglePlus.getUser()',user);
				UsersService.createToken(user).then(function(result) {
					// console.log(result);
					UsersService.setToken(result.data);
					UsersService.signinOrRegister(user,where).then(function() {
						// console.log('User signed in');
						$state.go('languages'); // go to the first page
					}, function(err){
						console.log('Error: ', err);
					});
				},function(err) {
					console.log(err);
				});
		};
		$scope.authenticate = function(where) {
			// console.log('$scope.authenticate', where);
			switch(where) {
				case 'google':
					GooglePlus.login().then(function () { //authResult) {
						// console.log(authResult);
						GooglePlus.getUser().then(function (user) {
							secondStage(user,where);
						});
					}, function (err) {
						console.log(err);
					});
				break;
				case 'twitter':
					// TODO
				break;
				case 'linkedin':
					// TODO
				break;
				case 'facebook':
					Facebook.login(function() {// result) {
						// console.log(result);
						Facebook.api('/me', {fields: 'id,gender,link,name,picture,locale'},function(user) {
							// console.log('Facebook.api',user);
							user.picture = user.picture.data.url;
							secondStage(user,where);
						});
					});
				break;
				default:
				break;
			}
		};

		// In the case that a person has multiple accounts(google,facebook), 
		// we need to force reset his local cached credentials in order to be able 
		// to signin with another account
		UsersService.resetToken();
		UsersService.resetRegistered();
	};
	angular.module('active.dict').controller('SigninController',['$scope','$state','GooglePlus','Facebook','UsersService',SigninController]);
})();
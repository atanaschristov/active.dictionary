/*global angular,_*/
(function() {
	'use strict';
	var UsersService = function($q,localStorageService,Base64,ApiService) {
		// console.log('UsersService');
		var url = '/user',
			currentUser,
			localStore = 'registered',
			token;

		var apiRequest = function(cmd,data,forcedUrl,reqOptions) {
			var deferred = $q.defer(),
					reqUrl,
					idx;
			// console.log('ThemesService apiRequest arguments',url,cmd,data);
			
			// NOTE! seting or reseting reqOptions, because they are per request
			// if reqOptions is undefined, the reqOptions is reset
			ApiService.setReqOptions(reqOptions);

			reqUrl = url;
			if(forcedUrl) {
				reqUrl  = forcedUrl;
			}
			switch(cmd) {
				case 'get':
					if(data) {
						reqUrl += '?';
						for(var key in data) {
							if(data[key] instanceof Array) {
								reqUrl += key+'[]=';
								for(idx in data[key]) {
									reqUrl += data[key][idx]+',';
								}
								reqUrl = reqUrl.slice(0, - 1); // remove the last ','
							} else {
								reqUrl += key+'='+data[key];
							}
							reqUrl += '&';
						}
						reqUrl = reqUrl.slice(0, - 1); // remove the last '&'
					}
					// console.log('apiRequest get',reqUrl);
					return ApiService.get(reqUrl);
				case 'post':
					if(!data) {
						deferred.reject('Error: Missing payload');
					} else {
						return ApiService.post(reqUrl,data); // returns a promise
					}
				break;
				case 'put':
					if(!data) {
						deferred.reject('Error: Missing payload');
					} else {
						return ApiService.put(reqUrl,data); // returns a promise
					}
				break;
				case 'delete':
					if(!data) {
						deferred.reject('Error: Missing payload');
					} else {
						var urlPayload = '';
						for(idx in data) {
							urlPayload += '/'+data[idx];
						}
						return ApiService.delete(reqUrl+urlPayload,data);
					}
				break;
				default:
					deferred.reject('Error: unrecognized request command ');
				break;
			}
			return deferred.promise;
		};

		var majorReset = function() {
			// TODO Send event for the services to reset their storage
			// LanguagesService.reset();
			// ThemesService.reset();
		};

		var self = {
			setToken: function(newToken) {
				token = newToken;
				localStorageService.set('accessToken',newToken);
			},
			getToken: function() {
				return localStorageService.get('accessToken');
			},
			resetToken: function() {
				token = undefined;
				localStorageService.remove('accessToken');				
			},
			resetRegistered: function() {
				currentUser = undefined;
				localStorageService.remove(localStore);
			},
			get: function(data) {
				var deferred = $q.defer(),
					token = self.getToken(),
					reqOptions;

				// jshint camelcase: false
				if(!token || !token.access_token) {
					deferred.reject('No access token');
					return deferred.promise;
				}
				reqOptions = {
					headers : {
						'Authorization': 'Bearer '+self.getToken().access_token
					}
				};
				if(currentUser) {
					if(data && data.from && data.id && currentUser.from !== data.from && currentUser.id !== data.id) {
						apiRequest('get',data,undefined,reqOptions).then(function(result) {
							if(result && result.data) {
								result = result.data;
							}
							if(result && result.user && typeof result.user.length !== 'undefined') {
								currentUser = result.user[0];
							}
							deferred.resolve(_.clone(currentUser));
						},
						function(err){
							deferred.reject(err);
						});
					} else {
						deferred.resolve(_.clone(currentUser));
					}
				} else {
					if(!data) {
						data = 	localStorageService.get(localStore);
					}
					if(data) {
						// console.log('UsersService apiRequest(\'get\',data)',data);
						apiRequest('get',data,undefined,reqOptions).then(function(result) {
							if(result && result.data) {
								result = result.data;
							}
							if(result && result.user && typeof result.user.length !== 'undefined') {
								currentUser = result.user[0];
							}
							deferred.resolve(_.clone(currentUser));
						},
						function(err){
							deferred.reject(err);
						});
					} else {
						deferred.reject('Not registered');
					}
				}
				return deferred.promise;
			},
			createToken: function() {
				// jshint camelcase: false
				var deferred = $q.defer();	
					// TODO Improve and change app credentials
					var data = {
						'grant_type':'client_credentials'
					};
					var reqOptions= {
						headers : {
							'Authorization': 'Basic '+Base64.encodeFromStr('officialApiClient:th!is1isn0tAPASS'),
						}
					};
					apiRequest('post',data,'/token',reqOptions).then(function(result) {
						// console.log('createToken result',result);
						deferred.resolve(_.clone(result));
					},
					function(err){
						deferred.reject(err);
					});
				return deferred.promise;				
			},
			signinOrRegister: function(user,where) {
				var deferred = $q.defer(),
					token = self.getToken(),
					data,reqOptions;

				// jshint camelcase: false
				if(!token && !token.access_token) {
					deferred.reject('Access token not provided');
					return deferred.promise;
				}
				reqOptions= {
						headers : {
							'Authorization': 'Bearer '+token.access_token
						}
				};

				data = {
					id: user.id,
					from: where
				};
				self.get(data).then(function(result) {
					if(result) {
						// user is registered, just return it
						var localData = localStorageService.get(localStore);
						if(!localData || !localData.from || !localData.id) {
							localStorageService.set(localStore,data);
							majorReset();
						}
						if(localData && localData.from && localData.id && data.from!==localData.from && data.id!==localData.id) {
							localStorageService.set(localStore,data);
							majorReset();
						}
						deferred.resolve(result); // already cloned from the get request
					} else {
						// register user and return it
						data.name =  user.name;
						data.gender =  user.gender;
						data.link =  user.link;
						data.locale =  user.locale;
						data.picture =  user.picture;
						apiRequest('put',data,undefined,reqOptions).then(function(result) {
							if(result && result.data) {
								result = result.data;
							}

							if(result.n > 0 && result.ok === 1) {
								if(result.upserted) {
									data._id = result.upserted[0]._id;
								}
								currentUser = data;
								localStorageService.set(localStore,{id: data.id,from: data.from});
								majorReset();
								deferred.resolve(_.clone(currentUser));
							} else {
								deferred.reject(result);
							}
						},
						function(err){
							deferred.reject(err);
						});
					}
				},function(err) {
					deferred.reject(err);
				});
				return deferred.promise;
			},
			unregister: function() {
				var deferred = $q.defer();
				deferred.reject('TODO');
				return deferred.promise;
			}
		};
		return self;
	};
	angular.module('active.dict').factory('UsersService',['$q','localStorageService','Base64','ApiService',UsersService]);
})();
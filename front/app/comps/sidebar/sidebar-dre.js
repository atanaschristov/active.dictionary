/*global angular*/
(function() {
	'use strict';
	var sidebar = function() {
		return {
			templateUrl: 'comps/sidebar/sidebar.html',
			controller : 'SidebarController',
			restrict:'E',
			link: function($scope,el,attr) {
				console.log('sidebar',$scope,el,attr);
			}
		};
	};
	angular.module('active.dict').directive('sidebar',[sidebar]);
})();
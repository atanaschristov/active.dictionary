/*global angular*/
(function() {
	'use strict';
	var HomeController = function($scope, JsonGenerator) {
		console.log('HomeController',$scope);
		$scope.translations = [];

		var model = {
			config: {rows:50},
			model: {
				from: {
					lnaguage: {type: 'country',format: 'abbr'},
					word: {type: 'text',length: 1},
				},
				to: {
					lnaguage: {type: 'country',format: 'abbr'},
					word: {	type: 'text',	length: 1},
				}
			}
		};
		JsonGenerator.generateData(model).then(function(data){
			console.log('JsonGenerator ',data);
			$scope.translations = data;
		},function(err) {
			console.log('JsonGenerator ',err);
		});

	};
	angular.module('active.dict').controller('HomeController',['$scope','JsonGenerator',HomeController]);
})();
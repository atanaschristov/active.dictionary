/*global angular*/
(function() {
	'use strict';
	var search = function() {
		return {
			templateUrl: 'comps/search/search.html',
			controller : 'SearchController',
			restrict:'E',
			scope: {
				data: '=',
				by: '@'
			},
			link: function(/*$scope,el,attr*/) {
				// $scope[what] = $scope.$parent[what];
				// console.log('search',$scope,el,attr);
			}
		};
	};
	angular.module('active.dict').directive('search',[search]);
})();
/*global angular,_*/
(function() {
	'use strict';
	var SearchController = function($scope,$timeout,FocusService) {
		console.log('SearchController',$scope);
		$scope.searchText='';
		$scope.toggleSearch=false;
		$timeout(function() {
			$scope.cache = _.clone($scope.data);
		},0);
		$scope.forceFocus = function() {
			if($scope.toggleSearch) {
				FocusService.fire('searchForm');
			}
		};

		var filterData = function() {
			console.log('SearchController',$scope.searchText);
			if($scope.searchText && $scope.searchText !== '') {
				$scope.data = _.filter($scope.cache, function(obj) {
					return obj[$scope.by].indexOf($scope.searchText) > -1;
				});
			} else {
				if($scope.cache) {
					$scope.data = $scope.cache.slice();
				}
			}
			console.log($scope.data);
		};

		var filterTextTimeout = null;
		$scope.$watch('searchText',function() {
			// console.log(val,filterTextTimeout);
			if (filterTextTimeout) {
				// console.log('canceled');
				$timeout.cancel(filterTextTimeout);
				filterTextTimeout = $timeout(filterData, 250); // delay 250 ms
			} else {
				filterTextTimeout = $timeout(filterData, 250); // delay 250 ms
			}
		});
	};
	angular.module('active.dict').controller('SearchController',['$scope','$timeout','FocusService',SearchController]);
})();
/*global angular,_*/
(function() {
	'use strict';
	var LanguagesService = function($q,$state,localStorageService,UsersService,ApiService) {
		// console.log('LanguagesService');
		var url = '/languages',
			languages;

		var redirectUnauthorize = function(res) {
			if(res.status >400 && res.status <404) {
				$state.go('signin');
			}
		};

		var apiRequest = function(cmd,data,forcedUrl) {
			var deferred = $q.defer(),
				token = UsersService.getToken(),
				reqUrl = url,
				reqOptions;

			// console.log('apiRequest token',token);
			// jshint camelcase: false
			if(!token && !token.access_token) {
				deferred.reject('Access token not provided');
				return deferred.promise;
			}
			reqOptions= {
					headers : {
						'Authorization': 'Bearer '+token.access_token
					}
			};
			// NOTE! seting or reseting reqOptions, because they are per request
			// if reqOptions is undefined, the reqOptions is reset
			ApiService.setReqOptions(reqOptions);
			// console.log('Language apiRequest requestOptions',ApiService.getReqOptions());

			if(forcedUrl) {
				reqUrl  = forcedUrl;
			}
			switch(cmd) {
				case 'get':
					// Process the request in order to update the local cached languges
					if(data) {
						reqUrl += '?';
						for(var key in data) {
							reqUrl += key+'='+data[key]+'&';
						}
						reqUrl = reqUrl.slice(0, - 1); // remove the last '&'
					}
					// console.log('apiRequest get Url',reqUrl);
					return ApiService.get(reqUrl);
				case 'add':
					if(!data) {
						deferred.reject('Can not add a language if no language is given');
					} else {
						return ApiService.post(reqUrl,data);
					}
					break;
				case 'delete':
					if(!data) {
						deferred.reject('Can not add a language if no language is given');
					} else {
						var urlPayload = '';
						for(var idx in data) {
							urlPayload += '/'+data[idx];
						}
						return ApiService.delete(reqUrl+urlPayload);
					}
					break;
				default: 
					deferred.reject('Unknown cmd for the request');
					break;
			}
			return deferred.promise;
		};

		var filterById = function(id) {
			var result;
			if(!languages) {
				return;
			}
			result = _.find(languages,{_id: id});
			// console.log(result);
			return result;
		};

		var self = {
			setSelected: function(language) {
				localStorageService.set('selectedLanguage', language);
			},
			getSelected: function() {
				return localStorageService.get('selectedLanguage');
			},
			delSelected: function() {
				localStorageService.remove('selectedLanguage');
			},
			get: function(params) {
				var deferred = $q.defer();
				apiRequest('get',params).then(function(result) {
					if(result && result.data && result.data.languages) {
						// cache the result so we can later use it
						languages = result.data.languages;
					}
					if(params && params._id) {
						deferred.resolve(filterById(params._id));
					} else {
						deferred.resolve(_.clone(languages));
					}
				},function(err) {
					redirectUnauthorize(err);
					deferred.reject(err);
				});
				return deferred.promise;
			},
			add: function(language) {
				var deferred = $q.defer();
				apiRequest('add',language).then(function(result) {
					if(result.data && result.data.family && result.data.name) {
						languages.push(result.data);
					} 
					deferred.resolve(_.clone(result));
				},function(err) {
					redirectUnauthorize(err);
					deferred.reject(err);
				});
				return deferred.promise;
			},
			remove: function(params) {
				var deferred = $q.defer();
				var key = _.findKey(languages,{_id: params._id});
				apiRequest('delete',params).then(function(result){
					if(result.data && result.data.ok && result.data.ok === 1 && result.data.n > 0 && key) {
						languages.splice(key,1);							
					} 
					deferred.resolve(result);
				},function(err) {
					redirectUnauthorize(err);
					deferred.reject(err);
				});
				return deferred.promise;
			},
			reset: function() {
				languages = undefined;
			}
		};
		return self;
	};
	angular.module('active.dict').factory('LanguagesService',['$q','$state','localStorageService','UsersService','ApiService',LanguagesService]);
})();
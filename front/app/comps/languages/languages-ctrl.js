/*global _,angular,globalLanguages,globalCountries*/
(function () {
	'use strict';
	var LanguagesController = function ($scope, $state, UsersService, FocusService, LanguagesService) {
		// console.log('LanguagesController',$scope);
		$scope.$parent.view = 'languages';
		$scope.activeForm = false;
		$scope.allLanguages = [];
		if (globalLanguages) {
			$scope.allLanguages = globalLanguages;
		}
		$scope.languages = [];
		$scope.newLanguage = {};
		$scope.setFocus = function () {
			FocusService.fire('justFocusHere');
		};

		$scope.langSelect = function (language) {
			// console.log('Selected language', language);
			LanguagesService.setSelected(language);
			$state.go('languages.themes', {
				'langSlug': language.name,
				langId: language._id
			});
			// ui-sref="languages.themes({'langSlug': language['name'], langId: language._id})"
		};

		var handleAllFlags = function (globalCountries) {
			if (!globalCountries) {
				return [];
			}
			var missingFlags = ['aq', 'gf', 'tf', 'gp', 'hm', 'mq', 'nc', 're', 'sj', 'tk', 'um', 'eh'];
			globalCountries = _.transform(globalCountries, function (transformed, val) {
				val = val.split('|');
				var obj = {
					name: val[1],
					code: val[0],
					flagUrl: '/data/flags/' + val[0].toLowerCase() + '.png'
				};
				var flagMissing = _.some(missingFlags, function (code) {
					return code === val[0].toLowerCase();
				});
				if (!flagMissing) {
					transformed.push(obj);
					return obj;
				}
			});
			// console.log('handleAllFlags',globalCountries);
			return globalCountries;
		};
		$scope.allCountries = handleAllFlags(globalCountries);
		$scope.flag = {};

		$scope.add = function () {
			// console.log('LanguagesController add language',$scope.newLanguage.selected);
			// console.log('LanguagesController add country',$scope.flag.selected);
			var key = _.findKey($scope.languages, {
				name: $scope.newLanguage.selected.name
			});
			var language = $scope.languages[key];
			// console.log('Does the lang exists',$scope.languages,$scope.newLanguage.selected,language,key);

			if (!language) {
				var params = _.clone($scope.newLanguage.selected);
				params.userId = $scope.user._id;
				// $scope.newLanguage.selected = {}; // reset
				delete params.$$hashKey;
				if ($scope.flag.selected) {
					params.flagUrl = $scope.flag.selected.flagUrl;
				}


				// console.log('LanguagesController params',params);
				LanguagesService.add(params).then(function (msg) {
					// console.log('LanguagesController msg',msg);
					if (msg.data && msg.data.family && msg.data.name) {
						$scope.languages.push(msg.data);
						$scope.activeForm = false;
					}
				}, function (err) {
					console.log('Error adding language', err);
				});
			}
		};

		$scope.remove = function (key) {
			var lang = $scope.languages[key],
				params;
			// console.log('LanguagesController $scope.remove lang', lang);

			if (!lang) {
				return;
			}

			params = {
				_id: lang._id
			};
			// console.log('LanguagesController $scope.remove params', params);

			LanguagesService.remove(params).then(function (result) {
				// console.log('LanguagesService.remove', result);
				if (result.data && result.data.ok && result.data.ok === 1 && result.data.n > 0) {
					$scope.languages.splice(key, 1);
				}
			}, function (err) {
				console.log('Error removing language', err);
			});
		};

		// UsersService.resetRegistered(); // DEBUG
		// UsersService.get().then(function(data){
		UsersService.get().then(function (user) {
			// console.log('UsersService.get()',user);
			// LanguagesService.delSelected(); // DEBUG
			if (user) {
				$scope.user = user;
				var params = {
					userId: $scope.user._id
				};
				LanguagesService.get(params).then(function (result) {
					// console.log('LanguagesService.get ', result);
					$scope.languages = result;
				}, function (err) {
					console.log('LanguagesService.get err ', err);
				});
			}
		}, function (err) {
			console.log('Error ', err);
			$state.go('signin');
		});

	};
	angular.module('active.dict').controller('LanguagesController', ['$scope', '$state', 'UsersService', 'FocusService', 'LanguagesService', LanguagesController]);
})();
#!/bin/bash
export base=`pwd`
# echo $base
echo "---------------------------"
echo "DEPLOY the client's app"
echo "---------------------------"
cd $base/front; grunt deploy;
echo "---------------------------"
echo "DEPLOY the backend service"
echo "---------------------------"
cd $base/service; grunt deploy;

/*jshint expr: true*/ // This should allow 'expect.to.be.true' types of expressions
/*global appRoot,sinon,expect,it,describe,before,beforeEach,after*/
(function () {
	'use strict';
	var proxyquire = require('proxyquire').noPreserveCache(), // Helps us inject our stubed objects and methods
		sinonPromise = require('sinon-promise'),
		ThemeModel = require(appRoot + '/comps/themes/theme-model.js'),
		StubbedWordCtrl = require(appRoot + '/comps/words/word-ctrl.js'),
		dummyTheme = {
			'body': 'Test theme',
			'userId': 'd5a51395ab87e6f1d6e51c4b',
			'languageIds': ['55b2725dddc3619647d79098', '55b2725dddc3619647d79099'],
			'meta': {},
			'createdAt': '2015-07-24T17:14:05.317Z'
		},
		ThemeCtrl = proxyquire(appRoot + '/comps/themes/theme-ctrl.js', {
			'./theme-model.js': ThemeModel, // stubbed mongoose model
			'q': sinonPromise.Q, // modified Q for immediate invocation
			'../words/word-ctrl.js': StubbedWordCtrl
		});
	sinonPromise(sinon);

	describe('Theme controller: test-theme-ctrl.js ', function () {
		describe('get ', function () {
			var stubbedFind, filter = {
					_id: '55b2725dddc3619647d79098',
				},
				resultThemes = [dummyTheme],
				mockFind = {
					find: function () {
						return this;
					},
					sort: function () {
						return this;
					},
					exec: function (callback) {
						callback(null, resultThemes);
					}
				};

			before(function () {
				stubbedFind = sinon.stub(ThemeModel, 'find').returns(mockFind);
			});
			after(function () {
				stubbedFind.restore();
			});
			it('should resolve', function () {
				var successSpy = sinon.spy(function (result) {
						expect(result).to.be.an('object').and.to.have.any.key('themes');
						expect(result.themes).to.be.an('Array').and.to.be.eql(resultThemes);
					}),
					rejectedSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if rejected
					});
				return ThemeCtrl.get(filter).then(successSpy, rejectedSpy);
			});
			it('should be rejected: no themes', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if rejected
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.be.string('No themes');
					});
				mockFind.exec = function (callback) {
					callback(null, null);
				};
				return ThemeCtrl.get(filter).then(successSpy, rejectedSpy);
			});
			it('should be rejected: mongoose error', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if rejected
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.be.string('Mongoose error');
					});
				mockFind.exec = function (callback) {
					callback('Mongoose error', null);
				};
				return ThemeCtrl.get(filter).then(successSpy, rejectedSpy);
			});
		});
		describe('add ', function () {
			var stubbedSave,
				payloadCopy = JSON.parse(JSON.stringify(dummyTheme));
			payloadCopy._id = 'cccccccccccccccc';
			before(function () {
				stubbedSave = sinon.stub(ThemeModel.prototype, 'save');
			});
			after(function () {
				stubbedSave.restore();
			});
			it('should resolve', function () {
				var successSpy = sinon.spy(function (result) {
						expect(result).to.be.an('object').and.to.have.any.key('_id');
						expect(result._id).to.be.string('cccccccccccccccc');
					}),
					rejectedSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if rejected
					});
				stubbedSave.yields(null, payloadCopy);
				return ThemeCtrl.add(dummyTheme).then(successSpy, rejectedSpy);
			});
			it('should be rejected: missing payload', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.have.string('Error: the query is badly formatted');
					});

				return ThemeCtrl.add().then(successSpy, rejectedSpy);
			});
			it('should be rejected: Mongoose error', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).be.an.instanceof(Error);
						expect(err.message).to.have.string('Mongoose error');
					});
				stubbedSave.yields('Mongoose error', null);
				return ThemeCtrl.add(dummyTheme).then(successSpy, rejectedSpy);
			});
		});
		describe('update ', function () {
			var stubbedUpdate, payloadCopy;
			before(function () {
				stubbedUpdate = sinon.stub(ThemeModel, 'findOneAndUpdate');
			});
			after(function () {
				stubbedUpdate.restore();
			});
			beforeEach(function () {
				payloadCopy = JSON.parse(JSON.stringify(dummyTheme));
				payloadCopy._id = 'cccccccccccccccc';
			});
			it('should resolve', function () {
				var successSpy = sinon.spy(function (result) {
						// console.log(result);
						expect(result).to.be.eql(payloadCopy); // fail if rejected
					}),
					rejectedSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if rejected
					});
				stubbedUpdate.yields(null, payloadCopy);
				return ThemeCtrl.update(payloadCopy).then(successSpy, rejectedSpy);
			});
			it('should be rejected: missing data', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.have.string('Error: the query is badly formatted');
					});
				return ThemeCtrl.update().then(successSpy, rejectedSpy);
			});
			it('should be rejected: missing _id', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.have.string('Error: the query is badly formatted');
					});
				return ThemeCtrl.update(dummyTheme).then(successSpy, rejectedSpy);
			});
			it('should be rejected: missing body', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.have.string('Error: the query is badly formatted');
					});
				payloadCopy.body = '';
				// delete payloadCopy.body;
				return ThemeCtrl.update(payloadCopy).then(successSpy, rejectedSpy);
			});
			it('should be rejected: mongoose error', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if rejected
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.have.string('Mongoose error');
					});
				stubbedUpdate.yields('Mongoose error', null);
				return ThemeCtrl.update(payloadCopy).then(successSpy, rejectedSpy);
			});
		});
		describe('remove ', function () {
			var stubbedRemove, stubbedFindOne, stubbedFindOneAndUpdate,
				stubbedWordGetter, filter, callbackResult, id = 'cccccccccccccccc';

			var createStubbedFindOne = function (data, err) {
				if (!stubbedFindOne || !stubbedFindOne.isSinonProxy) {
					return;
				}

				stubbedFindOne.returns({
					findOne: function () {
						return this;
					},
					exec: function (callback) {
						// console.log('stubbedFindOne called');
						callback(err, data);
					}
				});
			};

			var createStubbedFindOneAndUpdate = function (languageId, data, err) {
				if (!stubbedFindOne || !stubbedFindOne.isSinonProxy) {
					return;
				}

				// stubbedFindOneAndUpdate should simulate the removal of filter.languageId from the 
				// theme's languageIds and storage in the database, the result should be 
				// the new theme object without the removed language id
				stubbedFindOneAndUpdate.returns({
					findOneAndUpdate: function () {
						return this;
					},
					exec: function (callback) {
						var dataCopy = JSON.parse(JSON.stringify(data));
						dataCopy.languageIds.splice(dataCopy.languageIds.indexOf(languageId), 1);
						// console.log('stubbedFindOneAndUpdate called');
						callback(err, dataCopy);
					}
				});
			};

			beforeEach(function () {
				callbackResult = JSON.parse(JSON.stringify(dummyTheme));
				callbackResult._id = id;
				filter = {
					_id: id,
					languageId: '55b2725dddc3619647d79099'
				};
			});
			before(function () {
				stubbedRemove = sinon.stub(ThemeModel, 'remove');
				stubbedFindOne = sinon.stub(ThemeModel, 'findOne');

				// stubbedFindOneAndUpdate is used by detachLanguage private method to pull out the 
				// selected langId from the mongoose database
				stubbedFindOneAndUpdate = sinon.stub(ThemeModel, 'findOneAndUpdate');

				// get words for the theme if exists, thus we controll the behaviour
				stubbedWordGetter = sinon.stub(StubbedWordCtrl, 'get', sinon.promise());
				stubbedWordGetter.resolves(null);
			});
			after(function () {
				stubbedRemove.restore();
				stubbedFindOne.restore();
				stubbedFindOneAndUpdate.restore();
				stubbedWordGetter.restore();
			});
			it('should resolve and detach language', function () {
				// NOTE In the case when there are other languages using the theme, 
				// that is dummyTheme.lanlanguageIds.length > 1, the remove request 
				// should not delete the theme, but rather detach the language from it

				var successSpy = sinon.spy(function (result) {
						// console.log('successSpy', result);
						expect(result).to.be.an('object');
						expect(result.ok).to.be.eq(1);
						expect(result.msg).to.be.string('Language detached'); // ensures that findOneAndUpdate was called
					}),
					rejectedSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if rejected
					});

				createStubbedFindOneAndUpdate(filter.languageId, callbackResult);
				createStubbedFindOne(callbackResult);

				// stubbedRemove should never be called
				// TODO how to test that

				return ThemeCtrl.remove(filter).then(successSpy, rejectedSpy);
			});
			it('should resolve and delete language', function () {
				// If there is only one languages using the theme, 
				// that is dummyTheme.lanlanguageIds.length === 1, 
				// the remove request should delete the theme

				var successSpy = sinon.spy(function (result) {
						expect(result).to.be.an('object');
						expect(result.ok).to.be.eq(1);
						expect(result.msg).to.be.string('Language removed'); // ensures that removed was called
					}),
					rejectedSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if rejected
					}),
					callbackResultCopy = JSON.parse(JSON.stringify(callbackResult));

				// just remove the first language from the dummy data 
				callbackResultCopy.languageIds.splice(
					callbackResultCopy.languageIds.indexOf('55b2725dddc3619647d79098'), 1
				);

				// if called, make findOne to return the modified dummy data, so that we simulate query success
				createStubbedFindOne(callbackResultCopy);

				// if called, make make remove return success object
				stubbedRemove.yields(null, {
					ok: 1,
					n: 1,
					msg: 'Language removed'
				});

				// stubbedFindOneAndUpdate should never be called
				// TODO how to test that

				return ThemeCtrl.remove(filter).then(successSpy, rejectedSpy);
			});
			it('should be rejected: missing or incorrect parameters', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.have.string('Error: the query is badly formatted');
					});
				return ThemeCtrl.remove().then(successSpy, rejectedSpy);
			});
			it('should be rejected: mongoose error in findOne', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.have.string('mongoose error');
					});
				createStubbedFindOne(null, 'mongoose error');
				return ThemeCtrl.remove(filter).then(successSpy, rejectedSpy);
			});
			it('should be rejected: theme is not found', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.have.string('Theme is not found');
					});
				createStubbedFindOne(null, null);
				return ThemeCtrl.remove(filter).then(successSpy, rejectedSpy);
			});
			it('should be rejected: contains words', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.be.an('object');
						expect(err.ok).to.be.eq(0);
						expect(err.msg).to.be.string('The theme contains words'); // ensures that removed was called
					});

				createStubbedFindOne(callbackResult);
				stubbedWordGetter.resolves({
					words: new Array(1) // simulate a word
				});

				return ThemeCtrl.remove(filter).then(successSpy, rejectedSpy);
			});
			it('should be rejected: words get request is rejected', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.be.string('Word getter error'); // ensures that removed was called
					});

				stubbedWordGetter.rejects('Word getter error');

				return ThemeCtrl.remove(filter).then(successSpy, rejectedSpy);
			});
			it('should be rejected: mongoose error when findOneAndUpdate', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.be.string('findOneAndUpdate mongoose error'); // ensures that removed was called
					});

				stubbedWordGetter.resolves(null);
				createStubbedFindOneAndUpdate(filter.languageId, callbackResult, 'findOneAndUpdate mongoose error');

				return ThemeCtrl.remove(filter).then(successSpy, rejectedSpy);
			});
			it('should be rejected: mongoose error when remove', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.be.string('remove mongoose error'); // ensures that removed was called
					}),
					callbackResultCopy = JSON.parse(JSON.stringify(callbackResult));

				// just remove the first language from the dummy data 
				callbackResultCopy.languageIds.splice(
					callbackResultCopy.languageIds.indexOf('55b2725dddc3619647d79098'), 1
				);

				createStubbedFindOne(callbackResultCopy);
				// createStubbedFindOneAndUpdate(filter.languageId, callbackResult);
				stubbedRemove.yields('remove mongoose error', null);
				return ThemeCtrl.remove(filter).then(successSpy, rejectedSpy);
			});
		});
	});
})();
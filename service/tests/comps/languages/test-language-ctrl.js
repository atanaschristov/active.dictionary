/*jshint expr: true*/ // This should allow 'expect.to.be.true' types of expressions
/*global appRoot,sinon,expect,it,describe,before,after*/
(function () {
	'use strict';

	var proxyquire = require('proxyquire').noPreserveCache(), // Helps us inject our stubed objects and methods
		sinonPromise = require('sinon-promise'),
		LanguageModel = require(appRoot + '/comps/languages/language-model.js'),
		StubbedThemeCtrl = require(appRoot + '/comps/themes/theme-ctrl.js'),
		// Test and make sure that the mongose model is reloaded when required
		// delete require.cache[require.resolve(appRoot + '/comps/languages/language-model.js')];
		// delete mongoose.connection.models['Language'];
		LangCtrl = proxyquire(appRoot + '/comps/languages/language-ctrl.js', {
			'./language-model.js': LanguageModel, // stubbed mongoose model
			'q': sinonPromise.Q, // modified Q for immediate invocation
			'../themes/theme-ctrl.js': StubbedThemeCtrl
		});
	sinonPromise(sinon);


	describe('Language controller: test-language-ctrl.js ', function () {
		describe('get ', function () {
			var filter = {
				userId: 'd5a51395ab87e6f1d6e51c4b',
				_id: '55b2725dddc3619647d79098',
			};
			before(function () {
				sinon.stub(LanguageModel, 'find', function (data, callback) {
					// console.log('sinon stub is called');
					callback(null, data);
				});
				// console.log(LanguageModel.find);
			});
			after(function () {
				LanguageModel.find.restore();
			});

			it('should resolve', function (done) {
				var successSpy = sinon.spy(),
					rejectedSpy = sinon.spy(function (err) {
						console.log(err);
					});
				// console.log(LangCtrl.getLanguage().find);

				LangCtrl.get(filter).then(successSpy, rejectedSpy);
				expect(successSpy.calledOnce).to.be.true;
				expect(rejectedSpy.calledOnce).to.be.false;
				done();
			});

			it('should have correct result', function () {
				var successSpy = sinon.spy(function (result) {
						expect(result).to.have.all.keys('languages');
						expect(result.languages).to.eql(filter);
					}),
					rejectedSpy = sinon.spy(function () {
						expect(false).to.be.true; // if reject is called it must fail
					});
				return LangCtrl.get(filter).then(successSpy, rejectedSpy);
			});
			it('should be rejected: Failed to get the languages', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // if reject is called it must fail
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.be.string('Failed to get the languages');
					});
				return LangCtrl.get().then(successSpy, rejectedSpy);
			});
			it('should be rejected: bad format', function (done) {
				var successSpy = sinon.spy(),
					rejectedSpy = sinon.spy();
				// console.log(LangCtrl.getLanguage().find);

				LangCtrl.get('string').then(successSpy, rejectedSpy);
				expect(successSpy.calledOnce).to.be.false;
				expect(rejectedSpy.calledOnce).to.be.true;

				LangCtrl.get(23).then(successSpy, rejectedSpy);
				expect(successSpy.calledOnce).to.be.false;
				expect(rejectedSpy.callCount).to.be.equal(2);

				LangCtrl.get([]).then(successSpy, rejectedSpy);
				expect(successSpy.calledOnce).to.be.false;
				expect(rejectedSpy.callCount).to.be.equal(3);
				done();
			});
			it('should be rejected: mongoose error', function (done) {
				var successSpy = sinon.spy(),
					rejectedSpy = sinon.spy();

				LanguageModel.find.restore();
				sinon.stub(LanguageModel, 'find', function (data, callback) {
					callback('Mongoose error', null);
				});

				LangCtrl.get(filter).then(successSpy, rejectedSpy);
				expect(successSpy.calledOnce).to.be.false;
				expect(rejectedSpy.calledOnce).to.be.true;
				done();
			});
		});
		describe('add ', function () {
			var stubbedSave, payload = {
				'639 - 1': 'český jazyk',
				'639 - 2 / b': 'ces',
				'639 - 2 / t': 'cs',
				'639 - 3': 'cze',
				'639 - 6': 'ces',
				family: 'Indo-European',
				flagUrl: '/data/flags/cz.png',
				name: 'Czech',
				'native name': 'čeština',
				notes: '',
			};
			var payloadCopy = JSON.parse(JSON.stringify(payload)); // copy the payload
			payloadCopy._id = '55b2725dddc3619647d79098';
			payloadCopy.userId = 'd5a51395ab87e6f1d6e51c4b';
			payloadCopy.createdAt = '2015-07-24T17:14:05.317Z';
			before(function () {
				stubbedSave = sinon.stub(LanguageModel.prototype, 'save');
			});
			after(function () {
				stubbedSave.restore();
			});
			it('should resolve', function () {
				stubbedSave.yields(null, payloadCopy);
				var successSpy = sinon.spy(function (result) {
						// console.log('resolved', result);
						expect(result).to.be.an('object');
						expect(result).to.have.any.keys('_id', 'userId', 'createdAt');
						expect(result._id).to.have.string('55b2725dddc3619647d79098');
					}),
					rejectedSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if reject is called
					});

				return LangCtrl.add(payload).then(successSpy, rejectedSpy);
			});

			it('should be rejected without payload', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.have.string('Error: the query is badly formatted');
					});

				return LangCtrl.add().then(successSpy, rejectedSpy);
			});

			it('should be rejected mongoose error', function () {
				stubbedSave.yields('Mongoose error', null);
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).be.an.instanceof(Error);
						expect(err.message).to.have.string('Mongoose error');
					});
				return LangCtrl.add(payloadCopy).then(successSpy, rejectedSpy);
			});
		});
		describe('remove ', function () {
			var getThemesStub, stubbedRemove,
				filter = {
					userId: 'd5a51395ab87e6f1d6e51c4b',
					_id: '55b2725dddc3619647d79098',
				};
			before(function () {
				getThemesStub = sinon.stub(StubbedThemeCtrl, 'get', sinon.promise());
				stubbedRemove = sinon.stub(LanguageModel, 'remove');
			});
			after(function () {
				getThemesStub.restore();
				stubbedRemove.restore();
			});
			it('should resolve', function () {
				var afterRemove = {
					ok: 1,
					n: 1
				};
				var successSpy = sinon.spy(function (result) {
						expect(result).to.be.eql(afterRemove);
					}),
					rejectedSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if rejected
					});
				getThemesStub.resolves(null);
				stubbedRemove.yields(null, afterRemove);

				return LangCtrl.remove(filter).then(successSpy, rejectedSpy);
			});
			it('should be rejected: the query is badly formatted', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.be.string('Error: the query is badly formatted');
					});

				return LangCtrl.remove().then(successSpy, rejectedSpy);
			});
			it('should be rejected: there are themes', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.be.eql({
							ok: 0,
							msg: 'The language contains themes'
						});
					});

				getThemesStub.resolves({
					themes: new Array(1)
				});
				return LangCtrl.remove(filter).then(successSpy, rejectedSpy);
			});
			it('should be rejected: the themes get request is rejected', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.be.string('Theme get request rejected');
					});

				getThemesStub.rejects('Theme get request rejected');
				return LangCtrl.remove(filter).then(successSpy, rejectedSpy);
			});
			it('should be rejected: mongoose remove returns error', function () {
				var successSpy = sinon.spy(function () {
						expect(false).to.be.true; // fail if resolved
					}),
					rejectedSpy = sinon.spy(function (err) {
						expect(err).to.have.string('Mongoose error');
					});

				getThemesStub.resolves(null);
				stubbedRemove.yields('Mongoose error', null);
				return LangCtrl.remove(filter).then(successSpy, rejectedSpy);
			});
		});
	});
})();
(function () {
	'use strict';

	var chai = require('chai');
	// chai.config.includeStack = true;

	// NOTE: An alternative would be to install app-root-path module
	global.appRoot = process.env.PWD;
	// console.log(appRoot);

	global.sinon = require('sinon');

	// Set all assertion mechanisms globally 
	global.expect = chai.expect;
	global.AssertionError = chai.AssertionError;
	global.Assertion = chai.Assertion;
	global.assert = chai.assert;

	// run tests for all modules
	require('./comps/languages/test-language-ctrl.js');
	require('./comps/themes/test-theme-ctrl.js');
})();
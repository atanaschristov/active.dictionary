// Generated on 2015-03-12 using
// generator-webapp 0.5.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// If you want to recursively match all subfolders, use:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// Load grunt tasks automatically
	require('load-grunt-tasks')(grunt);

	grunt.loadNpmTasks('grunt-contrib-jshint');

	// Configurable paths
	var config = {
		app: '.',
		docs: 'docs',
		dist: '../../prod/activedict',
		version: null
	};

	// Define the configuration for all the tasks
	grunt.initConfig({

		// Project settings
		config: config,

		nodemon: {
			dev: {
				script: 'server.js',
				options: {
					args: ['development'],
					nodeArgs: ['--debug', '--expose-gc'],
				}
			}
		},
		// Watches files for changes and runs tasks based on the changed files
		watch: {
			files: ['<%= config.app %>/**/*.js'],
			tasks: ['jshint']
		},

		// Empties folders to start fresh
		clean: {
			options: {
				force: true
			},
			docs: {
				files: [{
					dot: true,
					src: ['<%= config.docs %>/*']
				}]
			},
			dist: {
				files: [{
					dot: true,
					src: [
						'.tmp',
						'<%= config.dist %>/*',
						'!<%= config.dist %>/.git*',
						'!<%= config.dist %>/.openshift*'
					]
				}]
			},
			server: '.tmp'
		},

		// Make sure code styles are up to par and there are no obvious mistakes
		jshint: {
			options: {
				jshintrc: '<%= config.app %>/.jshintrc',
				reporter: require('jshint-stylish'),
				node: true
			},
			all: [
				'Gruntfile.js',
				'<%= config.app %>/comps/**/*.js',
				'<%= config.app %>/tests/**/*.js'
			]
		},
		jsdoc: {
			dist: {
				src: ['<%= config.app %>/comps/**/*.js'],
				options: {
					// configure: '.jsdocrc',
					// template: './node_modules/jsdoc-oblivion/template',
					destination: 'docs'
				}
			}
		},
		mochaTest: {
			test: {
				options: {
					reporter: 'spec',
					bail: true, // stop tesing if error occurs
					// captureFile: 'results.txt', // Optionally capture the reporter output to a file 
					// quiet: false, // Optionally suppress output to standard out (defaults to false) 
					// clearRequireCache: false // Optionally clear the require cache before running tests (defaults to false) 
				},
				src: ['tests/go.js']
			}
		},

		// uglify: {
		//   dist: {
		//     files: {
		//       '<%= config.dist %>/comps/scripts.js': [
		//         '<%= config.dist %>/comps/scripts.js'
		//       ]
		//     }
		//   }
		// },
		// concat: {
		//   dist: {}
		// },

		// Copies remaining files to places other tasks can use
		copy: {
			dist: {
				files: [{
						expand: true,
						dot: true,
						cwd: '<%= config.app %>',
						dest: '<%= config.dist %>',
						src: [
							'!<%= config.app %>/node_modules/**',
							'<%= config.app %>/*.pem',
							'<%= config.app %>/*.txt',
							'<%= config.app %>/*.md',
							'<%= config.app %>/*.js',
							'!<%= config.app %>/Gruntfile*',
							'<%= config.app %>/*.json',
							'<%= config.app %>/comps/**',
							'<%= config.app %>/config/**'
						]
					},
					// {
					// 	src: '<%= config.app %>/node_modules/apache-server-configs/dist/.htaccess',
					// 	dest: '<%= config.dist %>/.htaccess'
					// }
				]
			}
		},

		// Run some tasks in parallel to speed up build process
		concurrent: {
			server: ['watch', 'nodemon'],
			test: [
				// TASKS
			],
			dist: [
				// TASKS
			],
			options: {
				logConcurrentOutput: true
			}
		},
		shell: {
			getGitTags: {
				command: 'git describe --abbrev=0 --tags',
				// command: 'git rev-parse --short HEAD',
				options: {
					execOptions: {
						cwd: '<%= config.dist %>'
					},
					callback: function (err, stdout, stderr, cb) {
						var versionParts = stdout.replace('\n', '').split('.');
						// console.log(versionParts);
						versionParts[versionParts.length - 1]++;
						config.version = versionParts.join('.');
						// console.log(config);
						cb();
					}
				}
			}
		},
		exec: {
			gitSetTag: {
				cwd: '<%= config.dist %>',
				cmd: function () {
					// console.log(config.version);
					if (!config.version) {
						return;
					}
					return 'git tag -a ' + config.version + ' -m "Deploy tag at `date`"';
				}
			},
			npmInstall: {
				cwd: '<%= config.dist %>',
				cmd: 'npm install --production',
			},
			gitcommit: {
				cwd: '<%= config.dist %>',
				cmd: 'git add . -A && git commit -a -m "Deploy commit at `date`"',
			},
			gitpullpush: {
				cwd: '<%= config.dist %>',
				cmd: 'git pull && git push && git push --tags',
			},
			// app: {
			// 	gitcommit: {
			// 		cwd: '<%= config.app %>',
			// 		cmd: 'git add . -A && git commit -a -m "Deploy commit at `date`"',
			// 	},
			// 	gitpullpush: {
			// 		cwd: '<%= config.app %>',
			// 		cmd: 'git pull && git push',
			// 	},
			// }
		},
		// json_generator: {
		// 	app: {
		// 		dest: '<%= config.app %>/comps/common/config.json', // Destination file 
		// 		options: {
		// 			requestOrigin: 'localhost:9000'
		// 		}
		// 	},
		// 	dist: {
		// 		dest: '<%= config.dist %>/comps/common/config.json', // Destination file 
		// 		options: {
		// 			requestOrigin: 'adfront-tdev.rhcloud.com'
		// 		}
		// 	}
		// }

	});


	grunt.registerTask('serve', 'start the server and preview your app, --allow-remote for remote access', function (target) {
		if (grunt.option('allow-remote')) {
			grunt.config.set('connect.options.hostname', '0.0.0.0');
		}
		if (target === 'dist') {
			return grunt.task.run(['build', 'connect:dist:keepalive']);
		}

		grunt.task.run([
			// 'json_generator:app',
			// 'clean:server',
			'jshint',
			'mochaTest',
			'concurrent:server',
			// 'connect:livereload',
			// 'nodemon',
			'watch',
		]);
	});

	grunt.registerTask('server', function (target) {
		grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
		grunt.task.run([target ? ('serve:' + target) : 'serve']);
	});

	grunt.registerTask('test', function (target) {
		if (target !== 'watch') {
			grunt.task.run([
				// 'clean:server',
				// 'concurrent:test',
			]);
		}

		grunt.task.run([
			'jshint',
			'mochaTest'
		]);
	});

	grunt.registerTask('gendocs', function () {
		grunt.task.run([
			'clean:docs',
			'jsdoc:dist'
		]);
	});

	grunt.registerTask('build', [
		'clean:dist',
		'test',
		// 'gendocs',
		// 'useminPrepare',
		// 'concurrent:dist',
		// 'concat',
		// 'uglify',
		'copy:dist',
		// 'json_generator:dist',
		// 'usemin'
	]);

	grunt.registerTask('deploy', function ( /*targetr*/ ) {
		// if (target !== 'watch') {
		// 	grunt.task.run([
		// 		// 'test',
		// 	]);
		// }

		grunt.task.run([
			'build',
			'shell:getGitTags',
			'exec:gitcommit',
			'exec:gitSetTag',
			'exec:gitpullpush'
		]);
	});

	grunt.registerTask('default', [
		// 'newer:jshint',
		'test',
		'build'
	]);
};
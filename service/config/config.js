module.exports = (function(){
	switch(process.env.NODE_ENV){
		case 'development':
			// console.log('Development Environment')
			var dbName = 'activedict',
				mongoHost='mongodb://localhost/';

			return {
				db: mongoHost+dbName,
				requestOrigin: 'http://localhost:9000',
				facebook: {
					clientID: process.env.FACEBOOK_CLIENTID,
					clientSecret: process.env.FACEBOOK_SECRET,
					callbackURL: "http://localhost:3000/auth/facebook/callback"
				},
				twitter: {
					clientID: process.env.TWITTER_CLIENTID,
					clientSecret: process.env.TWITTER_SECRET,
					callbackURL: "http://localhost:3000/auth/twitter/callback"
				},
				github: {
					clientID: process.env.GITHUB_CLIENTID,
					clientSecret: process.env.GITHUB_SECRET,
					callbackURL: 'http://localhost:3000/auth/github/callback'
				},
				linkedin: {
					clientID: process.env.LINKEDIN_CLIENTID,
					clientSecret: process.env.LINKEDIN_SECRET,
					callbackURL: 'http://localhost:3000/auth/linkedin/callback'
				},
				google: {
					clientID: process.env.GOOGLE_CLIENTID,
					clientSecret: process.env.GOOGLE_SECRET,
					callbackURL: "http://localhost:3000/auth/google/callback"
				}
			};

		case 'production':
			// console.log('Production Environment')
			var dbName = 'activedict',
				mongoHost,
				requestOrigin;

			// NOTE! We may have multiple production environment, 
			// so make sure to define mongoHost the chosen production
			if(process.env.OPENSHIFT_MONGODB_DB_URL){
				mongoHost = process.env.OPENSHIFT_MONGODB_DB_URL;
			}

			requestOrigin = 'http://adfront-tdev.rhcloud.com';

			return {
				db: mongoHost+dbName,
				requestOrigin: requestOrigin
			};

		default:
			console.log('Unknown environment');
			return {
			};
	}
})();
'use strict';
// var mongoose = require('mongoose');
var UserController = require('../users/user-ctrl.js'),
	LanguageController = require('../languages/language-ctrl.js'),
	ThemeController = require('../themes/theme-ctrl.js'),
	WordController = require('../words/word-ctrl.js'),
	config = require('../../config/config.js');
module.exports = function (server) {
	// console.log(server);
	// console.log(server.router.routes);

	var validateRequest = function (req) {
		var passed = true;
		// console.log('req.headers.origin',req.headers.origin);
		// console.log(req.route.method,req.route.path,'Check validateRequest authorization',req.headers.authorization);
		// console.log('config.requestOrigin',config.requestOrigin);
		// Check allowed origins
		if (req.headers.origin !== config.requestOrigin) {
			console.log('rejected request origin');
			passed = false;
		}

		// Check if Authorization header exists
		if (!req.headers.authorization) {
			console.log('Missing authorization headers');
			passed = false;
		}
		// console.log('TRUE');
		return passed;
	};

	server.on('after', function ( /*request, response, route, error*/ ) {
		// console.log(route.spec.path, error);
		if (global.gc) {
			global.gc();
		} else {
			console.log('Garbage collection unavailable.  Pass --expose-gc ' +
				'when launching node to enable forced garbage collection.');
		}
	});

	server.put('/user', function (req, res, next) {
		// console.log('PUT /user',req);
		if (!validateRequest(req)) {
			return res.sendUnauthorized();
		}
		UserController.upsert(req, res, function (user) {
			// console.log('Route -> upserted',user);
			res.send(user);
		});
		return next();
	});

	server.get('/user', function (req, res, next) {
		// console.log('GET /user',req, res, next);
		// console.log('Route -> GET',req);
		if (!validateRequest(req)) {
			return res.sendUnauthorized();
		}
		UserController.get(req.params).then(function (user) {
			// console.log('Route -> GET',user);
			res.send(user);
		}, function (err) {
			res.send(err);
		});
		return next();
	});

	server.get('/languages', function (req, res, next) {
		// console.log('GET /languages',req, res, next);
		// console.log('GET /languages',req.params);
		// validateUser(req,res);
		if (!validateRequest(req)) {
			return res.sendUnauthorized();
		}

		LanguageController.get(req.params).then(function (languages) {
			// console.log('Route -> GET',languages);
			res.send(languages);
		}, function (err) {
			res.send(err);
		});
		return next();
	});

	server.post('/languages', function (req, res, next) {
		// console.log('Route -> POST /languages',req.params);
		if (!validateRequest(req)) {
			return res.sendUnauthorized();
		}
		LanguageController.add(req.params).then(function (newLanguage) {
			// console.log('Route -> POST Success saving the data: ',newLanguage);
			res.send(newLanguage);
		}, function (err) {
			res.send(err);
		});
		return next(); // call the callback if any
	});

	server.del('/languages/:_id', function (req, res, next) {
		// console.log('Route -> DELETE /languages',req.params);
		if (!validateRequest(req)) {
			return res.sendUnauthorized();
		}
		LanguageController.remove(req.params).then(function (result) {
			// console.log('Route -> POST Success saving the data: ',newLanguage);
			res.send(result);
		}, function (err) {
			res.send(err);
		});
		return next(); // call the callback if any
	});

	server.get('/themes', function (req, res, next) {
		// console.log('GET /themes',req, res, next);
		// console.log('GET /themes params',req.params);
		if (!validateRequest(req)) {
			return res.sendUnauthorized();
		}
		ThemeController.get(req.params).then(function (themes) {
			// console.log('Route -> GET',themes);
			res.send(themes);
		}, function (err) {
			res.send(err);
		});
		return next();
	});

	// create a theme
	server.post('/themes', function (req, res, next) {
		// console.log('Route -> POST /themes',req.params);
		if (!validateRequest(req)) {
			return res.sendUnauthorized();
		}
		ThemeController.add(req.params).then(function (newTheme) {
			// console.log('Route -> POST Success saving the data: ',newTheme);
			res.send(newTheme);
		}, function (err) {
			res.send(err);
		});
		return next(); // call the callback if any
	});

	// partially update a theme
	server.post('/themes/:_id', function (req, res, next) {
		// console.log('Route -> POST /themes',req.params);
		if (!validateRequest(req)) {
			return res.sendUnauthorized();
		}
		ThemeController.update(req.params).then(function (newTheme) {
			// console.log('Route -> POST Success saving the data: ',newTheme);
			res.send(newTheme);
		}, function (err) {
			res.send(err);
		});
		return next(); // call the callback if any
	});

	server.del('/themes/:languageId/:_id', function (req, res, next) {
		// console.log('Route -> DELETE /themes',req.params);
		if (!validateRequest(req)) {
			return res.sendUnauthorized();
		}
		ThemeController.remove(req.params).then(function (removed) {
			// console.log('Route -> DELETE Success removing data: ',removed);
			res.send(removed);
		}, function (err) {
			// console.log('Error: ',err);
			res.send(err);
		});
		return next(); // call the callback if any
	});

	server.get('/words', function (req, res, next) {
		// console.log('GET /words', req, res, next);
		console.log('GET /words params', req.params);
		if (!validateRequest(req)) {
			return res.sendUnauthorized();
		}
		WordController.get(req.params).then(function (words) {
			// console.log('Route -> GET',words);
			res.send(words);
		}, function (err) {
			res.send(500, err);
		});
		return next();
	});

	server.get('/words/:_id/meta', function (req, res, next) {
		// console.log('GET /words',req, res, next);
		// console.log('GET /words params',req.params);
		if (!validateRequest(req)) {
			return res.sendUnauthorized();
		}
		WordController.getMeta(req.params).then(function (meta) {
			// console.log('Route -> GET',meta);
			res.send(meta);
		}, function (err) {
			res.send(500, new Error(err));
		});
		return next();
	});

	// create a word with partial data
	server.post('/words', function (req, res, next) {
		// console.log('Route -> POST /words',req.params);
		if (!validateRequest(req)) {
			return res.sendUnauthorized();
		}
		WordController.add(req.params).then(function (newWord) {
			// console.log('Route -> POST Success saving the data: ',newWord);
			res.send(newWord);
		}, function (err) {
			res.send(500, new Error(err));
		});
		return next(); // call the callback if any
	});

	// partially update a word
	server.post('/words/:_id', function (req, res, next) {
		// console.log('Route -> POST /words/:id',req.params);
		if (!validateRequest(req)) {
			return res.sendUnauthorized();
		}
		WordController.update(req.params).then(function (newWord) {
			// console.log('Route -> POST Success saving the data: ',newWord);
			res.send(newWord);
		}, function (err) {
			// console.log('Route -> POST Error: ',err);
			res.send(500, new Error(err));
		});
		return next(); // call the callback if any
	});

	server.del('/words/:themeId/:_id', function (req, res, next) {
		// console.log('Route -> DELETE /words',req.params);
		if (!validateRequest(req)) {
			return res.sendUnauthorized();
		}
		// console.log('Route -> DELETE Passed validation');
		WordController.remove(req.params).then(function (removed) {
			// console.log('Route -> DELETE Success removing data: ',removed);
			res.send(removed);
		}, function (err) {
			res.send(500, new Error(err));
		});
		return next(); // call the callback if any
	});

	return {
		listAllRoutes: function () {
			// Debug list all the routes
			if (server) {
				console.log('GET paths:');
				var routes = {};
				server.router.routes.GET.forEach(function (value) {
					console.log(value.spec.path);
				});
				console.log('PUT paths:');
				server.router.routes.PUT.forEach(function (value) {
					console.log(value.spec.path);
				});
				console.log(routes);
			}
		}
	};
};
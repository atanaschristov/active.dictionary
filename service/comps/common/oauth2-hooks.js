(function() {
	'use strict';

	var _ = require('lodash'),
		crypto = require('crypto'),
		moment = require('moment');
		// config = require('./config/config.js');

	var database = {
		clients: {
			unofficialClient: { secret: 'DECAF' },
			officialApiClient: {secret: 'th!is1isn0tAPASS'}
		},
		tokensToClientIds: {},
		expiresIn: undefined
	};

	function generateToken(data) {
		var random = Math.floor(Math.random() * 100001);
		var timestamp = (new Date()).getTime();
		var sha256 = crypto.createHmac('sha256', random + 'WOO' + timestamp);

		return sha256.update(data).digest('base64');
	}

	exports.setExpirationperiod = function(periodSeconds) {
		database.expiresIn = periodSeconds;
	};

	exports.grantClientToken = function (credentials, req, cb) {
		// console.log('grantClientToken',credentials,database);
		var isValid = _.has(database.clients, credentials.clientId) &&
					  database.clients[credentials.clientId].secret === credentials.clientSecret;
		if (isValid) {
			// If the client authenticates, generate a token for them and store it so `exports.authenticateToken` below
			// can look it up later.

			var token = generateToken(credentials.clientId + ':' + credentials.clientSecret);
			database.tokensToClientIds[token] = {
				clientId: credentials.clientId,
				expiresAt: moment().add(database.expiresIn, 'seconds')
			};
			// console.log('Now',moment().format());
			// console.log('expiresAt',database.tokensToClientIds[token].expiresAt.format());

			// Call back with the token so Restify-OAuth2 can pass it on to the client.
			return cb(null, token);
		}

		// Call back with `false` to signal the username/password combination did not authenticate.
		// Calling back with an error would be reserved for internal server error situations.
		cb(null, false);
	};

	exports.authenticateToken = function (token, req, cb) {
		// console.log('authenticateToken',token,database);
		var granted = false;

		if (_.has(database.tokensToClientIds, token)) {
			// console.log('check now to expiresAt',moment().unix(),database.tokensToClientIds[token].expiresAt.unix());
			granted = true;
			if(moment().unix() >= database.tokensToClientIds[token].expiresAt.unix()) {
				console.log('Token expired');
				granted = false;
			}

			if(granted) {
				// The token is used before it expires so update the validity period
				database.tokensToClientIds[token].expiresAt = moment().add(database.expiresIn, 'seconds');
				// console.log('The new exipration date is', database.tokensToClientIds[token].expiresAt.format());

				// If the token authenticates, set the corresponding property on the request, and call back with `true`.
				// The routes can now use these properties to check if the request is authorized and authenticated.
				req.clientId = database.tokensToClientIds[token].clientId;
				// console.log('authenticateToken approved for '+ req.getUrl().pathname);				
			}
		}

		// granted = true; // DEBUG

		// If the token does not authenticate, call back with `false` to signal that.
		// Calling back with an error would be reserved for internal server error situations.
		cb(null, granted);
	};
})();
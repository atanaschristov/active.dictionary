(function () {
	'use strict';

	var Theme = require('./theme-model.js'),
		WordController = require('../words/word-ctrl.js'),
		Q = require('q');

	exports.get = function (filter) {
		var deferred = Q.defer(),
			result;
		if (filter.languageIds && filter.languageIds.length && filter.languageIds.length > 0) {
			filter.languageIds = {
				$in: filter.languageIds
			};
		}
		// console.log('Theme filter',filter);
		Theme.find(filter).sort({
			'createdAt': 'desc'
		}).exec(function (err, themes) {
			// console.log('Theme get result',err, themes);
			if (err) {
				deferred.reject(err);
			}
			if (!themes) {
				// return next(new Error('Failed to load User ' + id));
				deferred.reject('No themes');
			}
			// req.result = themes;
			result = {
				themes: themes
			};
			// console.log('ThemeController get result',result);
			deferred.resolve(result);
		});
		return deferred.promise;
	};

	exports.add = function (payload) {
		var deferred = Q.defer(),
			themeModel;

		if (!payload) {
			deferred.reject('Error: the query is badly formatted');
			return deferred.promise;
		}
		themeModel = new Theme(payload);
		// console.log('ThemeController add newTheme', newTheme);
		// console.log('ThemeController add model instance',themeModel);
		themeModel.save(function (err, theme) {
			// saves it in the database
			if (err) {
				// console.log('ThemeController add error', err,theme);
				deferred.reject(err);
			}
			// console.log('ThemeController add call the next callback');
			deferred.resolve(theme);
		});
		return deferred.promise;
	};

	// .update({_id: contact.id}, upsertData, {upsert: true}, function(err{...});
	exports.update = function (payload) {
		var deferred = Q.defer(),
			id,
			newData;
		// console.log('ThemeController update payload', payload);

		if (!payload || typeof payload._id !== 'string' ||
			typeof payload.body !== 'string' || payload.body === '') {
			deferred.reject('Error: the query is badly formatted');
			return deferred.promise;
		}
		if (typeof payload.meta !== 'object') {
			payload.meta = null;
		}

		id = payload._id;
		newData = {
			$set: {
				body: payload.body,
				meta: payload.meta
			},
			$addToSet: {
				languageIds: {
					$each: payload.languageIds
				}
			}
		};
		// console.log('ThemeController update id',id);
		Theme.findOneAndUpdate({
			_id: id
		}, newData, {
			upsert: true,
			'new': true
		}, function (err, updatedTheme) {
			if (err) {
				// console.log('ThemeController update error', err,updatedTheme);
				deferred.reject(err);
			}
			// console.log('ThemeController updated. Call the next callback');
			deferred.resolve(updatedTheme);
		});
		return deferred.promise;
	};

	var detachLanguage = function (theme, langId) {
		var deferred = Q.defer(),
			newData;
		// console.log('detachLanguage params',theme,langId);

		newData = {
			$pull: {
				languageIds: langId
			}
		};
		// console.log('detachLanguage newData',newData);
		Theme.findOneAndUpdate({
			_id: theme._id
		}, newData, {
			safe: true,
			upsert: true,
			'new': true
		}).
		exec(function (err, theme) {
			if (err) {
				deferred.reject(err);
			}
			// console.log('detachLanguage result',theme);
			deferred.resolve(theme);
			// console.log('============');
		});
		return deferred.promise;
	};

	exports.remove = function (filter) {
		var deferred = Q.defer(),
			wordParams;

		if (!filter || typeof filter._id !== 'string' || typeof filter.languageId !== 'string') {
			deferred.reject('Error: the query is badly formatted');
			return deferred.promise;
		}

		// console.log('ThemeController remove method filter', filter);
		Theme.findOne({
			_id: filter._id
		}).exec(function (err, found) {
			if (err) {
				deferred.reject(err);
				return deferred.promise;
			}
			// console.log('exports.remove found', found);
			if (found) {
				wordParams = {
					languageId: filter.languageId,
					themes: [found._id]
				};
				WordController.get(wordParams).then(function (result) {
					// console.log('Theme remove found words', result);
					if (result && result.words && result.words instanceof Array && result.words.length > 0) {
						deferred.reject({
							ok: 0,
							msg: 'The theme contains words'
						});
					} else {
						if (found.languageIds instanceof Array && found.languageIds.length > 1) {
							// console.log('found.languageIds',found.languageIds.length);
							detachLanguage(found, filter.languageId).then(function ( /*updatedTheme*/ ) {
								// console.log('detached language', updatedTheme);
								deferred.resolve({
									ok: 1,
									n: 1,
									msg: 'Language detached'
								});
							}, function (err) {
								deferred.reject(err);
							});
						} else {
							Theme.remove({
								_id: filter._id
							}, function (err, removed) {
								// removes it from the database
								// console.log('ThemeController remove', err, removed);
								if (err) {
									deferred.reject(err);
								}
								// console.log('ThemeController removed method elements',removed);
								deferred.resolve(removed);
							});
						}
					}
				}, function (err) {
					// console.log('Error', err);
					deferred.reject(err);
				});
			} else {
				deferred.reject('Theme is not found');
			}
		});

		return deferred.promise;
	};
})();
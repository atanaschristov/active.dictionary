'use strict';
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var ThemeSchema = new Schema({
	'body': { type: String, default: '' },
	'userId': { type: String, default: '' },
	'languageIds': [{type: Schema.Types.ObjectId, ref: 'Language'}],
	'meta': {},
	'createdAt': { type: Date }
});

ThemeSchema.pre('save', function(next) {
	// Set date automatically to the createdAt field
	var now = new Date();
	if ( !this.createdAt ) {
		this.createdAt = now;
	}

	// if (!validatePresenceOf(this.password) && !this.skipValidation()) {
	// 	next(new Error('Invalid password'));
	// } else {
	// 	next();
	// }
	next();
});

ThemeSchema.methods.customMethod = function(callback) {
	callback();
};

var Theme = mongoose.model('Theme', ThemeSchema);

module.exports = Theme;
(function () {
	'use strict';

	var Language = require('./language-model.js'),
		ThemeController = require('../themes/theme-ctrl.js'),
		Q = require('q');

	exports.get = function (filter) {
		var deferred = Q.defer(),
			result;
		if (filter && (typeof filter !== 'object' || filter instanceof Array)) {
			deferred.reject('Error: the query is badly formatted');
		}
		// console.log('Language get filter',filter);
		Language.find(filter, function (err, languages) {
			// console.log('Language get result',err, languages);
			if (err) {
				// throw err;
				deferred.reject(err);
			}
			if (!languages) {
				deferred.reject('Failed to get the languages');
			}
			result = {
				languages: languages
			};
			// console.log('LanguageController get result',result);
			deferred.resolve(result);
		});
		return deferred.promise;
	};

	exports.add = function (payload) {
		var deferred = Q.defer(),
			model;

		// console.log('LanguageController add newLanguage', payload);
		if (!payload) {
			deferred.reject('Error: the query is badly formatted');
		}

		model = new Language(payload);
		// console.log('LanguageController add model instance', model.save);
		model.save(function (err, language) {
			// saves it in the database
			if (err) {
				// console.log('LanguageController add error', err);
				deferred.reject(err);
			}
			if (!language || typeof language._id === 'undefined') {
				deferred.reject('Error: couldn\'t create a language');
			} else {
				// console.log('LanguageController add call the next callback');
				deferred.resolve(language);
			}
		});
		return deferred.promise;
	};

	exports.remove = function (filter) {
		var deferred = Q.defer(),
			themeReqParams;
		// console.log('LanguageController remove method options', options);
		if (!filter || typeof filter._id !== 'string') {
			deferred.reject('Error: the query is badly formatted');
			return deferred.promise;
		}

		themeReqParams = {
			languageIds: {
				$in: [filter._id]
			}
		};

		ThemeController.get(themeReqParams).then(function (result) {
			// console.log('ThemeController.get', result);
			if (result && result.themes && result.themes instanceof Array && result.themes.length > 0) {
				// console.log('LanguageController found themes', result);
				deferred.reject({
					ok: 0,
					msg: 'The language contains themes'
				});
			} else {
				// console.log('LanguageController no themes');
				Language.remove(filter, function (err, result) {
					// saves it in the database
					if (err) {
						// console.log('LanguageController remove method error', err);
						deferred.reject(err);
					}
					// console.log('LanguageController removed method elements',result);
					deferred.resolve(result);
				});
			}
		}, function (err) {
			deferred.reject(err);
		});
		return deferred.promise;
	};
})();
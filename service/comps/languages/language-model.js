'use strict';
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var LanguageSchema = new Schema({
	'639-1': { index: true, type: String, default: '' },
	'639-2/b': { index: true, type: String, default: '' },
	'639-2/t': { index: true, type: String, default: '' },
	'639-3':{ index: true, type: String, default: '' },
	'639-6':{ index: true, type: String, default: '' },
	'family': { type: String, default: '' },
	'flagUrl': { type: String, default: '' },
	'name': { type: String, default: '' },
	'native name': { type: String, default: '' },
	'notes': { type: String, default: '' },
	// 'themeIds': [{type: Schema.Types.ObjectId, ref: 'Theme'}],
	'userId': {type: Schema.Types.ObjectId, ref: 'User'},
	'createdAt': { type: Date }
});

LanguageSchema.pre('save', function(next) {
	// Set date automatically to the createdAt field
	var now = new Date();
	if ( !this.createdAt ) {
		this.createdAt = now;
	}
	// if (!validatePresenceOf(this.password) && !this.skipValidation()) {
	// 	next(new Error('Invalid password'));
	// } else {
	// 	next();
	// }
	next();
});

LanguageSchema.methods.customMethod = function(callback) {
	callback();
};

var Language = mongoose.model('Language', LanguageSchema);

module.exports = Language;
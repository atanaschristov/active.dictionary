(function () {
	'use strict';

	var Word = require('./word-model.js'),
		Theme = require('../themes/theme-model.js'),
		Q = require('q'),
		_ = require('lodash');

	exports.get = function (filter) {
		var deferred = Q.defer(),
			result;
		// console.log('WordController exports.get filter',filter);
		if (filter.themes) {
			filter.themes = {
				$in: filter.themes
			};
		}
		// console.log('WordController exports.get filter',filter);
		Word.find(filter).select('-meta.audioRecord').populate('translations.word').sort({
			createdAt: -1
		}).exec(function (err, words) {
			if (err) {
				deferred.reject(err);
			}
			if (!words) {
				deferred.reject('Failed to load Words');
			}
			// req.result = words;
			result = {
				words: words
			};
			// console.log('WordController exports.get result',result)
			deferred.resolve(result);
		});
		return deferred.promise;
	};

	exports.getMeta = function (filter) {
		var deferred = Q.defer(),
			id = filter._id;

		// NOTE! filter has the format which select method requires
		// { fieldName: 1, fieldName: 0, fieldName: 1}

		delete filter._id;
		// console.log('exports.getMeta filter',filter);

		Word.findOne({
			_id: id
		}).select(filter).exec(function (err, meta) {
			if (err) {
				deferred.reject(err);
			}
			meta = meta.toObject();
			// console.log('getMeta result',meta);
			deferred.resolve(meta);
		});
		return deferred.promise;
	};

	/*
		store - function
		used by export.add and pushNewTranslation
		desc: Store in the database ot attach a theme to it
	 */
	var store = function (wordModel) {
		var deferred = Q.defer(),
			newData;
		var params = {
			word: wordModel.word,
			languageId: wordModel.languageId
		};
		// Before storing the function, make sure it doesn't exist
		// console.log('store -> findOne params',params);
		Word.findOne(params, function (err, found) {
			if (found) {
				// console.log('store -> found existing word',err,found);
				// If the word exists, than make sure that it will be shown for the selected theme
				// add the current themeId to the array of themes the word is associated with
				newData = {
					$addToSet: {
						themes: {
							$each: wordModel.themes
						}
					}
				};
				if (wordModel.translations) {
					newData.$addToSet.translations = {
						$each: wordModel.translations
					};
				}
				// console.log('Found existing word: store params',newData);
				Word.findOneAndUpdate({
					_id: found._id
				}, newData, {
					safe: true,
					upsert: true,
					'new': true
				}).
				populate('translations.word').exec(function (err, updatedWord) {
					if (err) {
						// console.log('WordController update error', err,updatedWord);
						deferred.reject(err);
					}
					// console.log('WordController updated: ',updatedWord);
					deferred.resolve(updatedWord);
				});
			} else {
				// console.log('store -> translations does not exist');
				wordModel.save(function (err, savedWord) {
					// saves it in the database
					if (err) {
						// console.log('WordController add error', err,savedWord);
						deferred.reject(err);
					}
					// console.log('WordController add call the next callback');
					deferred.resolve(savedWord);
				});
			}
		});
		return deferred.promise;
	};

	exports.add = function (newWord) {
		// console.log('WordController add params', newWord);
		var wordModel = new Word(newWord);
		return store(wordModel);
	};

	var updateWord = function (trigger, payload) {
		var deferred = Q.defer();
		if (!trigger) {
			deferred.resolve(payload);
		} else {
			var newData = {
				$set: {
					word: payload.word
				}
			};
			// console.log('updateWord newData',newData);
			Word.findOneAndUpdate({
				_id: payload._id
			}, newData, {
				safe: true,
				upsert: true,
				'new': true
			}).
			populate('translations.word').exec(function (err, updatedWord) {
				if (err) {
					// console.log('WordController update error', err,updatedWord);
					deferred.reject(err);
				}
				// console.log('WordController updated: ',updatedWord);
				deferred.resolve(updatedWord);
			});
		}

		return deferred.promise;
	};

	/**
	 * updateTranslation Updates modified translation
	 * 1. Checks if the modified value exists as a word
	 * 2. If yes, Create a translation cross reference between the translated word and the new found translation
	 * 3. If no, Update the translation with the new value
	 * @param  {Object} updatedTranslation [The word representing the modified translation]
	 * @param  {Object} payload            [The data comming from the request, representing the translated word]
	 * @return {Object}                    [promise]
	 */
	var updateTranslation = function (updatedTranslation, payload) {
		var deferred = Q.defer(),
			filter,
			translatedWord = payload;

		// console.log('updateTranslation updated',updated);
		// console.log('updateTranslation payload',payload);

		var translationKey = _.findKey(translatedWord.translations, {
			word: {
				'_id': updatedTranslation._id
			}
		});
		if (!translationKey) {
			deferred.reject('Failed to find the translation to be updated');
			return deferred.promise;
		}

		// console.log('updateTranslation translationKey',translationKey);
		translatedWord.translations[translationKey].word = updatedTranslation._id;
		// console.log('updateTranslation modified word',translatedWord);

		//Check if the new translation exists as a seprate word.
		filter = {
			languageId: updatedTranslation.languageId,
			word: updatedTranslation.word
		};

		// console.log('updateTranslation findOne filter',filter);
		Word.findOne(filter, function (err, otherTranslation) {
			// console.log('updateTranslation findOne result',err,otherTranslation);

			// If the new translation already exists as a word, 
			// just add link the new existing translation and the translated word
			// remove the link between the old translation and the translated word
			if (otherTranslation) {
				// TODO add the new theme to the word

				// console.log('updateTranslation found', otherTranslation);
				removeCrossReference(translatedWord, updatedTranslation).then(function () {
					// console.log('removeCrossReference result',result);
					return addCrossReference(translatedWord, otherTranslation);
				}).then(function (result) {
					// console.log('addCrossReference result',result);
					result.translated.populate('translations.word').execPopulate().then(function (updatedWord) {
						// console.log('Word updated. Before sending back the data',updatedWord);
						deferred.resolve(updatedWord);
					});
				}, function (err) {
					deferred.reject(err);
				});
			} else {
				// console.log('updateTranslation not found');
				// If the new translation does not exists, update the word itself, 
				updateWord(true, updatedTranslation).then(function (updatedWord) {
					payload.translations[translationKey].word = updatedWord;
					deferred.resolve(payload);
				}, function (err) {
					deferred.reject(err);
				});
			}

		});
		return deferred.promise;
	};

	var addLanguageToThemes = function (translatedWord) {
		var deferred = Q.defer(),
			promises = [];
		var newData, themeId, idx = translatedWord.themes.length;

		var updatedCallback = function (err, updatedTheme) {
			var updateTheme = function () {
				var deferred = Q.defer();
				if (err) {
					// console.log('addLanguageToThemes update error', err,updatedTheme);
					deferred.reject(err);
				}

				// resolve after the last update has passed. Currently we do not check for failures
				if (idx >= translatedWord.themes.length - 1) {
					deferred.resolve(updatedTheme);
				}
				// console.log('addLanguageToThemes success for ',updatedTheme);
				return deferred.promise;
			};

			promises.push(updateTheme());
		};

		// Iterate through all themes and of every one add a new languageId
		for (idx = 0; idx < translatedWord.themes.length; idx++) {
			themeId = translatedWord.themes[idx];
			newData = {
				$addToSet: {
					languageIds: translatedWord.languageId // add language id if not exist
				}
			};
			// console.log('addLanguageToThemes',themeId,newData);
			Theme.findOneAndUpdate({
				_id: themeId
			}, newData, {
				upsert: true,
				'new': true
			}, updatedCallback);
		}
		// console.log('addLanguageToThemes: Number of promises to be resolved',promises.length);
		Q.all(promises).then(function () { //themesArr) {
			// console.log('Updated theme',themesArr);
			// console.log('addLanguageToThemes translatedWord',translatedWord);
			deferred.resolve(translatedWord);
		}, function (err) {
			deferred.reject(err);
		});
		return deferred.promise;
	};

	var linkTranslations = function (wordItem, translations) {
		var deferred = Q.defer(),
			newData;

		newData = {
			$addToSet: {
				'translations': {
					$each: translations
				}
			}
		};
		// console.log('before update translations',payload._id,newData);
		Word.findOneAndUpdate({
			_id: wordItem._id
		}, newData, {
			safe: true,
			upsert: true,
			'new': true
		}).
		exec(function (err, updatedWord) {
			if (err) {
				deferred.reject(err);
			}
			// console.log('WordController updated. Call the next callback',updatedWord);
			deferred.resolve(updatedWord);
		});

		return deferred.promise;
	};

	var addCrossReference = function (translatedWord, translationWord) {
		var deferred = Q.defer(),
			result = {};

		linkTranslations(translationWord, [{
			language: translatedWord.languageId,
			word: translatedWord._id
		}]).then(function (translationWord) {
			result.translations = translationWord;
			// console.log('addTranslations translationWord',translationWord);
			return linkTranslations(translatedWord, [{
				language: translationWord.languageId,
				word: translationWord._id
			}]);
		}).then(function (translatedWord) {
			// console.log('addCrossReference translatedWord',result);
			result.translated = translatedWord;
			// console.log('addCrossReference result',result);
			deferred.resolve(result);
		}, function (err) {
			deferred.reject(err);
		});

		return deferred.promise;
	};

	/**
		@name pushNewTranslation
		If the translations field is updated with a new word
		1. The word is stored(Check: store)
		2. The ?!?!selected?!?! theme/s is/are updated, with the new languageId, which is of the language
		3. If the word, which is translated has other translations, 
			make a crossreference between those translations and the new translation
		4. Finally make a cross reference between the translated word and the new translation
		@author ATHR
	 **/
	var pushNewTranslation = function (newTranslation, payload) {
		var deferred = Q.defer(),
			newWord;

		// console.log('pushNewTranslation ===============');
		// console.log('pushNewTranslation payload',payload);
		// console.log('newTranslation',newTranslation);
		// var newTranslation = payload.newTranslation; delete payload.newTranslation;

		newWord = {
			word: newTranslation.word,
			languageId: newTranslation.languageId,
			themes: payload.themes,
			// translations: [{
			// 	languageId: payload.languageId,
			// 	word: payload._id
			// }]
		};

		// console.log('newWordModel',newWord);
		var wordModel = new Word(newWord);

		// console.log('pushNewTranslation -------------------');
		store(wordModel).then(addLanguageToThemes).
		then(function (translatedWord) {
			// console.log('pushNewTranslation before crossReferOtherTranslations',translatedWord.toObject());
			return addCrossReference(payload, translatedWord);
		}).
		then(function (result) {
			// console.log('Result after translations cross reference',result);
			// After all modifications, populate
			result.translated.populate('translations.word').execPopulate().then(function (updatedWord) {
				// if (err) {
				// 	deferred.reject(err);
				// }
				// console.log('pushNewTranslation Word updated. Before sending back the data',updatedWord);
				deferred.resolve(updatedWord);
			});
		}, function (err) {
			deferred.reject(err);
		});

		return deferred.promise;
	};

	var updateMeta = function (trigger, updatedWord, payload) {
		var deferred = Q.defer(),
			newData, idx;
		if (!trigger) {
			deferred.resolve(updatedWord);
		} else {
			newData = {
				$set: {}
			};
			// By setting only the provided meta fileds in the $set object,
			//  we ensure that the untouched meta fields are preserved
			for (idx in updatedWord.meta) {
				newData.$set['meta.' + idx] = updatedWord.meta[idx];
			}

			// console.log('updateMeta', newData);
			Word.findOneAndUpdate({
				_id: payload._id
			}, newData, {
				safe: true,
				upsert: true,
				'new': true
			}).
			populate('translations.word').exec(function (err, updatedWord) {
				if (err) {
					deferred.reject(err);
				}
				// console.log('WordController updated. Call the next callback', updatedWord);
				deferred.resolve(updatedWord);
			});
		}
		return deferred.promise;
	};

	var unlinkTranslations = function (wordItem, translations) {
		var deferred = Q.defer(),
			newData,
			words = [],
			languages = [];
		// console.log('+++++++++++++');
		// console.log('unlinkTranslations wordItem',wordItem,'translations',translations);
		if (!wordItem) {
			console.log('unlinkTranslations missing wordItem', wordItem);
			deferred.reject(wordItem);
		} else {
			for (var idx in translations) {
				words.push(translations[idx].word);
				languages.push(translations[idx].language);
			}
			// console.log('unlinkTranslations words,translations',words,translations);
			newData = {
				$pull: {
					translations: {
						word: {
							'$in': words
						},
						language: {
							'$in': languages
						}
					}
				}
			};
			// console.log('unlinkTranslations newData',newData.$pull.translations);
			Word.findOneAndUpdate({
				_id: wordItem._id
			}, newData, {
				safe: true,
				upsert: true,
				'new': true
			}).
			exec(function (err, updatedWord) {
				if (err) {
					deferred.reject(err);
				}
				// console.log('unlinkTranslations result',updatedWord);
				deferred.resolve(updatedWord);
				// console.log('============');
			});
		}


		return deferred.promise;
	};

	var removeCrossReference = function (translatedWord, translationWord) {
		var deferred = Q.defer(),
			translations = [],
			result = {};

		translations.push({
			language: translationWord.languageId,
			word: translationWord._id
		});
		unlinkTranslations(translatedWord, translations).then(function (updatedWord) {
			result.translated = updatedWord;
			translations = [];
			translations.push({
				language: translatedWord.languageId,
				word: translatedWord._id
			});
			return unlinkTranslations(translationWord, translations);
		}).then(function (updatedWord) {
			result.translations = updatedWord;
			// console.log('removeCrossReference sresult',result)
			deferred.resolve(result);
		}, function (err) {
			deferred.reject(err);
		});
		return deferred.promise;
	};

	var removeTranslations = function (updatedTranslation, payload) {
		var deferred = Q.defer(),
			translatedWord = payload,
			translationKey = _.findKey(translatedWord.translations, {
				word: {
					_id: updatedTranslation._id
				}
			});

		translatedWord.translations[translationKey].word = updatedTranslation._id;
		// console.log('removeCrossReference translatedWord,updatedTranslation',translatedWord,updatedTranslation);
		removeCrossReference(translatedWord, updatedTranslation).then(function (result) {
			// console.log('unlinkTranslation result',result);
			result.translated.populate('translations.word').execPopulate().then(function (updatedTranslation) {
				// console.log('Word updated. Before sending back the data',updatedTranslation);
				deferred.resolve(updatedTranslation);
			}, function (err) {
				deferred.reject(err);
			});
		}, function (err) {
			deferred.reject(err);
		});
		return deferred.promise;
	};

	var handleTranslations = function (changed, updatedWord, payload) {
		var deferred = Q.defer(),
			translations = payload.translations,
			promises = [];
		if (!changed) {
			deferred.resolve(updatedWord);
			return deferred.promise;
		}

		// console.log('handleTranslations payload translations',translations);
		// gather all the promises in the promises array, so we can resolve tham at once later
		translations.forEach(function (translation) {
			var promise;
			var cmd = translation.cmd;
			delete translation.cmd;
			if (typeof cmd !== 'string') {
				return;
			}
			// console.log('handleTranslations translations.cmd',cmd);
			switch (cmd) {
			case 'unlink':
				// console.log('handleTranslations We have unlink command');
				promise = removeTranslations(translation.word, payload);
				break;
			case 'update':
				// console.log('handleTranslations We have update command');
				promise = updateTranslation(translation.word, payload);
				break;
			case 'add':
				// console.log('handleTranslations We have add command');
				promise = pushNewTranslation(translation.word, payload);
				break;
			case 'remove':
				// TODO We may ask the users to force delete translation words, not just unlinking.
				// If a remove command is set, we force remove the translation world 
				// only if it is not used in another theme, otherwise we just unlink it. 
				console.log('handleTranslations We have remove command');
				break;
			default:
				return;
			}
			if (promise) {
				promises.push(promise);
			}
		});
		// console.log('handleTranslations: Number of promises to be resolved',promises.length);
		Q.all(promises).then(function (resultsArr) {
			// console.log('handleTranslations After all updates updatedWord',updatedWord);

			// Q.all returns array of results for every promise. We just need the last result
			deferred.resolve(resultsArr.pop());
		}, function (err) {
			deferred.reject(err);
		});

		return deferred.promise;
	};

	// .update({_id: contact.id}, upsertData, {upsert: true}, function(err{...});
	exports.update = function (payload) {
		var deferred = Q.defer();
		// console.log('WordController update payload', payload);
		var id = payload._id;
		var changed = payload.updated;
		delete payload.updated;
		if (!id || !changed) {
			deferred.reject('Unexpected data in update request');
			return deferred.promise;
		}

		updateWord(changed.word, payload).then(function (updatedWord) {
			// console.log('before pushNewTranslations',updatedWord);
			return handleTranslations(changed.translations, updatedWord, payload);
		}).then(function (updatedWord) {
			return updateMeta(changed.meta, updatedWord, payload);
		}).then(function (updatedWord) {
			// console.log('Result',updatedWord);
			deferred.resolve(updatedWord);
		}, function (err) {
			deferred.reject(err);
			return deferred.promise;
		});

		return deferred.promise;
	};

	var cleanTranslations = function (word) {
		var deferred = Q.defer(),
			idx = 0,
			translations = [];

		// console.log('cleanTranslations initial word',word);
		if (word.translations) {
			for (; idx < word.translations.length; idx += 1) {
				translations = [];
				translations.push({
					language: word.languageId,
					word: word._id
				});
				// console.log('Word translations to be removed',word.translations[idx].word);
				unlinkTranslations(word.translations[idx].word, translations);
			}
		}
		deferred.resolve(word);

		return deferred.promise;
	};

	var detachTheme = function (word, themeId) {
		var deferred = Q.defer(),
			newData;
		// console.log('detachTheme params',word,themeId);

		newData = {
			$pull: {
				themes: themeId
			}
		};
		// console.log('detachTheme newData',newData);
		Word.findOneAndUpdate({
			_id: word._id
		}, newData, {
			safe: true,
			upsert: true,
			'new': true
		}).
		populate('translations.word').exec(function (err, updatedWord) {
			if (err) {
				deferred.reject(err);
			}
			// console.log('detachTheme result',updatedWord);
			deferred.resolve(updatedWord);
			// console.log('============');
		});

		return deferred.promise;
	};

	exports.remove = function (filter) {
		var deferred = Q.defer();
		// console.log('WordController remove filter', filter);
		Word.findOne({
			_id: filter._id
		}).populate('translations.word').exec(function (err, found) {
			if (err) {
				deferred.reject(err);
			}
			// console.log('remove word found',found);
			if (found) {
				// If there are more than one themeIds in the theme, 
				// remove the themeId from themes 
				if (found.themes instanceof Array && found.themes.length > 1) {
					// console.log('found.themes',found.themes.length);
					detachTheme(found, filter.themeId).then(function (updatedWord) {
						// console.log('updatedWord',updatedWord);
						deferred.resolve(updatedWord);
					}, function (err) {
						deferred.reject(err);
					});
				} else { // Else remove all the translations and the whole word
					cleanTranslations(found).then(function (word) {
						// console.log('Cleaned trnaslations',word);

						Word.remove({
							_id: word._id
						}, function (err, removed) {
							// saves it in the database
							if (err) {
								// console.log('WordController remove method error', err);
								deferred.reject(err);
							}
							// console.log('WordController removed method elements',removed);
							deferred.resolve(removed);
						});
					});
				}
			}
		});
		return deferred.promise;
	};
})();
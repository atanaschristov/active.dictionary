'use strict';
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var TranslationSchema = new Schema({
	'language': {type: Schema.Types.ObjectId, ref: 'Language'},
	'word': {type: Schema.Types.ObjectId, ref: 'Word'},
},{_id : false});

var WordSchema = new Schema({
	'word': { type: String, default: '' },
	'languageId': {type: Schema.Types.ObjectId, ref: 'Language'},
	'themes': [{ type: Schema.Types.ObjectId, ref: 'Theme' }],
	'translations': [TranslationSchema],
	'meta': {},
	'createdAt': { type: Date }
});

WordSchema.pre('save', function(next) {
	// Set date automatically to the createdAt field
	var now = new Date();
	if ( !this.createdAt ) {
		this.createdAt = now;
	}

	// if (!validatePresenceOf(this.password) && !this.skipValidation()) {
	// 	next(new Error('Invalid password'));
	// } else {
	// 	next();
	// }
	next();
});

WordSchema.methods.customMethod = function(callback) {
	callback();
};

var Word = mongoose.model('Word', WordSchema);

module.exports = Word;
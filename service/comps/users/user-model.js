'use strict';
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var UserSchema = new Schema({
	'from': { type: String, default: '' },
	'id': { type: String, default: '' },
	'name': { type: String, default: '' },
	'gender': { type: String, default: '' },
	'picture': { type: String, default: '' },
	'locale':{ type: String, default: '' },
});

UserSchema.pre('findOneAndUpdate', function(next) {
	// if (!validatePresenceOf(this.password) && !this.skipValidation()) {
	// 	next(new Error('Invalid password'));
	// } else {
	// 	next();
	// }
	next();
});

UserSchema.methods.customMethod = function(callback) {
	callback();
};

var User = mongoose.model('User', UserSchema);

module.exports = User;
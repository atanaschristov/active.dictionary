(function() {
	'use strict';
	var User = require('./user-model.js'),
		Q = require('q');

	exports.get = function (filter) {
		var deferred = Q.defer(),
			result;
		// console.log('exports.get User filter',filter, typeof filter);
		User.find(filter,function (err, user) {
			// console.log('exports.get User find result',user);
			if (err) {
				deferred.reject(err);
			}
			if (!user) {
				deferred.reject('Failed to load User');
			}
			// req.result = themes;
			result = {user: user};
			deferred.resolve(result);
		});
		return deferred.promise;
	};

	// .update({_id: contact.id}, upsertData, {upsert: true}, function(err{...});
	exports.upsert = function (req, res, next) {
		var payload = req.params;
		// console.log('UserController update payload', payload);
		User.update({id: payload.id}, payload, {upsert: true},function(err,user) {
			if (err) {
				console.log('UserController update error', err,user);
				return next(err);
			}
			// console.log('UserController update call the next callback');
			next(user); // return the saved language
		});
	};

	exports.remove = function (req, res, next, filter) {
		var options = {};
		if(filter) {
				options = filter;
		}
		User.remove(options, function(err,removed) {
			// saves it in the database
			if (err) {
				return next(err);
			}
			next(removed); // return the saved language
		}); 
	};
})();

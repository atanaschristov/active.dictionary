var restify = require('restify'),
	restifyOAuth2 = require('restify-oauth2'),
	fs = require('fs'),
	mongoose = require('mongoose'),
	config = require('./config/config.js');

// Setup memory watcher to identify leaks
var memwatch = require('memwatch');
memwatch.on('leak', function (info) {
	console.error('Memory leak detected: ', info);
});
// memwatch.on('stats', function (stats) {
// 	console.error('Stats: ', stats);
// });

var serverPort = process.env.OPENSHIFT_NODEJS_PORT || 8181,
	serverSSLPort = 8443,
	serverIpAddress = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1',
	startSSL = !process.env.OPENSHIFT_NODEJS_IP ? true : false;

// Connect to the database
var dbConnect = function () {
	// NOTE: If there is an error: 
	// { [MongoError: getaddrinfo ENOTFOUND] name: 'MongoError', message: 'getaddrinfo ENOTFOUND' }
	// 
	// make sure NODE_ENV is set. Check ./config/config.js
	var options = {
		server: {
			socketOptions: {
				keepAlive: 1
			}
		}
	};
	mongoose.connect(config.db, options);
};
dbConnect();

mongoose.connection.on('error', console.log);
mongoose.connection.on('disconnected', dbConnect);

console.log(__dirname);

// fs.readdirSync(__dirname + '/models').forEach(function (file) {
//  	if (~file.indexOf('.js')) {
//  		console.log(__dirname + '/app/models/' + file);
//  		// require(__dirname + '/app/models/' + file);
//  	}
// });

var serverSetup = function (serverInstance) {
	var oauthHooks = require('./comps/common/oauth2-hooks.js');

	serverInstance.pre(restify.fullResponse());
	// serverInstance.pre(restify.CORS());

	serverInstance.use(restify.authorizationParser());
	serverInstance.use(restify.bodyParser());
	// serverInstance.use(restify.bodyParser({ mapParams: false }));
	serverInstance.pre(restify.pre.sanitizePath());
	serverInstance.use(restify.queryParser());

	// Enable Authorization
	function unknownMethodHandler(req, res) {
		if (req.method.toLowerCase() === 'options') {
			// console.log('received an options method request');
			var allowHeaders = ['Authorization', 'Content-Type', 'Accept', 'Accept-Version', 'Api-Version', 'Origin', 'X-Requested-With'];
			// console.log('Access-Control-Allow-Headers',allowHeaders.join(', '));
			if (res.methods.indexOf('OPTIONS') === -1) {
				res.methods.push('OPTIONS');
			}

			res.header('Access-Control-Allow-Credentials', true);
			res.header('Access-Control-Allow-Methods', res.methods.join(', '));
			// res.header('Access-Control-Allow-Methods', ['GET', 'DELETE', 'POST', 'PUT'].join(', '));
			// res.header('Access-Control-Allow-Origin', req.headers.origin);
			res.header('Access-Control-Allow-Origin', '*');
			res.header('Access-Control-Allow-Headers', allowHeaders.join(', '));

			return res.send(204);
		} else {
			return res.send(new restify.MethodNotAllowedError());
		}
	}
	server.on('MethodNotAllowed', unknownMethodHandler);

	var expirationTime = 60 * 60 * 24 * 7; // 1 week
	// expirationTime = 10; // 10 seconds // DEBUG

	// store the exipration period, so we can check if they expired
	oauthHooks.setExpirationperiod(expirationTime);

	// This opens /token access point for post actions, which if pass successful, 
	// will return object {"access_token":"blxOBCjZNCF0+lU3OODXz1Y1KKV7fcWTeAClAANiWuM=","token_type":"Bearer"}
	// the requred parameters are: {'username': '','password': '','grant_type':'client_credentials'}
	restifyOAuth2.cc(serverInstance, {
		tokenEndpoint: '/token',
		hooks: oauthHooks,
		tokenExpirationTime: expirationTime
	});
	require('./comps/common/routes.js')(serverInstance); // Define API access points
}

// Instantiate the HTTP Servers
var httpOptions = {
	// certificate: 'String, If you want to create an HTTPS server, pass in the PEM-encoded certificate and key',
	// key: 'String, The same as above',
	// log: 'Object, You can optionally pass in a bunyan instance; not required',
	// spdy: 'Object, Any options accepted by node-spdy',
	// version: 'String, A default version to set for all routes',
	// handleUpgrades: 'Boolean, Hook the upgrade event from the node HTTP server, pushing Connection: Upgrade requests through the regular request handling chain; defaults to false',
	// httpsServerOptions: 'Object, Any options accepted by node-https Server. If provided the following restify server options will be ignored: spdy, ca, certificate, key, passphrase, rejectUnauthorized, requestCert and ciphers; however these can all be specified on httpsServerOptions.',
	name: 'ActiveDict REST Api',
	formatters: {
		'application/hal+json': function (req, res, body) {
			// console.log('formatters',body);
			return res.formatters['application/json'](req, res, body);
		}
	}
	// version: '1.0',
};
var server = restify.createServer(httpOptions);
serverSetup(server);
server.listen(serverPort, serverIpAddress, function () {
	console.log('%s listening at %s', server.name, server.url);
});

// startSSL=false;
// console.log('startSSL',startSSL);
if (startSSL) {
	// Setup some https server options
	var httpsOptions = {
		name: 'ActiveDict REST Api',
		key: fs.readFileSync('./key.pem'),
		certificate: fs.readFileSync('./cert.pem')
	};
	var securedServer = restify.createServer(httpsOptions);
	serverSetup(securedServer);
	securedServer.listen(serverSSLPort, serverIpAddress, function () {
		console.log('%s listening at %s', securedServer.name, securedServer.url);
	});
}